import java.util.Scanner;

public class ETest {

	public static void main(String[] args) {
		if (args.length == 0) {
		System.out.println(
				"I will compute all the numbers which have squares less than or equal to a given limit.\nPlease enter a limit below, then press enter.");
		}
		Scanner input = new Scanner(System.in);
		while (input.hasNextLong()) {
			long limit = input.nextLong();
			if (limit < 0) {
				invalidInput();
				
			} else {
				printTheAnswer(limit);
			}
		}

		if (input.hasNext()) {
			if (input.next().equals("exit")) {
				System.out.println("Goodbye!");
			} else {
				invalidInput();
				String[] callFromError = {"Help, I'm trapped in the computer"};
				main(callFromError);
			}
		}
		input.close();
	}

	public static void invalidInput() {
		System.out.println("Invalid input, please enter a positive integer (below " + Long.MAX_VALUE + "), or the word 'exit'.");
	}
	
	public static void printTheAnswer(long limit) {
		System.out.print("The numbers whose squares are less than or equal to " + limit + " are ");
		for (long i = 0; i * i <= limit; i++) {
			if (i == 0) {
				System.out.print(i);
			} else {
				System.out.print(", " + i);
			}
		}
		System.out.println(".\nPlease enter another number or type the word 'exit'.");
	}
}