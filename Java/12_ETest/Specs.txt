Write a program that, when supplied with a number, prints out all the numbers with squares below or equal to that number.
Make sure it handles invalid input elegantly.
