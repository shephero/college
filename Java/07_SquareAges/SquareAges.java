
import java.util.Scanner;

import javax.swing.JOptionPane;

public class SquareAges {
	public static final int MAX_LIFE = 123;

	public static void main(String[] args) {
		String yearinput = JOptionPane.showInputDialog("What year is it?");
		Scanner input = new Scanner(yearinput);
		int currentYear = 2015;
		currentYear = input.nextInt();

		input.close();

		String birthyearInput = JOptionPane.showInputDialog("In what year were you born?");
		input = new Scanner(birthyearInput);
		int birthyear = input.nextInt();
		input.close();
		// TODO error handling for when non-numeric characters are entered.
		
		String notices = "";
		if (currentYear < 2015) {
			notices += ("I doubt the current year is " + currentYear + ".\n");
		}
		if (currentYear < birthyear) {
			notices += ("If the current year is " + currentYear + " then how were you born in " + birthyear + "?\n");
		}
		if ((currentYear - birthyear) > 123) {
			notices += ("I doubt you're " + (currentYear - birthyear) + " years old.\n");
		}
		if (!notices.isEmpty()) {
			notices += ("That in mind, I'll try to figure it out anyway.");
		}
				
		System.out.println("The current year is " + currentYear);
		System.out.println("Your birthyear is " + birthyear);

		int[] squareAgeArray = new int[MAX_LIFE];
		String output = "";

		for (int age = 0; age < MAX_LIFE; age++) {
			squareAgeArray[age] = age * age;
			int testingYear = age + birthyear;
			int secondTestingYear = -1;
			if (age > 0) {
				secondTestingYear = age + birthyear - 1;
			}
			output += testAndAppend(testingYear, age, currentYear, squareAgeArray[age]);
			output += testAndAppend(secondTestingYear, age, currentYear, squareAgeArray[age]);
		}
		
		/* Every year, you go through stages of being two different ages.
		 * Eg. I was born in 1996, therefore in 1997 I was 0, then I was 1.
		 * Therefore I invoke the method testAndAppend (below) for two ages for every year.
		 */
		
		if (!notices.isEmpty()) {
		JOptionPane.showMessageDialog(null, notices);
		}
		
		if (!output.isEmpty()) {
			JOptionPane.showMessageDialog(null, output);
		}
		
		else {
			JOptionPane.showMessageDialog(null, "You will not be alive in a year that is the square of your age.");
		}
	}

	public static String testAndAppend(int testingYear, int age, int currentYear, int squareAge) {
		String toReturn = "";
		if (testingYear == squareAge) {
			System.out.println("Match found at year " + (testingYear) + " at age " + age);
			if (testingYear == currentYear) {
				toReturn += "This year is the square of your age\n";
			} else if (testingYear < currentYear) {
				toReturn += ("In " + testingYear + " you were " + age + ". \n" + age + " squared equals "
						+ testingYear + ".\n");
			} else if (testingYear > currentYear) {
				toReturn += ("In " + testingYear + " you will be " + age + ". \n" + age + " squared equals "
						+ testingYear + ".\n");
			}
		}
	return toReturn;
	}
	/* If the year for testing corresponds to the age (in that year) squared,
	 * return sentence to that effect, conjugated in relation to the current year.
	 */
}