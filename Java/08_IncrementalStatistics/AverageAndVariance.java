import java.util.Scanner;

public class AverageAndVariance {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double average = 0;
		double previousAverage = 0;
		double variance = 0;
		int numbersEntered = 0;
		
		System.out.println("Enter a number (or type 'exit'):");
		
		while (input.hasNextDouble()) { 
			numbersEntered++;
			double currentNumber = input.nextDouble();  // 
			average = getAverage(currentNumber, average, numbersEntered);
			variance = getVariance(currentNumber, variance, numbersEntered, average, previousAverage);
			System.out.println("The current average is " + average + ".");
			System.out.println("The current variance is " + variance + ".");
			previousAverage = average;
			System.out.println("Enter another number (or type 'exit')");
		}
		/* If a(nother) number is entered, this while loop calls functions to obtain the average
		 * and variance of all numbers entered.
		 */
		
		
		if (input.hasNext()) {
			String command = input.next();
			if (command.equals("exit")) {
				System.out.println("The total average is " + average + ".");
				System.out.println("The total variance is " + variance + ".");
				System.out.println("Goodbye.");
			}
			else {
				System.out.println("Invalid input.");
			}
		}
		
		// If "exit" is entered into the standard input, print the latest average and variance, then say goodbye.
		
		input.close();
	}
	
	public static double getAverage(double currentNumber, double currentAverage, int numbersEntered) {
		if (numbersEntered == 1) {
			return currentNumber;
		}
		else {
			return (currentAverage + (currentNumber - currentAverage)/numbersEntered);
		}
	}
	// Implementation of the formula: Average(n) = Average(n-1) + (Number(n) - Average(n-1)) / n

	
	public static double getVariance(double currentNumber, double currentVariance, int numbersEntered, double average, double previousAverage) {
		if (numbersEntered == 1) {
			return 0.0;
		}
		else {
			return (((currentVariance * (numbersEntered - 1)) + (currentNumber - previousAverage) * (currentNumber - average))/numbersEntered);
		}
	}
	// Implementation of the formula: Variance(n) = [(Variance(n-1) * (n-1)) + (Number(n) - Average(n-1)) * (Number(n) - Average(n))] / n

}
