import java.util.Scanner;

public class TriangularStars {

	private static boolean isStarNumber(long number) {
		/*
		 * s = 6n(n-1)+1 
		 * (s-1)/6 = n(n-1) 
		 * n^2 - n - (s-1)/6 = 0 
		 * (n - 1/2)^2 - 1/4 -(s-1)/6 = 0 
		 * n = 1/2 + sqrt(1/4 + (s-1)/6)
		 * 
		 */

		long indexFloored = (long) Math.floor(0.5 + Math.sqrt(0.25 + (number - 1) / 6.0));
		long indexCeiled = indexFloored + 1;
		if (determineStarNumber(indexFloored) == number
				|| determineStarNumber(indexCeiled) == number) {
			return true;
		}
		return false;
	}
	
	private static long determineStarNumber(long index) {
		return (6 * index * (index - 1) + 1);
	}
	
	//	This is not used since writing the iterative function
	private static long determineTriangleNumberRecursively(long index) { 
		if (index == 1) {
			return 1;
		} else {
			return (index + determineTriangleNumberRecursively(index - 1));
		}
	}

	private static long determineTriangleNumberIteratively(long index) {
		long triangleNumber = 0;
		for (long i = 1; i <= index; i++) {
			triangleNumber += i;
		}
		return triangleNumber;
	}

	private static void printStarlongersectionTriangle(long limit) {
		for (long i = 1; i < limit; i++) {
			long triangleNumber = determineTriangleNumberIteratively(i);
			if (triangleNumber > limit) {
				break;
			}
			if (isStarNumber(triangleNumber)) {
				if (i == 1) {
					System.out.print(triangleNumber);
				} else {
					System.out.print(", " + triangleNumber);
				}
			}
		}
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		printStarlongersectionTriangle(Integer.MAX_VALUE);
		input.close();
	}
}
