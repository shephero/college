import java.util.ArrayList;
import java.util.Scanner;

public class Winners {
	
	private static String suffix(int i) {
		String suffix = "";
		i %= 100;
		if ((i < 20) && (i > 10)) {
			suffix += "th";
		}
		else {
			i %= 10;
			switch (i) {
			case 1:
				suffix += "st";
				break;
			case 2:
				suffix += "nd";
				break;
			case 3:
				suffix += "rd";
				break;
			default:
				suffix += "th";
				break;
			}
				
		}
		
		return suffix;
	}
	
	public static void main(String[] args) {
		System.out.println("Input number of participants:");
		Scanner input = new Scanner(System.in);
		int participants = input.nextInt();
		input.close();
		ArrayList<Integer> filter = new ArrayList<Integer>();
		
		for (int i = 0; i <= participants; i++) {
			filter.add(i);
		}
		
		for (int i = 2; i <= participants; i++) {
			for (int j = 2; j < participants; j++) {
				if ((i * j) <= participants) {
					filter.set((i*j), -1);
				}
			}
		}
		filter.set(0, -1);
		
		System.out.println();
		for (int i = 0; i <= participants; i++) {
			if (filter.get(i) != -1) {
				System.out.print(i + suffix(i) + ", ");
			}
		}
	}
}
