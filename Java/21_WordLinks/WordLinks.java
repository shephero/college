import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;

public class WordLinks {
    private static final String dictFileName = "words.txt";

    void run() {
        println("Loading Dictionary...");
        List<String> dict = loadDictionary();
        println("Done");
        Scanner scanner = new Scanner(System.in);
        println("WordLinks!");
        println("Please enter a dash ('-') separated list of words:");
        System.out.print("-> ");
        List<String> words = readWords(scanner);
        boolean isChain = isWordChain(words, dict);
        println("That is " + (isChain?"":"not ") + "a valid word chain.");
    }

    private boolean isDifferentByOne(String wordOne, String wordTwo) {
        int differsBy = 0;
        for(int i = 0; i < wordOne.length(); i++) {
            if (wordOne.charAt(i) != wordTwo.charAt(i)) {differsBy++;}
        }
        return differsBy == 1;
    }

    List<String> loadDictionary() {
        try {
            return Files.lines(Paths.get(dictFileName))
                        .map(w -> w.toLowerCase())
                        .collect(Collectors.toList());
        }
        catch (Exception e) {
            println("Cannot open dictionary file.\n" +
                    "Please place a dictionary file called\n" +
                    "\'words.txt\' in the bytecode directory.");
        }
        return null;
    }

    boolean isUnique(List<String> strings) {
        Set set = new HashSet<>(strings);
        return set.size() == strings.size();
    }

    List<String> readWords(Scanner scanner) {
        String line = scanner.nextLine();
        if (line.equals("\n")) {System.exit(0);}
        String[] wordArray = line.split("-");
        return Arrays.stream(wordArray)
                        .map(w -> w.replaceAll("\\s", "")
                                   .toLowerCase())
                        .collect(Collectors.toList());
    }

    private boolean isEnglishWord(String word, List<String> dict) {
        return dict.contains(word);
    }

    public static void main(String[] args) {
        WordLinks links = new WordLinks();
        links.run();
    }

    private boolean isWordChain(List<String> words, List<String> dict) {
        boolean valid = true;
        for (String word : words) {
            if (!isEnglishWord(word, dict)) {
                println("'" + word + "' is not an English word.");
                valid = false;
            }
        }
        for (int i = 0; i < words.size() - 1; i++) {
            if (words.get(i).length() == words.get(i+1).length()) {
                if (!isDifferentByOne(words.get(i), words.get(i + 1))) {
                    println("'" + words.get(i) + "' and '" + words.get(i + 1) + "'" +
                            " do not differ by one letter.");
                    valid = false;
                }
            }
            else {
                println("'" + words.get(i) + "' and '" + words.get(i + 1) + "'" +
                        " are not of the same length.");
                valid = false;
            }
        }
        if (!isUnique(words)) {
            println("Not all words are unique.");
            valid = false;
        }
        return valid;
    }

    private void println(String input) {
        System.out.println(input);
    }
}
