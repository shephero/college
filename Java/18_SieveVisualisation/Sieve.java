public class Sieve {

/**
 *
 * This program displays the Sieve or Eratosthenes.
 * A Model View Controller architecture is used.
 * The model is SieveSimulation, the controller is SieveController, and the view is SieveVisualisation.
 *
 */

    public static void main(String args[]) {
        SieveController controller = new SieveController();
        controller.run();
    }
}