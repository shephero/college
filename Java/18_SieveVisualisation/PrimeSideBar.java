public class PrimeSideBar {
    public static final int TEXT_HEIGHT = 20;
    public static final int TEXT_WIDTH = 40;
    int numbers;
    int currentColumn;
    int currentRow;
    Grid grid;
    String toPrint;

    PrimeSideBar(Grid grid) {
        toPrint = "Primes:";
        numbers = 0;
        currentColumn = 1;
        currentRow = 1;
        this.grid = grid;
        print();
    }

    double[] getPosition() {
        int x = grid.dimensions[0] * grid.SQUARE_SIZE_PIXELS + currentColumn * TEXT_WIDTH;
        int y = currentRow * TEXT_HEIGHT;
        double[] proportions = new double[2];
        proportions[0] = SieveVisualisation.pixelsToProportion(x, true);
        proportions[1] = 1 - SieveVisualisation.pixelsToProportion(y, false);
        return proportions;
    }

    void detectEndOfColumn() {
        if (currentRow * TEXT_HEIGHT + TEXT_HEIGHT /2 > grid.dimensions[1] * grid.SQUARE_SIZE_PIXELS) {
            currentColumn++;
            currentRow = 2;
        }
    }

    void addPrime(int number) {
        toPrint = Integer.toString(number);
        print();
    }

    void print() {
        StdDraw.setPenColor(SieveVisualisation.COLOUR_FOUR);
        detectEndOfColumn();
        double[] position = getPosition();
        StdDraw.text(position[0], position[1], toPrint);
        numbers++;
        currentRow++;
    }
}
