
class Grid {
    public static final int SQUARE_SIZE_PIXELS = 50;
    public GridCell[] cells;
    protected int[] dimensions;

    Grid(int numbers) {
        cells = new GridCell[numbers];
        dimensions = new int[2];
        dimensions[0] = determineColumns(numbers);
        dimensions[1] = determineRows(cells.length, dimensions[0]);

        //Cell coordinates start counting at one, implementation was quicker that way.
        int coordinates[] = new int[2];
        coordinates[0] = 1;
        coordinates[1] = 1;

        for (int i = 0; i < numbers; i++) {
            coordinates[0]++;
            cells[i] = new GridCell(i+2, this, coordinates);
            if (coordinates[0] >= dimensions[0]) {
                coordinates[0] = 0;
                coordinates[1]++;
            }
        }
    }

    void setCentres() {
        for (GridCell cell : cells) {
            cell.setCentre(cell.getCentre());
        }
    }

    void drawGrid() {
        for (GridCell cell : cells) {
            cell.setColor(SieveVisualisation.COLOUR_THREE);
            cell.drawCell(false);
        }
    }

    private int determineRows (int tiles, int columns) {
        int rows = 0;
        for (int i = 0; i < tiles; i++) {
            if (i % columns == 0) {rows++;}
        }
        return rows;
    }

    private int determineColumns(int n) {
        return (int) Math.sqrt(n) + 1;
    }
}
