import javax.swing.*;
import java.awt.*;

public class SliderDialog {
    public static int getInt() {
        JDialog dialog = new JDialog();
        dialog.setLocationRelativeTo(null);
        dialog.setSize(new Dimension(200, 100));
        dialog.setModal(true);
        dialog.setLayout(new GridLayout(2, 1));
        JSlider slider = new JSlider();
        slider.setMinimum(2);
        slider.setMaximum(300);
        slider.setValue(151);
        dialog.add(slider);
        JButton button = new JButton("Okay");
        button.addActionListener(actionEvent -> {
            dialog.setVisible(false);
        });
        dialog.add(button);
        dialog.setVisible(true);
        return slider.getValue();
    }
}
