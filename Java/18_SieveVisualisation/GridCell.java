import java.awt.Color;

class GridCell {
    private int number;
    private Color color;
    int[] gridReference;
    Grid parentGrid;
    double[] centre;

    GridCell(int number, Grid parent, int[] coordinates) {
        this.number = number;
        parentGrid = parent;
        gridReference = new int[2];
        gridReference[0] = coordinates[0];
        gridReference[1] = coordinates[1];
        setColor(SieveVisualisation.COLOUR_ONE);
    }

    public double[] getCentre() {
        double[] centre = new double[2];
        centre[0] = (double) (gridReference[0] * SieveVisualisation.pixelsToProportion((double) Grid.SQUARE_SIZE_PIXELS, true) - SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, true));
        centre[1] = (double) (1 - (gridReference[1] * SieveVisualisation.pixelsToProportion((double)
                Grid.SQUARE_SIZE_PIXELS, false) - SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, false)));
        return centre;
    }

    public void setCentre(double[] centre) {
        this.centre = (centre);
    }

    public void drawCell(boolean patterned) {
        if (!patterned) {
            StdDraw.setPenColor(color);
            StdDraw.filledRectangle(
                    centre[0],
                    centre[1],
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, true),
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, false)
            );
            StdDraw.setPenColor(SieveVisualisation.COLOUR_ONE);
            StdDraw.setPenRadius(SieveVisualisation.pixelsToProportion(1, false));
            StdDraw.rectangle(
                    centre[0],
                    centre[1],
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, true),
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, false)
            );
            StdDraw.setPenColor(SieveVisualisation.COLOUR_ONE);
            StdDraw.text(centre[0], centre[1], Integer.toString(number));
        }
        else {
            StdDraw.setPenColor(color);
            StdDraw.setPenRadius(SieveVisualisation.pixelsToProportion(1, false));
            StdDraw.filledRectangle(
                    centre[0],
                    centre[1],
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, true),
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, false)
            );
            StdDraw.setPenColor(SieveVisualisation.COLOUR_ONE);
            StdDraw.setPenRadius(SieveVisualisation.pixelsToProportion(1, false));
            StdDraw.rectangle(
                    centre[0],
                    centre[1],
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, true),
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS / 2.0, false)
            );
            StdDraw.setPenRadius(SieveVisualisation.pixelsToProportion(0, false));
            for (int i = Grid.SQUARE_SIZE_PIXELS/2; i > 0; i -= 2) {
                StdDraw.rectangle(
                        centre[0],
                        centre[1],
                        SieveVisualisation.pixelsToProportion(i, true),
                        SieveVisualisation.pixelsToProportion(i, false)
                );
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            StdDraw.setPenColor(color);
            StdDraw.filledRectangle(
                    centre[0],
                    centre[1],
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS/3, true),
                    SieveVisualisation.pixelsToProportion(Grid.SQUARE_SIZE_PIXELS/3, false)
            );
            StdDraw.setPenColor(SieveVisualisation.COLOUR_ONE);
            StdDraw.text(centre[0], centre[1], Integer.toString(number));
        }
    }

    //void

    public void setColor(Color color) {
        this.color = color;
    }
}
