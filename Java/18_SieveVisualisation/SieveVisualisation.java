import java.awt.Color;

public class SieveVisualisation {
    public final static Color COLOUR_ONE = new Color(25, 26, 25);
    public final static Color COLOUR_THREE = new Color(100, 100, 100);
    public final static Color COLOUR_TWO = new Color(150, 150, 150);
    public final static Color COLOUR_FOUR = new Color(220, 220, 220);

    protected NumberInSieve[] steps;
    private Grid grid;
    public static int[] canvasSize;

    SieveVisualisation(NumberInSieve[] steps) {
        this.setSteps(steps);
    }

    public void visualise() {
        grid = new Grid(steps.length);
        canvasSize = new int[2];
        canvasSize[0] = (int) (grid.dimensions[0] * Grid.SQUARE_SIZE_PIXELS * 1.7);
        canvasSize[1] = grid.dimensions[1] * Grid.SQUARE_SIZE_PIXELS;
        grid.setCentres();
        StdDraw.setCanvasSize(canvasSize[0], canvasSize[1]);
        StdDraw.clear(COLOUR_ONE);
        StdDraw.show(0);
        grid.drawGrid();
        StdDraw.show();

        Color currentColour = new Color(204, 102, 102);
        float[] hsbVals = new float[3];
        Color.RGBtoHSB(currentColour.getRed(), currentColour.getGreen(), currentColour.getBlue(), hsbVals);
        hsbVals[0] += Math.random();
        if (hsbVals[0] >=1) {hsbVals[0] -= 1;}
        currentColour = new Color(Color.HSBtoRGB(hsbVals[0], hsbVals[1], hsbVals[2]));

        PrimeSideBar side = new PrimeSideBar(grid);
        for (int step = 0; step < steps.length; step++) {
            if (steps[step].isPrime()) {
                if ((step < steps.length - 1 && steps[step+1].isPrime()) || step == steps.length - 1) {
                    int number = steps[step].getNumber();
                    grid.cells[number - 2].setColor(COLOUR_FOUR);
                    grid.cells[number - 2].drawCell(true);
                }
                else {
                    Color.RGBtoHSB(currentColour.getRed(), currentColour.getGreen(), currentColour.getBlue(), hsbVals);
                    hsbVals[0] += 0.61803398875;
                    if (hsbVals[0] >= 1) {
                        hsbVals[0] -= 1;
                    }
                    currentColour = new Color(Color.HSBtoRGB(hsbVals[0], hsbVals[1], hsbVals[2]));
                    int number = steps[step].getNumber();
                    grid.cells[number - 2].setColor(currentColour);
                    grid.cells[number - 2].drawCell(true);
                }
                side.addPrime(steps[step].getNumber());
            }
            else {
                //currentColour = new Color(Color.HSBtoRGB(hsbVals[0], hsbVals[1], hsbVals[2]));
                int number = steps[step].getNumber();
                grid.cells[number - 2].setColor(currentColour);
                grid.cells[number - 2].drawCell(false);
            }
            try {
                Thread.sleep(70);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void setSteps(NumberInSieve[] steps) {
        this.steps = steps;
    }

    public static  double pixelsToProportion(int pixels, boolean isInXSense) {
        return (pixels / ((double) canvasSize[isInXSense? 0 : 1]));
    }

    public static  double pixelsToProportion(double pixels, boolean isInXSense) {
        return (pixels / ((double) canvasSize[isInXSense? 0 : 1]));
    }

}
