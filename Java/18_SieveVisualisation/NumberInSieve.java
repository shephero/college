public class NumberInSieve {
    private int updateOrder;
    private int number;
    private boolean isPrime;
    private int crossedOffBy;

    // All numbers start off prime, then are crossed off.
    NumberInSieve(int inputNumber) {
        setPrime(true);
        setNumber(inputNumber);
    }

    // Called when shown not to be prime.
    void setNotPrime(int inputUpdateOrder, int inputCauseOfPrime) {
        setPrime(false);
        setUpdateOrder(inputUpdateOrder);
        setCrossedOffBy(inputCauseOfPrime);
    }

    void setPrime(int inputUpdateOrder) {
        setUpdateOrder(inputUpdateOrder);
    }

    void print() {
        System.out.println("Number: " + getNumber());
        System.out.println("Update order: " + getUpdateOrder());
        System.out.println("Is prime: " + isPrime());
        if (!isPrime()) {
            System.out.println("Crossed off by: " + getCrossedOffBy());
        }
    }

    public int getUpdateOrder() {
        return updateOrder;
    }

    public void setUpdateOrder(int updateOrder) {
        this.updateOrder = updateOrder;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public void setPrime(boolean prime) {
        isPrime = prime;
    }

    public int getCrossedOffBy() {
        return crossedOffBy;
    }

    public void setCrossedOffBy(int crossedOffBy) {
        this.crossedOffBy = crossedOffBy;
    }
}
