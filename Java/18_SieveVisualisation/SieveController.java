public class SieveController {
    private int maxNumber;
    //SieveInputView slider;
    SieveSimulation simulation;
    SieveVisualisation visualisation;

//    Spawn an input dialog and simulation.
    SieveController() {
    }

    void run() {
        maxNumber = SliderDialog.getInt();
        //maxNumber =
        simulation = new SieveSimulation(maxNumber);
        simulation.simulate();
        visualisation = new SieveVisualisation(simulation.getOrderedSieve());
        visualisation.visualise();
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }
}
