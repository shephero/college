public class SieveSimulation {
    private NumberInSieve[] numbersInSieve;
    private NumberInSieve[] orderedSieve;
    private int maxNumber;

    SieveSimulation(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    void simulate() {
        setNumbersInSieve(new NumberInSieve[getMaxNumber() - 1]);
        for (int i = 0; i < getMaxNumber() - 1; i++) {
            getNumbersInSieve()[i] = new NumberInSieve(i + 2);
        }

//        I try not to look at the following nest of code. If it's not broken don't fix it.
        int updateOrder = 0;
        for (int i = 0; i < getMaxNumber() - 1; i++) {
            if (getNumbersInSieve()[i].isPrime()) {
                getNumbersInSieve()[i].setPrime(updateOrder);
                updateOrder++;
                for (int j = i; getNumbersInSieve()[j].getNumber() <= getMaxNumber() / getNumbersInSieve()[i].getNumber(); j++) {
                    if (getNumbersInSieve()[getNumbersInSieve()[i].getNumber() * getNumbersInSieve()[j].getNumber() - 2].isPrime()) {
                        getNumbersInSieve()[getNumbersInSieve()[i].getNumber() * getNumbersInSieve()[j].getNumber() - 2].setNotPrime(updateOrder, i + 2);
                        updateOrder++;
                    }
                }
            }
        }
        order();
        //printOrdered();
    }

    void print() {
        for(NumberInSieve i : getNumbersInSieve()) {
            i.print();
            System.out.println();
        }
    }

    void printOrdered() {
        for(NumberInSieve i : getOrderedSieve()) {
            i.print();
            System.out.println();
        }
    }

    void order() {
        setOrderedSieve(new NumberInSieve[getNumbersInSieve().length]);
        int currentStep = 0;
        while (currentStep < getNumbersInSieve().length) {
            for (NumberInSieve i : getNumbersInSieve()) {
                if (currentStep == i.getUpdateOrder()) {
                    getOrderedSieve()[currentStep] = i;
                    currentStep++;
                }
            }
        }
    }

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public NumberInSieve[] getNumbersInSieve() {
        return numbersInSieve;
    }

    public void setNumbersInSieve(NumberInSieve[] numbersInSieve) {
        this.numbersInSieve = numbersInSieve;
    }

    public NumberInSieve[] getOrderedSieve() {
        return orderedSieve;
    }

    public void setOrderedSieve(NumberInSieve[] orderedSieve) {
        this.orderedSieve = orderedSieve;
    }
}
