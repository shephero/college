import java.util.Scanner;
import java.util.stream.IntStream;

public class SieveOfEratosthenes {
    private static boolean debug = false;

    private static int[] createSequence(int limit) {
        int[] initialisedArray = IntStream.rangeClosed(2, limit).toArray();
        return initialisedArray;
    }

    private static void crossOutHigherMultiples(int[] possiblePrimes, int baseNumber) {
        int biggestNumber = Math.abs(possiblePrimes[possiblePrimes.length - 1]);
        if (debug) {
            System.out.println("Crossing out multiples of " + baseNumber);
        }
        for (int j : possiblePrimes) {
            int numberToCrossOff = baseNumber * Math.abs(j);
            if (numberToCrossOff > biggestNumber) {break;}
            int index = numberToCrossOff - 2;
            if (possiblePrimes[index] > 0) {
                possiblePrimes[index] = possiblePrimes[index] * (-1);
            }
            if (debug) {
                System.out.print(possiblePrimes[index] + " ");
            }
        }
    }

    private static int[] sieve(int limit) {
        int possiblePrimes[] = createSequence(limit);
        for (int i : possiblePrimes) {
            if (i > 0) {
                crossOutHigherMultiples(possiblePrimes, i);
            }
        }
        return possiblePrimes;
    }

    private static String nonCrossedOutSubseqToString(int[] primeList) {
        String output = "";
        boolean insertComma = false;
        for (int i : primeList) {
            if (i > 0) {
                if (insertComma) {output += "\n";}
                output += i;
            }
            insertComma = true;
        }
        return output;
    }

    private static String sequenceToString(int[] primeList) {
        String output = "";
        boolean insertComma = false;
        for (int i : primeList) {
            if (insertComma) {output += ", \n";}
            if (i > 0) {
                output += i;
            }
            else {
                output += ("[" + Math.abs(i) + "]");
            }

            insertComma = true;
        }
        return output;
    }

    public static void main(String[] args) {
        boolean validNumber = false;
        int limit = 0;
        Scanner inputScanner = new Scanner(System.in);
        while (!validNumber) {
            System.out.println("Find all prime numbers up to (enter number):");
            System.out.print(" -> ");

            if (inputScanner.hasNextInt()) {
                limit = inputScanner.nextInt();
                validNumber = true;
            }
            else {
                System.out.println("That's not a valid input (try an integer).");
            }
            inputScanner.nextLine();
        }
        int[] primeList = sieve(limit);

        if (debug) {
            System.out.println("Limit = " + limit);
            System.out.println("Possible primes up to limit:");
            for (int i : primeList) {
                System.out.print(i + " ");
            }
        }
        boolean validAnswer = false;
        while (!validAnswer) {
            System.out.println("Output non-primes as well? (y/n)");
            System.out.print(" -> ");
            String answer = inputScanner.nextLine();
            if (answer.equals("n")) {
                System.out.println(nonCrossedOutSubseqToString(primeList));
                validAnswer = true;
            }
            else if (answer.equals("y")) {
                System.out.println(sequenceToString(primeList));
                validAnswer = true;
            }
            else {
                System.out.println("That's not a valid answer, please enter either 'y' or 'n'.");
            }
        }
    }
}
