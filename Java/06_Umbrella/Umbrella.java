import javax.swing.JOptionPane;
public class Umbrella {

	public static void main(String[] args) {
		
		int raining = JOptionPane.showConfirmDialog(null, "Is it raining?");
		if (raining == JOptionPane.YES_OPTION)
		{
			JOptionPane.showMessageDialog(null, "It's raining, bring an umbrella and put it up.");
		}
		else if (raining == JOptionPane.CLOSED_OPTION | raining == JOptionPane.CANCEL_OPTION)
		{
			
		}
		else
		{
			
			int willRain = JOptionPane.showConfirmDialog(null, "Does it look like it will rain later?");
			if (willRain == JOptionPane.YES_OPTION)
			{
				JOptionPane.showMessageDialog(null, "It looks like it might rain, bring an umbrella.\nDon't put it up just yet though.");
			}
			else if (willRain == JOptionPane.CLOSED_OPTION | willRain == JOptionPane.CANCEL_OPTION)
			{
				
			}
			else
			{
				JOptionPane.showMessageDialog(null, "It's pretty dry outside, don't bring an umbrella.");
			}
			
		}
		
	}
	
}
