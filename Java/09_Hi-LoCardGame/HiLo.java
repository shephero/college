import java.util.HashMap;
import java.util.Scanner;

public class HiLo {

	private static HashMap<Integer, String> cardDescriptions;
	private static HashMap<Integer, String> numberWords;
	
	public static void main(String[] args) {
		initialiseCardDescriptions();
		initialiseNumberWords();
		HiLo instance = new HiLo();
		instance.runGame();
	}
	
	private static void initialiseNumberWords() {

		numberWords = new HashMap<Integer, String>();
		numberWords.put(0, "zero");
		numberWords.put(1, "one");
		numberWords.put(2, "two");
		numberWords.put(3, "three");
		numberWords.put(4, "four");
	}

	private static void initialiseCardDescriptions() {

		cardDescriptions = new HashMap<Integer, String>();
		cardDescriptions.put(1, "an ace");
		cardDescriptions.put(2, "a two");
		cardDescriptions.put(3, "a three");
		cardDescriptions.put(4, "a four");
		cardDescriptions.put(5, "a five");
		cardDescriptions.put(6, "a six");
		cardDescriptions.put(7, "a seven");
		cardDescriptions.put(8, "an eight");
		cardDescriptions.put(9, "a nine");
		cardDescriptions.put(10, "a ten");
		cardDescriptions.put(11, "a jack");
		cardDescriptions.put(12, "a queen");
		cardDescriptions.put(13, "a king");

		// return cardDescriptions;
	}

	private void runGame() {
		int gamesWon = 0;
		System.out.println("                            _         _   ");
		System.out.println(" _____ _     __           _|_|       |_|_ ");
		System.out.println("|  |  |_|___|  |   ___   | |  ___ ___  | |");
		System.out.println("|     | |___|  |__| . |  | | | -_| . | | |");
		System.out.println("|__|__|_|   |_____|___|  |_|_|___|_  |_|_|");
		System.out.println("                           |_|     |_|_|  ");
		// http://patorjk.com/software/taag/ used to generate ASCII art
		for (int i = 0; i < 15; i++) {
			System.out.print(". ");
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}			// Prints ". . . . . . . . . . . . . . . . . . . . "
		System.out.println("Version 1.0");
		System.out.println("\nUsing ruleset 3.0:\nAces are low.\n"
				+ "You must win four rounds to win the game.");
		System.out.println();
		while (gamesWon < 4) {
			gamesWon = playGames(gamesWon);
		}
		printCongrats();
	}

	private static void printCongrats() {
		System.out.println("                              __ ");
		System.out.println(" __ __                  _    |  |");
		System.out.println("|  |  |___ _ _    _ _ _|_|___|  |");
		System.out.println("|_   _| . | | |  | | | | |   |__|");
		System.out.println("  |_| |___|___|  |_____|_|_|_|__|");
		System.out.println("                       (The game)");

		// http://patorjk.com/software/taag/ used to generate ASCII art
	}

	private static String getAnswer() {
		String input = "";
		Scanner inputScanner = new Scanner(System.in);
		while (inputScanner.hasNextLine()) {
			input = inputScanner.next();
			if (input.equals("hi") || input.equals("lo") || input.equals("eq")) {
				return input;
			} else {
				return "invalid";
			}
		}
		inputScanner.close();
		return "finished";
	}

	private static boolean playGame() {
		int cardNumber = (int) (Math.random() * 13 + 1);
		String cardString = cardToString(cardNumber);
		System.out.println("I have selected a card between an ace and a king.");
		System.out.println("It is " + cardString + ".");
		System.out
				.println("Do you think the next card will be higher, lower or equal to "
						+ cardString + "?  (hi/lo/eq)");

		String hilo;
		do {
			hilo = getAnswer();
			System.out.println("The answer recorded was "
					+ validityConjugation(hilo));
		} while (hilo.equals("invalid"));

		int cardNumberTwo = (int) (Math.random() * 13 + 1);
		System.out.println("The second card was " + cardToString(cardNumberTwo)
				+ ".");
		if (hilo.equals("hi")) {
			return (cardNumberTwo > cardNumber);
		} else if (hilo.equals("eq")) {
			return (cardNumberTwo == cardNumber);
		} else {
			return (cardNumberTwo < cardNumber);
		}
	}

	private static String cardToString(int anInteger) {
		return cardDescriptions.get(anInteger);
	}
	
	private static String numberToWord(int anInteger) {
		return numberWords.get(anInteger);
	}

	private static String validityConjugation(String hilo) {
		return hilo.equals("invalid") ? "invalid. Please try again with 'hi', 'lo' or 'eq'."
				: hilo + ".";
	}

	private static int playGames(int gamesWon) {
		boolean roundWon = playGame();
		if (roundWon) {
			gamesWon++;
			System.out.println("You win this round, you've won " + numberToWord(gamesWon)
					+ " so far.\n");
		} else {
			if (gamesWon == 0) {
				System.out
						.println("You lose this round. You're still on zero wins.\n");
			} else {
				gamesWon = 0;
				System.out
						.println("You lose this round. Resetting wins to zero.\n");
			}
		}
		return gamesWon;
	}
}