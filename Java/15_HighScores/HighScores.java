import java.util.Scanner;
import java.util.Arrays;

public class ScoreMaintainer {

	public static void main(String[] args) {
		Scanner systemIn = new Scanner(System.in);
		int[] scores;
		int numberOfScores;
		while (true) {
			try {
				numberOfScores = getScoreboardLength(systemIn);
				scores = initialiseHighScores(numberOfScores);
				break;
			}
			catch (java.lang.OutOfMemoryError noMoreMemory) {
				System.out.println("Not enough memory to maintain a scoreboard that big.");
				numberOfScores = 0;
			}
		}
		
		while (true) {
			int score = getScore(systemIn);
			if (higherThan(scores, score))
				insertScore(scores, score);
			if (score == -1) {
				printHighScores(scores, true);
				System.out.println("Bye Bye.");
				break;
			}
			printHighScores(scores, false);
		}
		systemIn.close();
	}
	
	private static int getScore(Scanner scoreInput) {
		System.out.println("Input a score (or press 'q' to exit):");
		while (true) {
			if (scoreInput.hasNextInt()) {
				int proposedInt = scoreInput.nextInt();
				if (proposedInt > 0) {
					return proposedInt;
				}
				else {
					System.out.println("Scores less than one are forbidden, try again.");
				}
			}
			else if (scoreInput.hasNext()) {
				if (scoreInput.next().equals("q")) {
					return -1;
				}
				System.out.println("That's not a score. Try again.");
			}
		}
	}

	private static int getScoreboardLength(Scanner boardInput) {
		while (true) {
			System.out.println("How many scores would you like to maintain?");
			if (boardInput.hasNextInt()) {
				int scoreboards = boardInput.nextInt();
				if (scoreboards > 0) {
					return scoreboards;
				}
				else {
					System.out.println("Numbers less than zero are not valid. Try again.");
				}
			}
			else {
				System.out.println("That's not a number. Try again");
				boardInput.next();
			}
		}
	}
	
	private static int[] initialiseHighScores(int numberOfScores) {
		System.out.println("Setting up a scoreboard of size " + numberOfScores + ".");
		int[] scores = new int[numberOfScores];
		for (int i = 0; i < numberOfScores; i++) {
			scores[i] = 0;
		}
		return scores;
	}
	
	private static boolean higherThan(int[] scores, int score) {
		//System.out.println(scores[scores.length-1]);
		return (scores[scores.length - 1] < score);
	}
	
	private static void insertScore(int[] scores, int score) {
		int position = 0;
		for (int i = scores.length - 1; i >= 0; i--) {
			if (score > scores[i]) {
				position = i;
			}
		}
		for (int i = scores.length - 1; i > position; i--) {
			scores[i] = scores[i-1];
		}
		scores[position] = score;
	}
	
	private static void printHighScores(int[] scores, boolean isFinal) {
		if (scores[0] == 0) {
			System.out.println("There are no scores to print.");
		}
		else {
			if (isFinal)
				System.out.print("The final high scores are: ");
			else
				System.out.print("The high scores are: ");
			int numberOfScores = countScores(scores);
			for (int i = 0; i < numberOfScores; i++) {
				if (i == numberOfScores - 1)
					System.out.println(scores[i] + ".");
				else 
					System.out.print(scores[i] + ", ");
			}
			//System.out.println();
		}
	}
	
	private static int countScores(int[] scores) {
		int numberOfScores = 0;
		for (int i: scores) {
			if (i != 0)
				numberOfScores++;
		}
		return numberOfScores;
	}
}
