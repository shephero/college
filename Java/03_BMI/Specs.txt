Write a program which compute the Body Mass Index (BMI) of a person.
The BMI is computed by dividing the weight of a person (in kgs) by the square of the height of the person in meters.
