import java.util.Scanner;

public class BodyMassIndex {

	public static void main(String[] args) {
		System.out.println("What Is Your Height? ");
		Scanner scanner = new Scanner(System.in);
		float height = scanner.nextFloat();
		
		System.out.println("What Is Your Weight? ");
		float weight = scanner.nextFloat();
		
		float bodyMassIndex = weight/(height * height);
		System.out.println("Your BMI is " + bodyMassIndex + ".");
		scanner.close();
			
		String health = "";
		if (bodyMassIndex < 18.5)
		{
			health = "... Do you even lift?";
		}
		else if (bodyMassIndex < 25)
		{
			health = "... Normal, I guess.";
		}
		else if (bodyMassIndex < 30)
		{
			health = "... Who ate all the pies?";
		}
		else
		{
			health = "... You're just big boned.";
		}
		
		System.out.print(health);
	}
}