Write a program to determine what class (amphibian, reptile, fish, mammal, or bird) a vertebrate is..

Class      Blood  Covering     Fins?
-----      -----  --------     -----
Amphibian  Cold   Moist skin   No fins
Reptile    Cold   Scales       No fins
Fish       Cold   Scales       Fins
Mammal     Warm   Hair or fur  No fins
Bird       Warm   Feathers     No fins
