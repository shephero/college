package livingClassification;

import javax.swing.JOptionPane;

public class LivingClassification {

	public static void main(String[] args) {
		String livingThing = "";
		int answer =  JOptionPane.showConfirmDialog(null, "Is your creature warm blooded?");
		if (answer == JOptionPane.YES_OPTION)
		{
			answer = JOptionPane.showConfirmDialog(null, "Does your creature have feathers?");
			if (answer == JOptionPane.YES_OPTION)
			{
				livingThing = "Bird";
			}
			else
			{
				livingThing = "Mammal";
			}
		}
		else
		{
			answer = JOptionPane.showConfirmDialog(null, "Does your creature have moist skin?");
			if (answer == JOptionPane.YES_OPTION)
			{
				livingThing = "Amphibian";
			}
			else
			{
				answer = JOptionPane.showConfirmDialog(null, "Does your creature have scales?");
				if (answer == JOptionPane.YES_OPTION)
				{
					livingThing = "Fish";
				}
				else
				{
					livingThing = "Reptile";
				}
			}
		}
		
		if (livingThing == "Amphibian")
		{
			JOptionPane.showMessageDialog(null, "Your creature is an " + livingThing + ".");
		}
		else
		{	
			JOptionPane.showMessageDialog(null, "Your creature is a " + livingThing + ".");
		}
	}
}