Dependencies:
Java JDK 8     (May work with later versions)
bash           (only for the compile-and-run command provided)

## To compile and run ##
$ javac *.java && java Main && rm *.class

## To compile only ##
$ mkdir -p bytecode
$ javac *.java -d bytecode

## To run ##
$ cd bytecod
$ java Main