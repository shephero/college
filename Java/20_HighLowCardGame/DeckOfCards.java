/* ---------------------------------------------------------------------------------------------------------------- */
/* DeckOfCards class which implements the DeckOfCardsI interface.                                                   */
/* ---------------------------------------------------------------------------------------------------------------- */

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DeckOfCards implements DeckOfCardsI {
    /** The collection of playing cards in the deck. */
    List<PlayingCardI> cards;
    boolean isEmpty;

    /** Build a new deck of playing cards containing the standard 52 different playing cards arranged by suit in */
    /** rank order, that is, not shuffled. *//** Invoke above constructor with parameter false. */
    public DeckOfCards() {
        cards = new ArrayList(52);
        for (int i = 0; i < 52; i++) {
            cards.add(new PlayingCard(
                    PlayingCardI.Suit.values()[i % PlayingCardI.Suit.values().length],
                    PlayingCardI.Rank.values()[i % PlayingCardI.Rank.values().length]
            ));
        }
        makeRemainingFaceDown();
        shuffle();
        isEmpty = false;
    }

// Instance (object) method defs:
    /** Have all the playing cards in this deck been dealt?  */
    public boolean isEmpty() { return isEmpty; }

    /** Return the top playing card from this deck of playing cards assuming this deck is non-empty; otherwise the */
    /** playing cards that have been previously dealt are re-shuffled and the new top playing card is dealt.       */
    public PlayingCardI deal() {
        PlayingCardI card = cards.get(0);
        cards.remove(0);
        isEmpty = cards.size() <= 0;
        return card;
    }

    /** Make each remaining playing card in this deck face down. */
    public void makeRemainingFaceDown() {
        cards.stream()
                .forEach(card -> card.makeFaceDown());
    }

    /** Randomly shuffle the remaining playing cards in this deck. */
    public void shuffle() {
        Collections.shuffle(cards);
    }


    /** Convert the deck of playing cards to a String object containing pictographs of each playing card. */
    public String toPictographs() {
        String result = "";
        for (PlayingCardI card : cards) {
            result += card.toPictograph() + "\n";
        }
        return result.trim();
    }

    /** Convert the deck of playing cards to a String object containing description of each playing card. */
    public String toString() {
        String result = "";
        for (PlayingCardI card : cards) {
            result += card.toString() + "\n";
        }
        return result.trim();
    }

// Non-instance (class) (static) method defs:
    /** A test client which builds a deck of playing cards without playing card of Knight rank and then prints the  */
    /** deck of playing cards to the standard output. *//** The deck is then shuffled and again printed to the      */
    /** standard output. *//** Twenty six playing cards are dealt face up from the deck and printed to the standard */
    /** output followed by the deck of playing cards. *//** Again, twenty six playing cards are dealt face up from  */
    /** the deck and printed to the standard output followed by the deck of playing cards. *//** Finally, a single  */
    /** card is dealt from the deck and both the card and the deck are printed to the standard output. */
    public static void main(String[] args) {
        DeckOfCardsI deck = new DeckOfCards();
        System.out.println("\n" + deck + "\n");
        deck.shuffle();
        System.out.println(deck + "\n");
        for(int i = 0; i < 26; i++) System.out.println(deck.deal().flip());
        System.out.println();
        System.out.println(deck + "\n");
        for(int i = 0; i < 26; i++) System.out.println(deck.deal().flip());
        System.out.println();
        System.out.println(deck + "\n");
        System.out.println(deck.deal().flip());
        System.out.println();
        System.out.println(deck + "\n");
        System.out.println(deck.toPictographs());
    }
}
