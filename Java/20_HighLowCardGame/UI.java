import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class UI {
    JDialog currentScreen;
    DeckOfCardsI cards;
    PlayingCardI previousCard;
    PlayingCardI currentCard;
    boolean guessedHigher;

    UI(DeckOfCardsI cards) {
        this.cards = cards;
        currentCard = cards.deal();
        makeStateDisplay();
    }

    private void makeStateDisplay() {
        if (cards.isEmpty()) {
            makeEndDisplay();
        }
        else {
            currentScreen = getBaseWindow();
            currentScreen.setLayout(new GridLayout(4, 1));
            JLabel deckMessage = new JLabel("I have a deck of cards.", SwingConstants.CENTER);
            JLabel flippedMessage = new JLabel("I have flipped over the top card.", SwingConstants.CENTER);
            JLabel cardMessage = new JLabel("It is " + currentCard.prefix() + " " + currentCard.toString(), SwingConstants.CENTER);
            JButton higherButton = new JButton("Higher");
            JButton lowerButton = new JButton("Lower");
            JButton quitButton = new JButton("Quit");
            JPanel buttonPanel = new JPanel();
            buttonPanel.add(higherButton);
            buttonPanel.add(lowerButton);
            buttonPanel.add(quitButton);
            currentScreen.add(deckMessage);
            currentScreen.add(flippedMessage);
            currentScreen.add(cardMessage);
            currentScreen.add(buttonPanel);

            higherButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    guessedHigher = true;
                    currentScreen.dispose();
                    makeOutcomeDisplay();
                }
            });

            lowerButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    guessedHigher = false;
                    currentScreen.dispose();
                    makeOutcomeDisplay();
                }
            });

            quitButton.addActionListener(action -> System.exit(0));
            centerWindow(currentScreen);
        }
    }

    private void makeOutcomeDisplay() {
        previousCard = currentCard;
        currentCard = cards.deal();
        currentScreen = getBaseWindow();
        currentScreen.setLayout(new GridLayout(4, 1));
        String message = "";
        if (guessedHigher) {
            if (currentCard.compareTo(previousCard) > 0) {
                message = "You win!";
            }
            else { message = "You lose"; }
        }
        else {
            if (currentCard.compareTo(previousCard) < 0) {
                message = "You win!";
            }
            else { message = "You lose"; }
        }
        JLabel newCard = new JLabel(
                "The card flipped over was " +
                        currentCard.prefix() + " " +
                        currentCard.toString(), SwingConstants.CENTER);
        JLabel oldCard = new JLabel(
                "The previous card was " +
                        previousCard.prefix() + " " +
                        previousCard.toString(), SwingConstants.CENTER);
        JButton quitButton = new JButton("Quit");
        JButton continueButton = new JButton("Continue");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(continueButton);
        buttonPanel.add(quitButton);
        currentScreen.add(newCard, SwingConstants.CENTER);
        currentScreen.add(oldCard, SwingConstants.CENTER);
        currentScreen.add(new JLabel(message, SwingConstants.CENTER));
        currentScreen.add(buttonPanel);

        continueButton.addActionListener(action -> {
            currentScreen.dispose();
            makeStateDisplay();
        });
        quitButton.addActionListener(changeEvent -> System.exit(0));
        centerWindow(currentScreen);
    }

    private void centerWindow(JDialog p) {
        p.setLocationRelativeTo(null);
    }

    private void makeEndDisplay() {
        currentCard = cards.deal();
        currentScreen = getBaseWindow();
        currentScreen.setLayout(new GridLayout(2, 1));
        JLabel stateDisplay = new JLabel("There are no cards left", SwingConstants.CENTER);
        JButton quitButton = new JButton("Quit");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(quitButton);
        currentScreen.add(stateDisplay);
        currentScreen.add(buttonPanel);
        quitButton.addActionListener(changeEvent -> System.exit(0));
        centerWindow(currentScreen);
    }

    public void display() {
        currentScreen.pack();
        currentScreen.setVisible(true);
    }

    private JDialog getBaseWindow() {
        JDialog window = new JDialog();
        window.setModal(true);
        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
        window.setTitle("High-Low card game");
        return window;
    }
}
