/* ---------------------------------------------------------------------------------------------------------------- */
/* PlayerCard class which implements the PlayingCardI interface.                                                    */
/* ---------------------------------------------------------------------------------------------------------------- */

import java.lang.String;

public class PlayingCard implements PlayingCardI {
// Instance (object) variable defs:
    /** The suit of this playing card. */
    private final Suit suit;

    /** The rank of this playing card. */
    private final Rank rank;

    /** Is the playing card face up? */
    private boolean isFaceUp;

    /** Unique ID for this PlayingCard object. */
    private int id;

// Constructor method defs:
    /** Construct playing card object with the given suit and rank. */
    public PlayingCard(Suit suit, Rank rank) { /* Write your code for the method body here! */
        this.suit = suit;
        this.rank = rank;
    }

// Instance (object) method defs:
    /** Return the suit of this playing card. */
    public Suit suit() { return suit; }

    /** Return the rank of this playing card. */
    public Rank rank() { return rank; }

    /** Return true if this playing card is face up; otherwise false. */
    public boolean isFaceUp() { return isFaceUp; }

    /** Flip this playing card: if this playing card is face down it becomes face up; otherwise if this playing card */
    /** is face up it becomes face down; return this playing card after flipping it. */
    public PlayingCardI flip() {
        isFaceUp = !isFaceUp;
        return this;
    }

    /** Make this playing card face down. */
    public void makeFaceDown() { isFaceUp = false; }

    /** Convert this playing card to a String object. */
    @Override public String toString() {
        return (rank.toString() + " of " + suit.toString()).toLowerCase();
    }

    public String prefix() {
        switch (toString().charAt(0)) {
            case 'a':
                return "an";
            case 'i':
                return "an";
            case 'o':
                return "an";
            case 'u':
                return "an";
            case 'e':
                return "an";
            default:
                return "a";
        }
    }

    public char getPic() {
        int result = 0;
        switch(suit) {
            case SPADES:
                result = 0x1F0A;
                break;
            case HEARTS:
                result = 0x1F0B;
                break;
            case DIAMONDS:
                result = 0x1F0C;
                break;
            case CLUBS:
                result = 0x1F0d;
                break;
        }
        result *= 16;
        result += rank().ordinal();
        return (char) result;
    }

    /** Note: Since the {@code PlayingCardI} interface "is a specialisation of" (extends) the                        */
    /** {@code Comparable<PlayingCardI>} interface any class which implements the {@code PlayingCardI} interface     */
    /** must also implement the {@code Comparable<PlayingCardI>} interface, that is, your implementing class must    */
    /** also provide a definition for the {@code compareTo} method (operation) which compares this playing card with */
    /** the given playing card for order and returns -1, 0, or +1 as this playing card is less than, equal to, or    */
    /** greater than the given playing card.                                                                         */
    public int compareTo(PlayingCardI card) {
        return Integer.compare(this.rank().ordinal(), card.rank().ordinal());
    }

    /** Convert this playing card to a pictograph String object. */
    public String toPictograph() {
        return this.isFaceUp ? this.pictographs[this.suit().ordinal()][this.rank().ordinal()]
                : this.backOfPlayingCard;
    }

// Non-instance (class) (static) variable defs:
    /** Back of playing card pictograph as a String object. */
    private static final String backOfPlayingCard;

    /** Playing card pictographs as String objects stored in a Suit-by-Rank two dimensional array. */
    private static final String[][] pictographs;

    /** Number of PlayingCard objects created. */
    private static int numberOfPlayingCardObjects;

// Non-instance (class) (static) variable initialisations:
    /** Initialise the back and front pictographs for the playing cards. */
    static { /* begin static variable initialisation. */
        int backOfPlayingCardUnicodePoint = 0x1F0A0;
        int[] unicodePoints = { backOfPlayingCardUnicodePoint };
        backOfPlayingCard = new String(unicodePoints, 0, 1);

        pictographs = new String [Suit.values().length][Rank.values().length];
        int unicodePoint = 0x1F0A1; /* Ace of spades playing card Unicode point (in hexadecimal). */
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                unicodePoints[0] = unicodePoint + rank.ordinal();
                pictographs[suit.ordinal()][rank.ordinal()] = new String(unicodePoints, 0, 1);
            }
            unicodePoint += 0x10; /* Ace of Hearts then Ace of Diamonds then Ace of Clubs. */
        }
        numberOfPlayingCardObjects = 0;
    }

// Non-instance (class) (static) method defs:
    /** A test client which builds a playing card of each rank and suit; flips the playing card and then prints */
    /** the pictographs for the playing card to the standard output. */
    public static void main(String[] args) {
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                PlayingCardI card = new PlayingCard(suit, rank);
                card.flip();
                System.out.println(card + " " + card.toPictograph());
                /* System.out.println(card); // Use if UTF-16 encoding is not enabled. */
            }
            System.out.println();
        }
    }
} /* end PlayingCard class */
