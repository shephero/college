/* ---------------------------------------------------------------------------------------------------------------- */
/* DeckOfCardsI interface abstracting the operation a deck of playing cards data type must provide.                 */
/* ---------------------------------------------------------------------------------------------------------------- */

public interface DeckOfCardsI {
// Instance (object) method signatures:
    /** Have all the playing cards in this deck been dealt?  */
    public boolean isEmpty();

    /** Return the top playing card from this deck of playing cards assuming this deck is non-empty; otherwise the */
    /** playing cards that have been previously dealt are re-shuffled and the new top playing card is dealt. */
    public PlayingCardI deal();

    /** Make each remaining playing card in this deck face down. */
    public void makeRemainingFaceDown();

    /** Randomly shuffle the remaining playing cards in this deck. */
    public void shuffle();

    /** Convert the deck of playing cards to a String object containing pictographs of each playing card. */
    public String toPictographs();
} /* end DeckOfCardsI interface */
