/* ---------------------------------------------------------------------------------------------------------------- */
/* PlayingCardI interface which abstracts the operations a playing card data type must provide.                     */
/* ---------------------------------------------------------------------------------------------------------------- */

import java.lang.Comparable; /** A generic interface to impose a total ordering on the objects of the class that is */
/** required to implement it. */

public interface PlayingCardI extends Comparable<PlayingCardI> {
// Non-instance (class) (static) inner classes:
    /** Note: Any class that implements the {@code PlayingCardI} interface will be able to use the following */
    /** enumerations (data types). */

    /** {@code Suit} enumeration (data type) whose values represent the suits of playing cards. */
    public static enum Suit {
        SPADES, HEARTS, DIAMONDS, CLUBS;
    }

    /** {code Rank} enumeration (data type) whose values represent the ranks of playing cards in increasing order */
    /** of value. */
    public static enum Rank {
        ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;
    }

// Instance (object) method signatures:
    /** Note: By "this playing card" I mean the object which this method (operation) is applied to. */
    /** Return the suit of this playing card. */
    public Suit suit();

    /** Return the rank of this playing card. */
    public Rank rank();

    /** Return true if this playing card is face up; otherwise false. */
    public boolean isFaceUp();

    /** Flip this playing card: if this playing card is face down it becomes face up; otherwise if this playing card */
    /** is face up it becomes face down; return this playing card after flipping it. */
    public PlayingCardI flip();

    /** Make this playing card face down. */
    public void makeFaceDown();

    /** Convert this playing card to a pictograph String object. */
    public String toPictograph();

    /** Note: Since the {@code PlayingCardI} interface "is a specialisation of" (extends) the                        */
    /** {@code Comparable<PlayingCardI>} interface any class which implements the {@code PlayingCardI} interface     */
    /** must also implement the {@code Comparable<PlayingCardI>} interface, that is, your implementing class must    */
    /** also provide a definition for the {@code compareTo} method (operation) which compares this playing card with */
    /** the given playing card for order and returns -1, 0, or +1 as this playing card is less than, equal to, or    */
    /** greater than the given playing card.                                                                         */
    public int compareTo(PlayingCardI card);

    //returns "an" or "a" depending on whether the card's name begins with a vowel.
    public String prefix();
} /* end PlayingCardI interface */
