import java.util.Scanner;
public class CircleMetrics {

	public static void main(String[] args) {
		System.out.print("Input a circle radius: ");
		Scanner input = new Scanner(System.in);
		float radius = input.nextFloat();
		
		double circumference = 2 * Math.PI * radius;
		double area = Math.PI * radius * radius;
		
		System.out.println("\nThe circumference of your cirlce is " + circumference);
		System.out.println("The area of your cirlce is " + area);
		input.close();
	}

}
