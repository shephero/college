
public class XDaysOfChristmas {

	public static void main(String[] args) {
		for (int i = 1; i < 13; i++) {
			printVerse(i);
		}
	}

	private static void printVerse(int i) {
		System.out.println("On the " + ordinalNumber(i) + " day of Christmas\n"
				+ "my true love sent to me:");
		for (int j = i; j > 0; j--) {
			if (j == 1) {
				System.out.print((i == 1)?"":"and ");
				}
			System.out.println(getGift(j));
		}
		System.out.println();
	}
	
	private static String getGift(int i) {
		switch (i) {
		case 1:
			return "a Partridge in a Pear Tree";
		case 2:
			return "Two Turtle Doves";
		case 3:
			return "Three French Hens";
		case 4:
			return "Four Calling Birds";
		case 5:
			return "Five Golden Rings";
		case 6:
			return "Six Geese a Laying";
		case 7:
			return "Seven Swans a Swimming";
		case 8:
			return "Eight Maids a Milking";
		case 9:
			return "Nine Ladies Dancing";
		case 10:
			return "Ten Lords a Leaping";
		case 11:
			return "Eleven Pipers Piping";
		case 12:
			return "12 Drummers Drumming";
		}
		return null;
	}
	
	private static String ordinalNumber(int i) {
		switch (i){
		case 1:
			return "first";
		case 2:
			return "second";
		case 3:
			return "third";
		case 4:
			return "fourth";
		case 5:
			return "fifth";
		case 6:
			return "sixth";
		case 7:
			return "seventh";
		case 8:
			return "eighth";
		case 9:
			return "ninth";
		case 10:
			return "tenth";
		case 11:
			return "eleventh";
		case 12:
			return "twelfth";
		}
		return null;
	}
}
