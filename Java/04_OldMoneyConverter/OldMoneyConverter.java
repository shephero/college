import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class OldMoneyConverter {
	public static final int OLD_PENNIES_IN_NEW_PENNIES = 67;
	public static final int OLD_SHILLINGS_IN_OLD_PENNIES = 12;
	public static final int OLD_POUND_IN_OLD_SHILLINGS = 20;

	public static void main(String[] args) {
		
		JTextField oldPoundsBox = new JTextField();
		JTextField oldShillingsBox = new JTextField();
		JTextField oldPenceBox = new JTextField();

		Object[] moneyArray = {
			"Olde Pounds to convert:", oldPoundsBox,
			"Olde Shillings to convert:", oldShillingsBox,
			"Olde Pence to convert:", oldPenceBox,
			"\n*note* if you have nothing to\nenter, please insert a zero.", null,
		};
		
		// TODO, insert default value of zero to prevent program crash when nothing is entered
		int moneyPane = JOptionPane.showConfirmDialog(null, moneyArray, "Money Conversion", JOptionPane.OK_CANCEL_OPTION);
		
		if (moneyPane == JOptionPane.OK_OPTION) {
			
			// Create a string of all input boxed combined
			String allInput = oldPoundsBox.getText() + " " + oldShillingsBox.getText() + " " + oldPenceBox.getText(); 
			Scanner inputScanner = new Scanner(allInput);

			// Use Scanner on above string to obtain numbers
			float oldPounds = inputScanner.nextFloat();
			float oldShillings = inputScanner.nextFloat();
			float oldPence = inputScanner.nextFloat();
			inputScanner.close();

			// Convert all old money to old pence
			oldPence += oldShillings*OLD_SHILLINGS_IN_OLD_PENNIES;
			oldPence += oldPounds*OLD_POUND_IN_OLD_SHILLINGS*OLD_SHILLINGS_IN_OLD_PENNIES;
			
			// Convert all old pence to new pence
			float newPence = oldPence*OLD_PENNIES_IN_NEW_PENNIES;
			float newPounds = (float) (newPence/100);
			String.format("%.2f", newPounds);
			
			JOptionPane.showMessageDialog(null, "That is equal to £" + newPounds + " in new money.");
		}
	}
}
