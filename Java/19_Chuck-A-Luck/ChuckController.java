import java.util.Arrays;
import java.util.HashMap;

enum WinType {
    number,
    triple,
    field,
    high,
    low,
    none,
}

public class ChuckController {
    Model model;
    private HashMap<WinType, Integer> odds;

    private int occuranceBetOn;
    private int betAmount;
    private WinType betType;
    private int betNumber;
    private int proportionWon;
    private MoneyI moneyWon;

    ChuckController(Model model) {
        this.model = model;
        odds = new HashMap<>();
        odds.put(WinType.number, 0);
        odds.put(WinType.triple, 30);
        odds.put(WinType.field, 1);
        odds.put(WinType.high, 1);
        odds.put(WinType.low, 1);
        odds.put(WinType.none, 0);
    }

    public void setOddsForOccurance(int number) {
        occuranceBetOn = number;
        switch(number) {
            case 1:
                odds.put(WinType.number, 1);
                break;
            case 2:
                odds.put(WinType.number, 2);
                break;
            case 3:
                odds.put(WinType.number, 10);
                break;
        }
    }

    public void run() {
        for (DiceI d : model.getDice()) {d.roll();}
        switch(betType) {
            case number:
                if (isNumber()) {win(WinType.number);}
                else {win(WinType.none);}
                break;
            case triple:
                if (isTriple()) {win(WinType.triple);}
                else {win(WinType.none);}
                break;
            case field:
                if (isField()) {win(WinType.field);}
                else {win(WinType.none);}
                break;
            case high:
                if (isHigh()) {win(WinType.high);}
                else {win(WinType.none);}
                break;
            case low:
                if (isLow()) {win(WinType.low);}
                else {win(WinType.none);}
                break;
        }
    }

    private void win(WinType type) {
        proportionWon = odds.get(type);
        setMoneyWon(new Money((proportionWon > 0 ? betAmount : 0) + betAmount * proportionWon));
        model.wallet.put(getMoneyWon());
    }

    private boolean isHigh() {
        return getTotal(model.getDice()) > 10
                && !isTriple();
    }

    private boolean isLow() {
        return getTotal (model.getDice()) < 11
                && !isTriple();
    }

    private boolean isField() {
        int total = getTotal(model.getDice());
        return total > 12 || total < 8;
    }

    private int getTotal(DiceI[] dice) {
        return Arrays.stream(dice)
                .mapToInt(d -> d.value())
                .sum();
    }

    private boolean isNumber() {
        int matches = (int) (Arrays.stream(model.getDice())
                .filter(d -> d.value() == betNumber)
                .count());
        return (matches == betNumber);
    }

    private boolean isTriple() {
        return containsThreeSame(model.getDice())
                && !containsOne(model.getDice())
                && !containsSix(model.getDice());
    }

    private boolean containsSix(DiceI[] dice) {
        return Arrays.stream(dice)
                .map(d -> d.value())
                .anyMatch(i -> i == 6);
    }

    private boolean containsOne(DiceI[] dice) {
        return Arrays.stream(dice)
                .map(d -> d.value())
                .anyMatch(i -> i == 1);
    }

    private boolean containsThreeSame(DiceI[] dice) {
        int distinct = (int) (Arrays.stream(model.getDice())
                .map(d -> d.value())
                .distinct()
                .count());
        return distinct == 1;
    }

    public int getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(int betAmount) {
        this.betAmount = betAmount;
    }

    public WinType getBetType() {
        return betType;
    }

    public void setBetType(WinType betType) {
        this.betType = betType;
    }

    public int getBetNumber() {
        return betNumber;
    }

    public void setBetNumber(int betNumber) {
        this.betNumber = betNumber;
    }

    public int getProportionWon() {
        return proportionWon;
    }

    public MoneyI getMoneyWon() {
        return moneyWon;
    }

    public void setMoneyWon(MoneyI moneyWon) {
        this.moneyWon = moneyWon;
    }

    public int getOccuranceBetOn() {
        return occuranceBetOn;
    }

    public void setOccuranceBetOn(int occuranceBetOn) {
        this.occuranceBetOn = occuranceBetOn;
    }
}
