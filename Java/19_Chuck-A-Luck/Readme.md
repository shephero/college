Dependencies:
Java JDK 8     (May work with later versions)
bash           (only for the compile-and-run command provided)

## To compile and run ##
$ bash -c 'mkdir -p bytecode && javac -Xlint:unchecked *.java -d bytecode && cd bytecode && java ChuckALuck && cd ..'

## To compile only ##
$ mkdir -p bytecode
$ javac *.java -d bytecode

## To run ##
$ cd bytecod
$ java ChuckALuck