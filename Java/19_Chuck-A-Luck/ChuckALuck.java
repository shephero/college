
public class ChuckALuck {
    public static void main(String[] args) {
        Model model = new Model();
        ChuckController controller = new ChuckController(model);
        View view = new View(controller, model);
        view.run();
    }
}
