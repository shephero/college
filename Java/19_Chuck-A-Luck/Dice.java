import java.util.Random;

public class Dice implements DiceI {
    int faces;
    int topFace;
    Random rand;

    Dice(int faces, Random rand) {
        this.rand = rand;
        this.faces = faces;
        this.topFace = 1;
    }

    @Override
    public int faces() {
        return faces;
    }

    @Override
    public int value() {
        return topFace;
    }

    @Override
    public void roll() {
        topFace = Math.abs(rand.nextInt() % faces) + 1;
    }
}
