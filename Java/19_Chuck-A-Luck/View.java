import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class View {
    ChuckController controller;
    Model model;
    JDialog currentScreen;

    View(ChuckController controller, Model model) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (Exception ex) {
            System.out.println( "You don't have GTK installed." +
                    "Guess we'll go with whatever ugly theme your system uses.");
            try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
            catch(Exception e) {}
        }
        this.controller = controller;
        this.model = model;
    }

    public void run() {
        displayRules();
        currentScreen = getStatusDisplay();
        while(true) {
            currentScreen.setVisible(true);
        }
    }

    private JDialog getBetMenu() {
        UIManager.put("Slider.paintValue", false);
        JSlider walletSlider = new JSlider(0, model.wallet.check().getMoneyInCents());
        JLabel betLabel = new JLabel("I would like to bet: €" +
                walletSlider.getValue()/100 + "." +
                walletSlider.getValue()%100, SwingConstants.CENTER);
        JLabel whatLabel = new JLabel("On a:", SwingConstants.CENTER);

        JButton numberButton = new JButton("Number");
        JButton tripleButton = new JButton("Triple");
        JButton fieldButton = new JButton("Field");
        JButton highButton = new JButton("High");
        JButton lowButton = new JButton("Low");

        JDialog window = getBaseWindow();
        window.setLayout(new GridLayout(4, 1));
        window.add(betLabel);
        window.add(walletSlider);
        window.add(whatLabel);
        JPanel buttons = new JPanel();
        buttons.add(numberButton);
        buttons.add(tripleButton);
        buttons.add(fieldButton);
        buttons.add(highButton);
        buttons.add(lowButton);
        window.add(buttons);

        numberButton.addActionListener(actionEvent -> {
            controller.setBetType(WinType.number);
            betPlaced(walletSlider, window);
        });

        tripleButton.addActionListener(actionEvent -> {
            controller.setBetType(WinType.triple);
            betPlaced(walletSlider, window);
        });

        fieldButton.addActionListener(actionEvent -> {
            controller.setBetType(WinType.field);
            betPlaced(walletSlider, window);
        });

        highButton.addActionListener(actionEvent -> {
            controller.setBetType(WinType.high);
            betPlaced(walletSlider, window);
        });

        lowButton.addActionListener(actionEvent -> {
            controller.setBetType(WinType.low);
            betPlaced(walletSlider, window);
        });

        walletSlider.addChangeListener(changeEvent -> {
            int centsInt = walletSlider.getValue()%100;
            String cents = (centsInt < 10)?
                    "0" + Integer.toString(centsInt) :
                    Integer.toString(centsInt);
            betLabel.setText("I would like to bet: €" +
                    walletSlider.getValue()/100 + "." +
                    cents);
        });

        window.pack();
        //window.setSize(window.getWidth() + 20, window.getHeight());
        centerWindow(window);
        return window;
        //window.setVisible(true);
    }

    private JDialog getBaseWindow() {
        JDialog window = new JDialog();
        window.setModal(true);
        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
        window.setTitle("Chuck-A-Luck");
        return window;
    }

    private void betPlaced(JSlider s, JDialog d) {
        controller.setBetAmount(s.getValue());
        try {model.wallet.take(new Money(s.getValue()));}
        catch (Exception e) {
            JLabel label = new JLabel(
                    "Something went wrong. Probably you hacking the game as my code is beautiful.",
                    SwingConstants.CENTER);
            JOptionPane.showMessageDialog(null, label);
            currentScreen = getBetMenu();
            return;
        }
        d.dispose();
        if (controller.getBetType() == WinType.number) {
            currentScreen = getAmountChooser();
        }
        else {
            controller.run();
            currentScreen = getOutcomeDisplay();
        }
    }

    private JDialog getAmountChooser() {
        JLabel faceLabel = new JLabel("What face are you betting on?", SwingConstants.CENTER);
        String[] faces = new String[6];
        for (int i = 0; i < 6; i++) {
            whatevs(faces, i);
        }
        JComboBox<String> combo = new JComboBox<>(faces);
        String[] diceNumber = new String[3];
        for (int i = 0; i < 3; i++) whatevs(diceNumber, i);
        JLabel occuranceLabel = new JLabel("How many times do you think it will occur?", SwingConstants.CENTER);
        JComboBox<String> occuranceBox = new JComboBox<>(diceNumber);
        JButton okayButton = new JButton("Okay");
        JDialog window = getBaseWindow();
        window.setLayout(new GridLayout(5, 1));

        window.add(faceLabel);
        window.add(combo);
        window.add(occuranceLabel);
        window.add(occuranceBox);
        window.add(okayButton);
        window.pack();
        window.setSize(window.getWidth() + 50, window.getHeight());
        centerWindow(window);
        okayButton.addActionListener(actionEvent -> {
            controller.setBetNumber(combo.getSelectedIndex() + 1);
            controller.setOddsForOccurance(occuranceBox.getSelectedIndex() + 1);
            controller.run();
            currentScreen = getOutcomeDisplay();
            window.dispose();
        });
        return window;
    }

    private void whatevs(String[] diceNumber, int i) {
        diceNumber[i] = Integer.toString(i+1);
    }

    private void centerWindow(JDialog p) {
        p.setLocationRelativeTo(null);
    }

    private void displayRules() {
    }

    private String aheadOrBehind() {
        if (model.wallet.check().getMoneyInCents() > model.STARTING_MONEY) {return "ahead";}
        else if (model.wallet.check().getMoneyInCents() < model.STARTING_MONEY) {return "behind";}
        else {return "neutral";}
    }

    private JDialog getStatusDisplay() {
        JLabel walletStatus = new JLabel("You have " +
                model.wallet.check().toString() +
                    " in your wallet\uD83C \uDCA3", SwingConstants.CENTER);
        JButton betButton = new JButton("Place Bet");
        JButton exitButton = new JButton("Quit while you're " + aheadOrBehind());
        JDialog window = getBaseWindow();
        window.setLayout(new GridLayout(2, 1));
        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        window.add(walletStatus);
        JPanel buttons = new JPanel();
        buttons.add(betButton);
        buttons.add(exitButton);
        window.add(buttons);
        betButton.addActionListener(actionEvent -> {
            window.dispose();
            if (model.wallet.check().getMoneyInCents() > 0) {
                currentScreen = getBetMenu();
            }
            else {currentScreen = getLoosingScreen();}
        });
        exitButton.addActionListener(actionEvent -> System.exit(0));
        window.pack();
        centerWindow(window);
        return window;
        //window.setVisible(true);
    }

    private JDialog getLoosingScreen() {
        JDialog window = getBaseWindow();
        window.setLayout(new GridLayout(2, 1));
        JLabel label = new JLabel("Sorry, you don't have any more money.", SwingConstants.CENTER);
        JPanel buttons = new JPanel();
        JButton returnButton = new JButton("Return");
        JButton exitButton = new JButton("Exit");
        buttons.add(returnButton);
        buttons.add(exitButton);
        window.add(label);
        window.add(buttons);
        returnButton.addActionListener(actionEvent -> {
            currentScreen = getStatusDisplay();
            window.dispose();
        });
        exitButton.addActionListener(actionEvent -> System.exit(0));
        window.pack();
        window.setSize(window.getWidth() + 50, window.getHeight());
        centerWindow(window);
        return window;
    }
    
    private JDialog getOutcomeDisplay() {
        JDialog window = getBaseWindow();
        window.setLayout(new GridLayout(4, 1));
        JLabel roundOutcome = new JLabel("The three dice landed like this:", SwingConstants.CENTER);
        JPanel dicePanel = new JPanel();
        for(DiceI d : model.getDice()) {
            dicePanel.add(new JLabel(" │ " + Integer.toString(d.value()) + " │ ", SwingConstants.CENTER));
        }
        String wonOrLost = ((controller.getProportionWon() > 0) ? "winnings" : "losses");
        JButton okayButton = new JButton("Accept your " + wonOrLost);
        JLabel wonLabel = new JLabel("You " + ((controller.getMoneyWon().getMoneyInCents() > 0) ?
                "won " + controller.getMoneyWon().toString() + " back" : "didn't win anything."), SwingConstants.CENTER);

        okayButton.addActionListener(actionEvent -> {
            currentScreen = getStatusDisplay();
            window.dispose();
        });

        window.add(roundOutcome);
        window.add(dicePanel);
        window.add(wonLabel);
        window.add(okayButton);
        window.pack();
        window.setSize(window.getWidth() + 50, window.getHeight());
        centerWindow(window);
        return window;
    }
}
