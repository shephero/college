
public class Wallet implements WalletI {
    private MoneyI money;

    Wallet(Money money) {
        this.money = money;
    }

    @Override
    public MoneyI check() {
        return money;
    }

    @Override
    public void put(MoneyI sum) {
        money = money.plus(sum);
    }

    @Override
    public void take(MoneyI sum) throws InsufficientMoneyInWalletException {
        try {
            money = money.minus(sum);
        }
        catch (MoneyI.InsufficientMoneyException e) {
            throw new InsufficientMoneyInWalletException();
        }
    }
}
