
public class Money implements MoneyI {
    private int moneyInCents; // In cents

    Money(int startingCents) {
        this.moneyInCents = startingCents;
    }

    @Override
    public int totalEuro() {
        return getMoneyInCents() / 100;
    }

    @Override
    public int remainingCent() {
        return getMoneyInCents() % 100;
    }

    @Override
    public MoneyI plus(MoneyI money) {
        return new Money(this.getMoneyInCents() + money.getMoneyInCents());
    }

    @Override
    public MoneyI minus(MoneyI money) throws InsufficientMoneyException {
        MoneyI newMoney = new Money(this.getMoneyInCents() - money.getMoneyInCents());
        if (newMoney.getMoneyInCents() < 0) {
            throw new InsufficientMoneyException();
        }
        else {
            return newMoney;
        }
    }

    @Override
    public String toString() {
        String cents =  Integer.toString(remainingCent());
        return "€" + totalEuro() +
            ((remainingCent() > 0) ? "." + cents +
                (cents.length() > 0 ? "" : 0)
                    : "");
    }

    @Override
    public int compareTo(MoneyI money) {
        return 0;
    }

    public int getMoneyInCents() {
        return moneyInCents;
    }
}
