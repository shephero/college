import java.util.Random;

public class Model {
    public static final int STARTING_MONEY = 300; // In cents
    private DiceI[] dice;
    WalletI wallet;

    Model() {
        DiceI[] dice = new DiceI[3];
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            dice[i] = new Dice(6, random);
        }
        this.setDice(dice);
        Money money = new Money(STARTING_MONEY);
        this.wallet = new Wallet(money);
    }

    public DiceI[] getDice() {
        return dice;
    }

    public void setDice(DiceI[] dice) {
        this.dice = dice;
    }
}
