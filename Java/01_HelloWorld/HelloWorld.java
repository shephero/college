import java.util.Scanner;

public class HelloWorld 
{	
	public static void main(String[] args) 
	{
		System.out.println("Hello World!\nPlease input your name, human!");
		Scanner input =  new Scanner( System.in );
		String name = input.next();
		System.out.println("Hello " + name);
		input.close();
	}
}
