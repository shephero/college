public class InvalidMoveException extends Exception {
    private String playerString;
    InvalidMoveException(C4PlayerI player) {
        playerString = player.playerString();
    }
    @Override
    public String getMessage()
    {
        return "Player " + playerString + " made an invalid move.";
    }
}