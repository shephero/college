public class Player {
    private int ID;
    private boolean AI;

    Player(int ID, boolean isAI)
    {
        this.ID = ID;
        this.AI = isAI;
    }

    public int getNumber()
    {
        return (ID);
    }
    public boolean isAI() {return AI;}
    public String playerString()
    {
        String result = "";
        if (ID == 1) {result += "one";}
        else {result += "two";}
        return result;
    }
}
