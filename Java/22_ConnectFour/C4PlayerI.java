import java.util.Scanner;

public interface C4PlayerI {
    int getNumber();
    boolean isAI();
    int getTurn(CNGrid grid, int winAmount, Scanner scanner);
    String playerType();
    String playerString();
}
