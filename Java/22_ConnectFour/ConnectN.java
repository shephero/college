import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ConnectN {
    private static final int MAX_INVALID_ANSWERS = 10;
    private C4PlayerI[] players;
    private CNGrid grid;
    private int[] dimensions;
    private int winAmount;
    private int playerTurn;
    private int turn;


    int otherPlayer()
    {
        return 1 - playerTurn;
    }

    private static boolean parameterOn(String[] args, String subString)
    {
        return Arrays.stream(args)
                .anyMatch(s -> s.equals(subString));
    }

    private static List<String> getMatchingArgs(String[] args, String subString)
    {
        return Arrays.stream(args)
                .filter(s -> s.contains(subString)).collect(Collectors.toList());
    }

    private static int argToInt(String[] args, String subString)
    {
        List<String> matches = getMatchingArgs(args, subString);
        if (matches.size() == 0) {return -1;}
        else {
            String firstResult = matches.get(0);
            String intString = firstResult.replace("[^0-9]", "");
            return Integer.parseInt(intString);
        }
    }

    public static void main(String[] args) throws Exception
    {
        Scanner scanner = new Scanner(System.in);

        if (parameterOn(args, "--help")) {
            System.out.println(getHelp(args));
            System.exit(0);
        }

        boolean benchmark = false;
        int timesToRun = 1;
        int[] dimensions = {100, 100};
        if (parameterOn(args, "--benchmark-ai")) {
            benchmark = true;
            timesToRun = 100;
        }

        int gameArg = argToInt(args, "--games=");
        if (gameArg > 0) {timesToRun = gameArg;}

        int millisElapsed = 0;
        int winAmount = 4;
        for (int i = 0; i < timesToRun; i++) {
            ConnectN game = new ConnectN();
            if (benchmark) {
                game.setup(new C4RandomAIPlayer[] {new C4RandomAIPlayer(1), new C4RandomAIPlayer(2)},
                        dimensions,
                        winAmount);
            } else {
                game.setup(scanner);
            }
            long startTime = System.currentTimeMillis();
            game.run(scanner);
            millisElapsed += System.currentTimeMillis() - startTime;
        }
        int averageMilis = millisElapsed / timesToRun;
        if (benchmark) {
            System.out.println("AIs were pitched against each other "
                    + timesToRun + " times.");
            System.out.println("This took " + millisElapsed + " milliseconds.");
            System.out.println("The average game time was "
                    + averageMilis + " milliseconds)");
        }
        scanner.close();
    }

    private static String getHelp(String[] args)
    {
        return "This program allows you (a human) or an AI to play Connect 4\n"
                        + "(and derived games) against a human or an AI.\n"
                        + "Usage: " + args[0] + " [options]\n"
                        + "Options:\n"
                        + "--help (prints this message)\n"
                        + "--benchmark-ai (allows you to benchmark the AI)\n"
                        + "--games=N (starts N games consecutively)\n";
    }

    private void run(Scanner scanner)
    {
        this.turn = 0;
        while (true) {
            this.turn++;
            C4PlayerI player = players[playerTurn];
            if (grid.getNonFullColumns().size() == 0) {
                System.out.println("Grid full. Nobody won. Everyone is a loser.");
                break;
            }

            if (!player.isAI()) {
                System.out.println("It's player "
                        + player.playerString()
                        + "'s turn.");
                grid.print();
            }
            int turn = getTurn(scanner);
            /*System.out.println("Turn " + this.turn + ": player "
                    + player.playerString()
                    + " (" + player.playerType() + ") "
                    + "placed a marker in column " + turn + ".");*/
            int lastRow;
            try {
                lastRow = grid.placeTile(turn, player);
            } catch (InvalidMoveException e) {
                System.out.println(e.getMessage());
                printWinner(players[otherPlayer()]);
                break;
            }
            boolean playerWon = grid.won(winAmount, turn, lastRow);
            //boolean playerWon = false;
            if (playerWon) {
                //grid.print();
                printWinner(players[playerTurn]);
                break;
            }
            playerTurn = otherPlayer();
        }
    }

    private int getTurn(Scanner scanner)
    {
        CNGrid gridCopy = new CNGrid(grid.getTiles().clone());
        return players[playerTurn].getTurn(gridCopy, winAmount, scanner);
    }

    private void printWinner(C4PlayerI player)
    {
        System.out.println("Player " + player.playerString() + " wins!");
    }

    private void setup(C4PlayerI[] players, int[] dimensions, int numberInARow)
    {
        this.dimensions = dimensions;
        this.players = players;
        this.winAmount = numberInARow;
        grid = new CNGrid(dimensions[0], dimensions[1]);
    }

    private void setup(Scanner scanner)
    {
        defineRules(scanner);
        players = new C4PlayerI[2];
        players[0] = getPlayerConfig(1, scanner);
        players[1] = getPlayerConfig(2, scanner);
        grid = new CNGrid(dimensions[0], dimensions[1]);
    }

    private int getDimension(Scanner scanner, boolean rows)
    {
        System.out.println("Input amount of " + (rows?"rows":"columns:"));
        return getChoice(10000, scanner);
    }

    private boolean customRules(Scanner scanner)
    {
        int choices = 2;
        System.out.println("Would you like to play:\n" +
                "(1) Connect Four\n" +
                "(2) Custom Rules");
        int choice = getChoice(choices, scanner);
        return choice == 2;
    }

    private void defineRules(Scanner scanner)
    {
        dimensions = new int[2];
        if (!customRules(scanner)) {
            dimensions[0] = 6;
            dimensions[1] = 7;
            winAmount = 4;
        }
        else {
            dimensions[0] = getDimension(scanner, true);
            dimensions[1] = getDimension(scanner, false);
            System.out.println("How many counters in a row to win?");
            winAmount = getChoice(Math.max(dimensions[0], dimensions[1]), scanner);
        }
    }

    public static int getChoice(int choices, Scanner scanner)
    {
        String error = "Not a valid answer, please enter a number between '1' and '" + choices + "'.";
        int choice;
        int invalidAnswers = 0;
        while (true) {
            System.out.print("-> ");
            try {
                String answer = scanner.nextLine();
                choice = Integer.parseInt(answer);
                if (choice < 1 || choice > choice) {
                    System.out.println(error);
                    continue;
                }
            } catch (Exception e) {
                System.out.println(error);
                if (invalidAnswers > MAX_INVALID_ANSWERS) {
                    System.out.println("Maximum amount of invalid answers reached ("
                            + MAX_INVALID_ANSWERS
                            + "). Exiting.");
                    System.exit(0);
                }
                invalidAnswers++;
                continue;
            }
            break;
        }
        return choice;
    }

    private C4PlayerI getPlayerConfig(int playerID, Scanner scanner)
    {
        C4PlayerI player = new C4RandomAIPlayer(playerID);
        int choices = 2;
        System.out.println("Would you like player "
                + player.playerString() +
                " to be:");
        System.out.println("(1) Human");
        System.out.println("(2) AI");
        int choice = getChoice(choices, scanner);
        switch (choice) {
            case 1:
                player = new C4HumanPlayer(playerID);
                break;
            case 2:
                player = new C4RandomAIPlayer(playerID);
                break;
        }
        return player;
    }
}