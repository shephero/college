import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
    ## Internal representation ##
      C0  C1  C2  C3  C4  C5  C6

y    | _ | _ | _ | _ | _ | _ | _ |    R0
y    | _ | _ | _ | _ | _ | _ | _ |    R1
y    | _ | _ | _ | _ | _ | _ | _ |    R2
y    | _ | _ | _ | 2 | _ | _ | _ |    R3
y    | _ | _ | _ | 2 | 1 | _ | _ |    R4
y    | _ | _ | _ | 1 | 2 | 1 | _ |    R5
     -----------------------------

Axis   x   x   x   x   x   x   x

To access C4, R1:
tiles[1][4]

*/

public class CNGrid {
    private int[][] tiles;

    CNGrid(int[][] tiles) {
        this.tiles = tiles;
    }

    // Create grid and initialise all to Tile.unowned
    CNGrid(int x, int y)
    {
        setTiles(new int[x][y]);
        Arrays.stream(getTiles())
              .forEach(arr -> Arrays.stream(arr)
                                    .forEach(n -> n = -1));
    }

    List<Integer> getNonFullColumns() {
        ArrayList<Integer> nonFullColumns = new ArrayList<>();
        for (int i = 0; i < tiles[0].length; i++) {
            if (tiles[0][i] == 0) {
                nonFullColumns.add(i);
            }
        }
        return nonFullColumns;
    }
    
    int placeTile(int col, C4PlayerI player) throws InvalidMoveException
    {
        if (col < 0 || col > tiles[0].length) {
            throw new InvalidMoveException(player);
        }
        int row = firstEmptyRowIndex(col);
        if (row < 0) {
            throw new InvalidMoveException(player);
        }
        tiles[row][col] = player.getNumber();
        return row;
    }

    int firstEmptyRowIndex(int col) {
        for (int i = tiles.length - 1; i >= 0; i--) {
            if (tiles[i][col] == 0) {
                return i;
            }
        }
        return -1;
    }

    public boolean won(int winAmount, int lastCol, int lastRow)
    {
        int[] directions = new int[4];
        directions[0] = checkHorizontal(winAmount);
        directions[1] = checkVertical(winAmount);
        directions[2] = checkDiagonals(winAmount);
        return Arrays.stream(directions).anyMatch(a -> a > 0);
    }

    int checkDiagonals(int winAmount)
    {
        int amount = 0;
        int current = 0;
        int amountOpp = 0;
        int currentOpp = 0;
        int width = getTiles()[0].length;
        int height = getTiles().length;
        int diags = height + width - 1;    // Number of diagonals
        for (int diag = 0; diag < diags; diag++) {
            int row_start = Math.max(0, diag - width + 1);
            int row_stop = Math.min(diag, height - 1);
            for (int row = row_start; row <= row_stop; row++) {  // First Direction
                int col = diag - row;
                int colOpp = width - col - 1;
                int tile = getTiles()[row][col];
                int tileOpp = getTiles()[row][colOpp];
                if (tile > 0 && current == tile) {
                    amount++;
                }
                else {
                    amount = 1;
                    current = tile;
                }
                if (amount == winAmount) {
                    return current;
                }
                if (tileOpp > 0 && currentOpp == tileOpp) {
                    amountOpp++;
                }
                else {
                    amountOpp = 1;
                    currentOpp = tileOpp;
                }
                if (amountOpp == winAmount) {
                    return currentOpp;
                }
            }
            amount = 0;
            current = 0;
            currentOpp = 0;
            amountOpp = 0;
        }
        return 0;
    }

    int checkVertical(int winAmount)
    {
        int amount = 0;
        int current = 0;
        for (int i = 0; i < getTiles()[0].length; i++) {
            for (int j = 0; j < getTiles().length; j++) {
                if (current == getTiles()[j][i] && current != 0) {
                    amount++;
                } else {
                    amount = 1;
                    current = getTiles()[j][i];
                }
                if (amount == winAmount) {
                    return current;
                }
            }
            amount = 0;
            current = 0;
        }
        return 0;
    }

    public void print()
    {
        System.out.println("\n");
        for(int[] arr : tiles) {
            for(int t : arr) {
                System.out.print("| " + t + " ");
            }
            System.out.println("|");
            hl(tiles[0].length * 4 + 1);
        }
        for(int col = 1; col < tiles[0].length + 1; col++) {
            System.out.print("  " + col + " ");
        }
        System.out.print("   <- Columns");
        System.out.println("\n");
    }

    private static void hl(int size)
    {
        for (int i = 0; i < size; i++)
            System.out.print("-");
        System.out.println();
    }

    int checkHorizontal(int winAmount)
    {
        int amount = 0;
        int current = 0;
        for (int[] row : getTiles()) {
            for (int til : row) {
                if (current == til && current != 0) {
                    amount++;
                } else {
                    amount = 1;
                    current = til;
                }
                if (amount == winAmount) {
                    return current;
                }
            }
            amount = 0;
            current = 0;
        }
        return 0;
    }

    public int[][] getTiles()
    {
        return tiles;
    }

    public void setTiles(int[][] tiles)
    {
        this.tiles = tiles;
    }
}
