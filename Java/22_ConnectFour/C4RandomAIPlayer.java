import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class C4RandomAIPlayer extends Player implements C4PlayerI {
    private Random random = new Random();

    C4RandomAIPlayer(int ID)
    {
        super(ID, true);
    }

    public String playerType() {
        return "random AI";
    }

    public int getTurn(CNGrid grid, int winAmount, Scanner scanner)
    {
        List<Integer> nonFull = grid.getNonFullColumns();
        return nonFull.get(Math.abs(random.nextInt()) % nonFull.size());
    }
}
