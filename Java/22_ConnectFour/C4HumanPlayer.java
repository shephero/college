import java.util.Scanner;

public class C4HumanPlayer extends Player implements C4PlayerI {

    C4HumanPlayer(int ID)
    {
        super(ID, false);
    }

    public String playerType() {
        return "human";
    }

    public int getTurn(CNGrid grid, int winAmount, Scanner scanner)
    {
        int turn;
        System.out.println("Please enter a column number:");
        turn = ConnectN.getChoice(grid.getTiles()[0].length, scanner);
        return turn - 1;
    }
}
