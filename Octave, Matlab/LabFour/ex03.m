st=figure('Name', 'normalised FFT plots');
figure(st);

[signal , sampling]=audioread('exercise2_piece.wav');
N=200000;
F=fftshift(abs(fft(signal,N)));              % frequency spectrum
fs=sampling;
newX=-fs/2:fs/N:fs/2-fs/N;
subplot(3, 1, 1);
plot(newX,F);
title(['Unmodulated']);
hold on;
rand=0.01*randn([size(signal),1]);

amsignal=ammod(signal, 30000, sampling);
amgn=amsignal+rand;
amoddec=amdemod(amgn,30000,sampling);
F=fftshift(abs(fft(amsignal,N)));              % frequency spectrum
subplot(3, 1, 2);
plot(newX,F);
title(['Amplitude Modulated']);

fmsignal=fmmod(signal, 30000 ,sampling, 20000);
fmgn=fmsignal+rand;
fmoddec=fmdemod(fmgn,30000,sampling, 20000);
F=fftshift(abs(fft(fmsignal,N)));              % frequency spectrum
subplot(3, 1, 3);
plot(newX,F);
title(['Frequency Modulated']);

sound(fmoddec,sampling);