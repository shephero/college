st=figure('Name', 'FFT for noise');
figure(st);

[notes, fsampling]=audioread('exercise.wav');
fs=fsampling;
splitindex=6845;
N=16384;
newX=-fs/2:fs/N:fs/2-fs/N;

noteOne=notes(1:splitindex);                 % 576 Hz -  a low D
noteTwo=notes(splitindex+1:end);             % 559 Hz  - c#

F=fftshift(abs(fft(noteOne,N)));             % frequency spectrum
plot(newX,noteOne);