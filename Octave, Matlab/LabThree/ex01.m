st=figure('Name', 'normalised FFT plots');
figure(st);

position=1;
for N=[64, 128, 256]            % number of FFT samples
  fs=100;                       % sampling frequency (Hz)
  frequency=10;                 % frequency of sine wave (Hz)
  time=2;                       % how many seconds to consider
  x=[0:1/fs:time-1/fs];         % points in x axis
  y=sin(2*pi*x*frequency);      % your signal                 
  F=fftshift(abs(fft(y,N)));              % frequency spectrum
  newX=-fs/2:fs/N:fs/2-fs/N ;
  
  subplot(2, 2, position);
  plot(newX,F);
  title(['FFT with points in FFT N = ' num2str(N) '.']);
  position = position+1;
end