st=figure('Name', 'FFT for noise');
figure(st);

y=load('array.mat');
x=0:1/1000:size(y);
N=1024;
F=abs(fft(y(),N));              % frequency spectrum

plot(F)