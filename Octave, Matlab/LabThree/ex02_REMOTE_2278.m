st=figure('Name', 'FFT for noise');
figure(st);

load('array.mat');
fs=1000
N=1024;
newX=-fs/2:fs/N:fs/2-fs/N ;
F=fftshift(abs(fft(y,N)));              % frequency spectrum
plot(newX,F)