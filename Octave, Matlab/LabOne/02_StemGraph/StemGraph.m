st=figure('Name', 'Plotting stem graphs');
figure(st)

x=1:9;
y=zeros(2, 9);

for k=1:9
  y(1, k)=k^2;
end

for k=-9:-1
  y(2, -k)=(k+10)^2;
end


stem(x, y(1, :));  % plot a stem graph of the forst row of y values
hold on
stem(x, y(2, :), 'r');
axis([1 9 0 90])

title('Stem function');
print -dpng 'StemGraph.png'