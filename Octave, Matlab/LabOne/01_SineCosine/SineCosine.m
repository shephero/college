sc=figure('Name', 'Plotting on single figure - sine & cosine');
figure(sc);

x=0:0.5:5;
x2=[0 0.5 1 1.5 2 2.5 2.5 3 3.5 4 4.5 5]
y1=sin(x);
y2=cos(x2);

plot(x, y1, 'b');
hold on
plot(x2, y2, 'r');
xlabel('Time');
ylabel('Amplitude');
legend('sin', 'cos');
title('Sine and cosine finctions');
axis([0 5 0 1]);
print -dpng 'sin_cos_functions.png'
