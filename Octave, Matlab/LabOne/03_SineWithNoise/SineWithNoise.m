st=figure('Name', 'Sine with Noise');
figure(st);

x=0:0.01:10;
y=0.5*sin(2*pi*x);
%random=randn * 0.1;

for k=1:1000
  y(k)=y(k)+(randn*0.1);
end

plot(x,y)
print -dpng 'sine_with_noise.png'
