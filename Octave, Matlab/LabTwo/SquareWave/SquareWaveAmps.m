pkg load signal

st=figure('Name', 'Various square wave approximations');
figure(st);

ref=square(2*pi*x);

position=0;
for i=[1, 3, 5, 10, 50, 500]
  position = position+1;
  subplot(3, 2, position);
  x = 0;
  for k=0:i*2
    if mod(k, 2) == 1
      x = x + 1;
    end
  end
  x1 = zeros(1, x);
  y = zeros(1, x);
  for k=1:i*2
    if mod(k, 2) == 1
      x1(k) = k;
      y(k) = 4/pi/k;
    end
  end
  stem(x1, y, 'b');
  %hold on
  %plot(x, ref, 'r');
  title(['Plot of ' num2str(i) ' amplitudes in the frequency domain']);

  axis([0 i+1 -0 1.5]);
end