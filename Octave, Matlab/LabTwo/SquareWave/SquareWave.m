pkg load signal

st=figure('Name', 'Various square wave approximations');
figure(st);

x=[-1:0.01:1];
ref=square(2*pi*x);

position=0;
for i=[1, 3, 5, 10, 50, 500]
  position = position+1;
  y1=zeros(length(x));
  for k=0:i*2
    if mod(k, 2) == 1
      for j=1:201
        y1(j)=y1(j) + (sin(2*pi*k*x(j))/k);
      end
    end
  end
  
  y1=(4/pi) * y1;
  
  subplot(3, 2, position);
  title(['Approx. with ' num2str(i) 'sine waves']);

  plot(x, y1);
  hold on
  plot(x, ref, 'r');
  axis([-1 1 -2 2]);
end



%print -dpng 'sine_with_noise.png'
