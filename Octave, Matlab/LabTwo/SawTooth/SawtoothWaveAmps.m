pkg load signal

st=figure('Name', 'Various square wave approximations');
figure(st);

position=0;
for i=[1, 3, 5, 10, 50, 500]
  position = position+1;
  subplot(3, 2, position);
  x = zeros(1, i);
  y = zeros(1, i);
  for k=0:i-1
    x(k+1) = k+1;
    y(k+1) = (8 / (pi^2)) * ((-1) ^ k) / (((2 * (k))  + 1) ^ 2);
  end
  stem(x, y, 'b');
  %hold on
  %plot(x, ref, 'r');
  title(['Plot of ' num2str(i) ' amplitudes in the frequency domain']);

  axis([0 i+1 -0 1.5]);
end



%print -dpng 'sine_with_noise.png'
