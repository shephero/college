pkg load signal

st=figure('Name', 'Various sawtooth wave approximations');
figure(st);

x=[-1:0.01:1];
ref=sawtooth(2*pi*(x + 0.25), 0.5);

position=0;
for i=[1 2 3 5 10 50]
  position=position+1;
  y1=zeros(length(x));
  for k=0:i
    for j=1:201
      y1(j)=y1(j) + (-1)^k * (sin(2*pi*(2*k + 1)*x(j))/((2*k + 1)^2));
    end
  end
  y1=(8/(pi^2)) * y1;
  
  subplot(3, 2, position);
  title(['Approx. with ' num2str(i) 'sine waves']);

  plot(x, y1);
  hold on
  plot(x, ref, 'r');
  axis([-1 1 -2 2]);
end

%print -dpng 'sine_with_noise.png'