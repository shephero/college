    AREA    ExpEval, CODE, READONLY
    IMPORT    main
    IMPORT    getkey
    IMPORT    sendchar
    EXPORT    start
    PRESERVE8

start
    MOV R0, #'C'
    BL  sendchar
    MOV R0, #'a'
    BL  sendchar
    MOV R0, #'l'
    BL  sendchar
    MOV R0, #'c'
    BL  sendchar
    MOV R0, #':'
    BL  sendchar
    MOV R0, #0xA
    BL  sendchar

    MOV R8, #0                  ; R7 will hold the subtotals
restart
    MOV R0, #' '
    BL sendchar
    MOV R7, R8                  ; 0, or total from last calculation
    MOV R6, #0                  ; R6 will store our operator later on
    MOV R5, #10                 ; used to convert to/from decimal
    MOV R8, #0
    MOV R9, #0
getNewNumber
    MOV R4, #0                  ; number starts as 0
read
    BL    getkey                ; read key from console
    CMP R0, #'q'                ; if (key == 'q')
    BEQ stop                    ; {end program}
    CMP R0, #'c'                ; if (key == 'c')
    BEQ clear                   ; {restart the program}
    CMP R0, #0x20
    BEQ sendCharOnly            ; if (key == space) { print only }
    MOV R9, R8
    MOV R8, R0
    CMP R0, #'0'
    BLO checkForOperator        ; if (key < 0) { check if it's an operator }
    CMP R0, #'9'
    BGT checkForOperator        ; if (key > 9) { check if it's an operator }
    
    BL     sendchar             ; otherwise, it's a digit
    SUB R0, R0, #'0'            ; convert character to number
    MUL R4, R5, R4              ; numberTotal = 10 * numberTotal
    ADD R4, R4, R0              ; numberTotal = numberTotal + number (latest key)
    B    read                   ; get another character
    
sendCharOnly
    BL    sendchar              ; echo key back to console
    B   read

clear
    MOV R0, #0xA
    BL  sendchar
    MOV R7, #0
    B   print

checkForOperator
    CMP R0, #'+'                ; if ((key == '+')
    BEQ performCalculation
    CMP R0, #'^'                ; || (key == '^')
    BEQ performCalculation
    CMP R0, #'!'                ; || ((key == '!')
    BEQ performCalculation
    CMP R0, #'-'                ; || (key == '-')
    BEQ performCalculation
    CMP R0, #'*'                ; || (key == '*')
    BEQ performCalculation
    CMP R0, #'/'                ; || (key == '/'))
    BEQ performCalculation      ; {update subtotal}
    CMP    R0, #0x0D            ; else if (key == CR) 
    BEQ performCalculationIsCR  ; {update total and print}
    B   read                    ; else {ignore key}

changeAnswersSign
    BL  sendchar
    MOV R6, R9                  ; make sure '-' is not the next operator used. 
    MVN R7, R7                  ; bitflips contents of total
    ADD R7, R7, #1              ; add one, answer is now positive
    B   getNewNumber
    
checkDoubleOperator
    CMP R9, #'0'                ; or a '/'
    BLO getNewNumber            ; make answer negative and replace current operator with [*/]
    B   minus

performCalculationIsCR
    MOV R0, #0xA
performCalculation
    MOV R1, R6                  ; use operator in buffer explained below
    MOV R6, R0                  ; store the operator just entered in R6, to be used 
                                ; next time
    CMP R6, #'-'                ; if (operator is '-')
    BNE minus
    CMP R9, #'*'                ; && (previous input was a '*'
    BEQ changeAnswersSign
    CMP R9, #'/'                ; || a '/')
    BEQ changeAnswersSign       ; {make answer negative and replace current operator with [*/]}
    CMP R9, #'^'
    BEQ error
    
    CMP R6, #'*'
    BEQ checkDoubleOperator    
    CMP R6, #'/'
    BEQ checkDoubleOperator
    CMP R6, #'*'
    BEQ checkDoubleOperator
    
minus
    CMP R1, #'-'                ; if (operator == '-')
    BNE multiply
    SUB R7, R7, R4              ; { subtotal = subtotal - number }
    B   checkForCR

multiply
    CMP R1, #'*'                ; if (operator == '*')
    BNE factorial
    MUL R7, R4, R7              ; { subtotal = subtotal * number }
    B   checkForCR
    
factorial
    CMP R1, #'!'                ; if (operator is '!')
    BNE power
    CMP R7, #0
    BEQ answerIsOne
    CMP R7, #0
    BLT error
    MOV R1, R7
keepFactorialing
    SUB R1, #1
    MUL R7, R1, R7
    CMP R1, #1
    BEQ checkForCR
    B   keepFactorialing
    
answerIsOne
    MOV R7, #1
    B   checkForCR
    
power
    CMP R1, #'^'                ; if (operator is '^')
    BNE divide
    MOV R1, R7
    CMP R4, #0
    BLO error
    CMP R4, #0
    BEQ answerIsOne
keepPowering
    CMP R4, #1
    BEQ checkForCR
    MUL R7, R1, R7
    SUB R4, #1
    B   keepPowering
    
divide
    CMP R1, #'/'                ; if (operator == '/')
    BNE plus
    CMP R4, #0
    BEQ error
    CMP R7, #0
    BLT divideNegative
    CMP R7, R4
    BLO smallDivide
    MOV R2, R7                  ;        move dividend to R2
    MOV R7, #0                  ;         set subtotal to 0, herein called the quotient
keepSubtracting                 ; {d
    SUB R2, R2, R4              ;  i    do { dividend -= divisor
    ADD R7, #0x1                ;  v    { dividend -= divisor
    CMP R2, R4                  ;  i     quotient += 1 }
    BGE keepSubtracting         ;  d     while (dividend < divisor)
    B   checkForCR              ;  e}        
    
divideNegative
    MVN R7, R7
    ADD R7, R7, #1
    CMP R7, R4
    BLO smallDivide
    MOV R2, R7           ;      move dividend to R2
    MOV R7, #0           ;      set subtotal to 0, herein called the quotient
keepAdding               ; {d
    SUB R2, R2, R4       ;  i
    ADD R7, #0x1         ;  v   do { dividend += divisor
    CMP R2, R4           ;  i   quotient += 1 }
    BGE keepAdding       ;  d   while (dividend < divisor)
    MVN R7, R7           ;  e}
    ADD R7, R7, #1
    B   checkForCR    

smallDivide
    MOV R7, #0
    B   checkForCR
    
plus
    ADD R7, R7, R4              ; subtotal = subtotal + number:
    
checkForCR
    BL  sendchar
    CMP R0, #0xA                ; if (last thing entered was Carriage Return), print answer.
    BNE getNewNumber
    MOV R8, R7

print
    MOV R0, #'-'
    BL  sendchar
    MOV R0, #'>'
    BL  sendchar
    MOV R0, #' '
    BL  sendchar
printWithoutPrompt
    CMP R7, #0
    BLT printNegative
    MOV R6, #1                  ; easier to understand
    MOV R2, #1                  ; if you start counting digits at 1
    MOV R3, #1
    MOV R4, #1
oneMoreDigit
    ADD R6, R2, #1
    MUL R3, R5, R4
    CMP R3, R7
    BGT r2Digits                ; this will multiply 1 * 10 until the number 
    ADD R2, R6, #1              ; generated is greater than the total 
    MUL R4, R5, R3              ; the times this happens is counted
    CMP R4, R7
    BGT r6Digits                ; then the number of times - 1 is used
    B   oneMoreDigit
    
r2Digits
    MOV R6, R2                  ; R6 now contains the number of digits to print
r6Digits
    MOV R3, #1
obtainDivisor                   ; stores 10^x in R2, where x is the number of digits - 1
    MOV R2, R6
byTen
    CMP R2, #1
    BEQ obtainDigit
    MUL R3, R5, R3
    SUB R2, R2, #1
    B   byTen                   ; store 10^x in R3

obtainDigit
    MOV R0, #0
    MOV R2, #0
iterateDigit
    CMP R3, R7
    BGT printDigit
    SUB R7, R7, R3
    ADD R2, R2, #1
    ADD R0, R0, #1
    B   iterateDigit
    
printDigit
    ADD R0, #'0'                ; convert to ASCII
    BL  sendchar
    SUB R6, R6, #1
    CMP R6, #0 
    BEQ restart
    B   r6Digits
    
printNegative
    MOV R0, #'-'
    BL  sendchar
    MVN R7, R7                  ; bitflips contents of total
    ADD R7, R7, #1              ; add one, answer is now positive
    B   printWithoutPrompt
error
    MOV R0, #0xA
    BL  sendchar
noNewline
    MOV R0, #'N'                ; print "No"
    BL  sendchar
    MOV R0, #'o'
    BL  sendchar
    MOV R0, #0xA
    BL  sendchar
    B   print                   ; restart program

stop    B    stop
    END    
