	AREA	MotionBlur, CODE, READONLY
	IMPORT	main
	IMPORT	getPicAddr
	IMPORT	putPic
	IMPORT	getPicWidth
	IMPORT	getPicHeight
	EXPORT	start
	PRESERVE8

start

	MOV R0, #25    ; threshold
	BL edgeDetect
	BL putPic
	B stop
	
	;-------------------------------------------------------------------------------
	
	; # Subroutines: #
	;
	; ## Edge Detect ##
	; 
	; Description:
	; Takes a threshold and, for every pixel,
	; checks whether it differs from its neighbours
	; by more than that threshold, if so, it sets the
	; pixel to white, otherwise to black.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - threshold
	
edgeDetect
	STMFD sp!, {R4 - R8, lr}
	MOV R8, R0
	BL	getPicAddr
	MOV	R4, R0
	BL	getPicHeight
	MOV	R5, R0
	
forEachRow
	CMP R5, #0
	BLT endForEachRow
	BL	getPicWidth
	MOV	R6, R0
	
forEachColumn
	CMP R6, #0
	BLT endForEachColumn
	MOV R7, #2
	MOV R9, #0       ; total channel delta
	
forEachChannel
	CMP R7, #0
	BLT endForEachChannel
	MOV R0, R5       ; row
	MOV R1, R6       ; column
	MOV R2, R7       ; channel
	BL  verticalDifference
	ADD R9, R0
	MOV R0, R5       ; row
	MOV R1, R6       ; column
	MOV R2, R7       ; channel
	BL  horizontalDifference
	ADD R9, R0
	SUB R7, #1
	B   forEachChannel
endForEachChannel

	MOV R0, R9
	MOV R1, #6
	BL divide
	MOV R2, #0
	CMP R0, R8
	BLT notAnEdge
	MOV R2, #1
notAnEdge

	MOV R0, R5
	MOV R1, R6
	BL  markEdge
	SUB R6, #1
	B   forEachColumn
endForEachColumn

	SUB R5, #1
	B   forEachRow
endForEachRow
	BL colourEdges
	LDMFD sp!, {R4 - R8, lr}
	BX lr
	
	; ## Colour Edges ##
	; 
	; Description:
	; Checks for pixels marked as edges in the
	; alpha channel, and colours them in
	; accordingly.
	;
	; ## Interface ##
	;
	; Parameters:
	; None
	
colourEdges
	STMFD sp!, {R4 - R8, lr}
	MOV R8, R0
	BL	getPicAddr
	MOV	R4, R0
	BL	getPicHeight
	MOV	R5, R0
	
colourEachRow
	CMP R5, #0
	BLT endColourEachRow
	BL	getPicWidth
	MOV	R6, R0
colourEachColumn
	CMP R6, #0
	BLT endColourEachColumn
	
	MOV R0, R5       ; row
	MOV R1, R6       ; column
	MOV R2, #3       ; alpha channel
	BL getChannel
	
	MOV R7, #2
	CMP R0, #0
	BGT forEachEdge
	
forEachNonEdge
	CMP R7, #0
	BLT endForEachEdge
	MOV R0, R5       ; row
	MOV R1, R6       ; column
	MOV R2, R7       ; alpha channel
	MOV R3, #0       ; colour
	BL  storeChannel
	SUB R7, #1
	B   forEachNonEdge
endForEachNonEdge
	
forEachEdge
	CMP R7, #0
	BLT endForEachEdge
	MOV R0, R5       ; row
	MOV R1, R6       ; column
	MOV R2, R7       ; alpha channel
	MOV R3, #255       ; colour
	BL  storeChannel
	SUB R7, #1
	B   forEachEdge
endForEachEdge

	SUB R6, #1
	B   colourEachColumn
endColourEachColumn

	SUB R5, #1
	B   colourEachRow
endColourEachRow

	LDMFD sp!, {R4 - R8, lr}
	BX lr
	
	; ## Mark Edge ##.
	;
	; ## Interface ##
	;
	; Description:
	; In the (hopefully unused) alpha channel
	; This marks whether a pixel is an edge.
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - is edge (boolean)
	; 
	; Returns:
	; R0 - new channel value
	
markEdge
	STMFD sp!, {lr}
	MOV R3, R2
	MOV R2, #3
	BL storeChannel
	LDMFD sp!, {lr}
	BX lr
	
	; ## Vertical Difference ##
	;
	; Description:
	; Takes a channel's coordinates and returns
	; the difference between it and its vertical
	; neighbours.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; 
	; Returns:
	; R0 - new channel value

verticalDifference
	STMFD sp!, {R4 - R9, lr}
	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	
	BL getChannel
	MOV R9, R0    ; Current pixel's value
	MOV R7, #0    ; Total of differences of neighbours
	MOV R8, #0    ; Pixels used

	CMP R4, #1
	BLT dontCountTop
	ADD R8, #1
	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	SUB R0, #1
	BL  getChannel
	ADD R7, R0
dontCountTop

	BL getPicHeight
	CMP R4, R0
	BGE dontCountBottom
	ADD R8, #1
	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	ADD R0, #1
	BL  getChannel
	ADD R7, R0
dontCountBottom

	CMP R8, #1
	BLE underTwoVerticals
	MOV R7, R7, LSR #1
underTwoVerticals

	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	BL getChannel
	SUB R0, R0, R7
	BL absoluteValue
	LDMFD sp!, {R4 - R9, lr}
	BX lr
	
	; ## Horizontal Difference ##
	;
	; Description:
	; Takes a channel's coordinates and returns
	; the difference between it and its horizontal
	; neighbours.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; 
	; Returns:
	; R0 - new channel value
	
horizontalDifference
	STMFD sp!, {R4 - R9, lr}
	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	
	BL getChannel
	MOV R9, R0    ; Current pixel's value
	MOV R7, #0    ; Total of differences of neighbours
	MOV R8, #0    ; Pixels used

	CMP R5, #1
	BLT dontCountLeft
	ADD R8, #1
	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	SUB R1, #1
	BL  getChannel
	ADD R7, R0
dontCountLeft

	BL getPicWidth
	CMP R5, R0
	BGE dontCountRight
	ADD R8, #1
	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	ADD R1, #1
	BL  getChannel
	ADD R7, R0
dontCountRight

	CMP R8, #1
	BLE underTwoHorizontals
	MOV R7, R7, LSR #1
underTwoHorizontals

	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	BL getChannel
	SUB R0, R0, R7
	BL absoluteValue
	LDMFD sp!, {R4 - R9, lr}
	BX lr
	
	; ## Get Channel ##
	;
	; Description:
	; Takes a row and column, returns pixel
	; at that position.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; 
	; Returns:
	; R0 - channel value
	
getChannel
	STMFD sp!, {R4 - R7, lr}

	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	
	SUB R4, #1
	SUB R5, #1
	BL	getPicWidth
	MUL R4, R0, R4
	ADD R4, R5
	
	MOV R2, #4
	MUL R4, R2, R4
	ADD R4, R6
	BL  getPicAddr
	ADD R4, R0
	LDRB R0, [R4]
	
	LDMFD sp!, {R4 - R7, lr}
	BX LR
	
	; ## Store Channel ##
	;
	; Description:
	; Takes a row and column, returns pixel
	; at that position.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; R3 - value
	; 
	; Returns:
	; None
	
storeChannel
	STMFD sp!, {R4 - R7, lr}

	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	MOV R7, R3
	
	SUB R4, #1
	SUB R5, #1
	BL	getPicWidth
	MUL R4, R0, R4
	ADD R4, R5
	
	MOV R2, #4
	MUL R4, R2, R4
	ADD R4, R6
	BL  getPicAddr
	ADD R4, R0
	STRB R7, [R4]
	
	LDMFD sp!, {R4 - R7, lr}
	BX LR
	
	; ## Divide ##
	;
	; Interface:
	;
	; Parameters:
 	; R0 - dividend
	; R1 - divisor
	;
	; Returns:
	; R0 - quotient
	; R1 - Remainder     (Because why not, it's calculated anyway)
	;
	; Registers:
	; *See parameters*
	
divide
	MOV R2, #0
subtractAgain
	CMP R0, R1
	BLT endSubtraction
	ADD R2, #1
	SUB R0, R1
	B   subtractAgain
endSubtraction
	MOV R1, R0
	MOV R0, R2
	BX lr	
	
	; ## Absolute Value ##.
	;
	; ## Interface ##
	;
	; Description:
	; Returns the absolute value of a
	; given integer.
	;
	; Parameters:
	; R0 - number
	; 
	; Returns:
	; R0 - new number
	
absoluteValue
	STMFD sp!, {lr}
	MOV R1, #0
	CMP R0, R1
	BGE computedAbsoluteValue
	SUB R0, R1, R0
computedAbsoluteValue
	LDMFD sp!, {lr}
	BX lr

	
stop	B	stop

	END	