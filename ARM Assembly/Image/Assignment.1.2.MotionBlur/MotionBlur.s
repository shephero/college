	AREA	MotionBlur, CODE, READONLY
	IMPORT	main
	IMPORT	getPicAddr
	IMPORT	putPic
	IMPORT	getPicWidth
	IMPORT	getPicHeight
	EXPORT	start
	PRESERVE8

start

	MOV R0, #7    ; diameter
	BL blur
	BL putPic
	B stop
	
	;-------------------------------------------------------------------------------
	
	; # Subroutines: #
	;
	; ## Blur ##
	; 
	; Description:
	; Takes a radius and, for every pixel
	; changes it to the average of $RADIUS pixels to
	; the North-West and South-East
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - radius
	
blur
	STMFD sp!, {R4 - R8, lr}
	MOV R8, R0
	BL	getPicAddr
	MOV	R4, R0
	BL	getPicHeight
	MOV	R5, R0
forEachRow
	CMP R5, #0
	BLT endForEachRow
	BL	getPicWidth
	MOV	R6, R0
forEachColumn
	CMP R6, #0
	BLT endForEachColumn
	MOV R7, #2
forEachChannel
	CMP R7, #0
	BLT endForEachChannel
	MOV R0, R5    ; row
	MOV R1, R6    ; column
	MOV R2, R7    ; channel
	MOV R3, R8    ; radius
	BL  blurChannel
	MOV R3, R0    ; value
	MOV R0, R5    ; row
	MOV R1, R6    ; column
	MOV R2, R7    ; channel
	BL storeChannel
	SUB R7, #1
	B   forEachChannel
endForEachChannel
	SUB R6, #1
	B   forEachColumn
endForEachColumn
	SUB R5, #1
	B   forEachRow
endForEachRow
	LDMFD sp!, {R4 - R8, lr}
	BX lr
	
	; ## Blur Channel ##
	;
	; Description:
	; Takes the average of $DIAMETER pixels,
	; taken from the top-left and bottom-right 
	; of the reference pixel.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; R3 - diameter
	; 
	; Returns:
	; R0 - new channel value

blurChannel
	STMFD sp!, {R4 - R10, lr}
	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	MOV R7, R3
	
	MOV R0, R3
	MOV R1, #2
	BL divide
	MOV R8, R0    ; Pixels in each direction to average
	
	MOV R9, #0    ; Pixel value
	MOV R10, #0   ; Pixels counted
	MOV R0, #0
	SUB R11, R0, R8    ; Place in Diameter
iterateOverDiameter

	CMP R11, R8
	BGT endIterateOverDiameter
	ADD R0, R4, R11
	CMP R0, #0
	BLT skipPlaceInDiameter
	CMP R0, R4
	BGT skipPlaceInDiameter
	ADD R1, R5, R11
	CMP R1, #0
	BLT skipPlaceInDiameter
	CMP R1, R5
	BGT skipPlaceInDiameter
	
	MOV R2, R6
	BL getChannel
	ADD R9, R0
	ADD R10, #1
skipPlaceInDiameter
	ADD R11, #1
    B iterateOverDiameter
endIterateOverDiameter
	MOV R0, R9
	MOV R1, R10
	BL divide
	LDMFD sp!, {R4 - R10, lr}
	BX lr
	
	; ## Get Channel ##
	;
	; Description:
	; Takes a row and column, returns pixel
	; at that position.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; 
	; Returns:
	; R0 - channel value
	
getChannel
	STMFD sp!, {R4 - R7, lr}

	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	
	SUB R4, #1
	SUB R5, #1
	BL	getPicWidth
	MUL R4, R0, R4
	ADD R4, R5
	
	MOV R2, #4
	MUL R4, R2, R4
	ADD R4, R6
	BL  getPicAddr
	ADD R4, R0
	LDRB R0, [R4]
	
	LDMFD sp!, {R4 - R7, lr}
	BX LR
	
	; ## Store Channel ##
	;
	; Description:
	; Takes a row and column, returns pixel
	; at that position.
	;
	; ## Interface ##
	;
	; Parameters:
	; R0 - row
	; R1 - column
	; R2 - channel
	; R3 - value
	; 
	; Returns:
	; None
	
storeChannel
	STMFD sp!, {R4 - R7, lr}

	MOV R4, R0
	MOV R5, R1
	MOV R6, R2
	MOV R7, R3
	
	SUB R4, #1
	SUB R5, #1
	BL	getPicWidth
	MUL R4, R0, R4
	ADD R4, R5
	
	MOV R2, #4
	MUL R4, R2, R4
	ADD R4, R6
	BL  getPicAddr
	ADD R4, R0
	STRB R7, [R4]
	
	LDMFD sp!, {R4 - R7, lr}
	BX LR
	
	; ## Divide ##
	;
	; Interface:
	;
	; Parameters:
 	; R0 - dividend
	; R1 - divisor
	;
	; Returns:
	; R0 - quotient
	; R1 - Remainder     (Because why not, it's calculated anyway)
	;
	; Registers:
	; *See parameters*
	
divide
	MOV R2, #0
subtractAgain
	CMP R0, R1
	BLT endSubtraction
	ADD R2, #1
	SUB R0, R1
	B   subtractAgain
endSubtraction
	MOV R1, R0
	MOV R0, R2
	BX lr	
	
stop	B	stop

	END	