	AREA	Adjust, CODE, READONLY
	IMPORT	main
	IMPORT	getPicAddr
	IMPORT	putPic
	IMPORT	getPicWidth
	IMPORT	getPicHeight
	EXPORT	start
	PRESERVE8

start
	
	MOV R0, #50        ; contrast
	MOV R1, #0         ; brightness
	BL brightnessAndContrast
	BL	putPic
	B stop
	; BL	putPic		; re-display the updated image
	
	
	
	; # Subroutines: #
	;
	; ## Brightness And Contrast Channel ##
	;
	; Iterates over each channel of each pixel, 
	; Loads the value, calls brightnessAndContrastChannel
	; on each one, and stores back the returned value
	;
	; Interface:
	;
	; Parameters:
	; R0 - contrast delta
	; R1 - brightness delta
	;
	; Returns:
	; R0 - New channel value
	
brightnessAndContrast
	STMFD sp!, {R4-R8, lr}
	MOV R7, R0
	MOV R8, R1
	
	BL	getPicAddr
	MOV	R4, R0
	BL	getPicHeight
	MOV	R5, R0
	BL	getPicWidth
	MOV	R6, R0

	MUL R5, R6, R5
	MOV R0, #4
	MUL R5, R0, R5
	ADD R5, R4
	SUB R5, #1
forEachPixel
	CMP R4, R5
	BHI endPixelLoop
	
	ADD R6, R4, #2
forEachChannel
	CMP R4, R6
	BHI endChannelLoop
	
	LDRB R2, [R4]         ; Get channel
	MOV R0, R7            ; contrast delta
	MOV R1, R8            ; brightness delta
	BL  brightnessAndContrastChannel
	STRB R0, [R4]         ; Store channel
	
	ADD R4, #1
	B   forEachChannel
endChannelLoop
	ADD R4, #1            ; Skip alpha channel
	B   forEachPixel
endPixelLoop
	LDMFD sp!, {R4-R8, lr}
	BX lr
	
	; ## Brightness And Contrast Channel ##
	;
	; Formula:
	; colour = b + (colour * c) / 16;
	; where c is the change in contrast,
	; b is the change in brightness,
	; and colour is one of red, green, or blue
	; channels.
	;
	; Interface:
	;
	; Parameters:
	; R0 - contrast delta
	; R1 - brightness delta
	; R2 - current channel value
	;
	; Returns:
	; R0 - New channel value

brightnessAndContrastChannel
	STMFD sp!, {R4, R5, lr}
	MOV R4, R1
	MOV R1, R2
	BL contrastChannel
	MOV R1, R0
	MOV R0, R4
	BL brightnessChannel
	LDMFD sp!, {R4, R5, lr}
	BX lr
	
	; ## Contrast Channel ##
	;
	; Formula:
	; colour = (colour * delta) / 16;
	; where delta is the change in contrast,
	; and colour is one of red, green, or blue
	; channels.
	;
	; Interface:
	;
	; Parameters:
	; R0 - contrast delta
	; R1 - current channel value
	;
	; Returns:
	; R0 - New channel value

contrastChannel
	STMFD sp!, {lr}
	MUL R0, R1, R0
	MOV R1, #16
	BL divide
	BL limitChannel
	LDMFD sp!, {lr}
	BX lr
	
	; ## Brightness Channel ##
	;
	; Formula:
	; colour = colour + delta;
	; where delta is the change in brightness,
	; and colour is one of red, green, or blue
	; channels.
	;
	; Interface:
	;
	; Parameters:
	; R0 - brightness delta
	; R1 - current channel value
	;
	; Returns:
	; R0 - New channel value
	
brightnessChannel
	STMFD sp!, {lr}
	ADD R0, R1, R0
	BL limitChannel
	LDMFD sp!, {lr}
	BX lr
	
	; ## Limit Channel ##
	;
	;
	;
	; Interface:
	;
	; Parameters:
 	; R0 - current channel value
	;
	; Returns:
	; R0 - New channel value
	
limitChannel
	CMP R0, #256
	BLT notTooHigh
	MOV R0, #255
notTooHigh
	CMP R0, #-1
	BGT notTooLow
	MOV R0, #0
notTooLow
	BX lr
	
	; ## Divide ##
	;
	; Interface:
	;
	; Parameters:
 	; R0 - dividend
	; R1 - divisor
	;
	; Returns:
	; R0 - quotient
	; R1 - Remainder     (Because why not, it's calculated anyway)
	;
	; Registers:
	; *See parameters*
	
divide
	MOV R2, #0
subtractAgain
	CMP R0, R1
	BLT endSubtraction
	ADD R2, #1
	SUB R0, R1
	B   subtractAgain
endSubtraction
	MOV R1, R0
	MOV R0, R2
	BX lr
	

stop	B	stop


	END	