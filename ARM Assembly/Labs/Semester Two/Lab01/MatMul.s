	AREA	MatMul, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R0, =matR
	LDR	R1, =matA
	LDR	R2, =matB
	LDR	R3, =N

	MOV R4,#0
forOne						;for (int i = 0;i < N;i++){
	CMP R4,R3
	BEQ endForOne
	MOV R5,#0
forTwo							;for (int j = 0;j < N;j++){
	CMP R5,R3
	BEQ endForTwo
	MOV R7,#0
	MOV R6,#0
forThree							;for (int k = 0;k < N;k++){
	CMP R6,R3
	BEQ endForThree
	
	LSL R8,R4,#2
	ADD R8,R8,R6		
	LDR R8,[R1,R8,LSL #2]
	
	LSL R9,R6,#2
	ADD R9,R9,R5		
	LDR R9,[R2,R9,LSL #2]
	
	MUL R10,R8,R9
	ADD R7,R7,R10

	ADD R6,R6,#1
	B forThree
endForThree

	LSL R8,R4,#2
	ADD R8,R8,R5
	STR R7,[R0,R8,LSL #2]

	ADD R5,R5,#1
	B forTwo
endForTwo
	
	ADD R4,R4,#1
	B forOne
endForOne
	
stop	B	stop


	AREA	TestArray, DATA, READWRITE

N	EQU	4

matA	DCD	5,4,3,2
	DCD	3,4,3,4
	DCD	2,3,4,5
	DCD	4,3,4,3

matB	DCD	5,4,3,2
	DCD	3,4,3,4
	DCD	2,3,4,5
	DCD	4,3,4,3

matR	SPACE	64

	END	