	AREA	ArrayMove, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R0, =array
	LDR	R1, =N
	LDR	R2, =6		; move from index
	LDR	R3, =3		; move to index

	LDR R5,[R0,R2,LSL #2]

	CMP R2,R3
	BEQ stop
	BGT skip
	
forOne
	CMP R2,R3
	BEQ endForOne
	ADD R4,R2,#1
	LDR R4,[R0,R4,LSL #2]
	STR R4,[R0,R2,LSL #2]
	ADD R2,R2,#1
	B forOne
endForOne
	STR R5,[R0,R3,LSL #2]
	
	B stop
skip
	
forTwo
	CMP R3,R2
	BEQ endForTwo
	SUB R4,R2,#1
	LDR R4,[R0,R4,LSL #2]
	STR R4,[R0,R2,LSL #2]
	SUB R2,R2,#1
	B forTwo
endForTwo
	STR R5,[R0,R3,LSL #2]	
	
stop	B	stop


	AREA	TestArray, DATA, READWRITE

N	equ	9
array	DCD	7,2,5,9,1,3,2,3,4

	END	