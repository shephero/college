	AREA	Val2Dec, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R4, =7891
	LDR	R5, =decstr
	
	MOV R0,R4
	MOV R1,R5
	
	BL VAL2DEC
	
	MOV R0,R1
	
	B stop

;
; Divide Subroutine
;	Counts quotient and remainder upon division
;	Parameters : R0 (Dividend), R1 (Divisor)
;	Output     : R0 (Quotient), R1 (Remainder)
DIVIDE
	STMFD sp!, {r4, lr}
	MOV R4,#0
while
	CMP R0,R1
	BLO endWhile
	SUB R0,R1
	ADD R4,#1
	B while
endWhile
	MOV R1,R0
	MOV R0,R4
	LDMFD sp!, {r4, pc}

;
; Shift and Store Subroutine
;	Shifts memory addresses right and store byte at start
;	Parameters : R0 (Value), R1 (Start Memory Address), R2 (End Memory Address)
;	Output     : None
SHIFTANDSTORE
	STMFD sp!, {r4-r5,lr}
fortwo
	CMP R2,R1
	BEQ endfortwo
	SUB R5,R2,#1
	LDRB R4,[R5]
	STRB R4,[R2]
	MOV R2,R5
	B fortwo
endfortwo
	STRB R0,[R1]
	LDMFD sp!, {r4-r5,pc}

;
; Value to Decimal Subroutine
;	Converts an integer to decimal and saves ASCII characters in memory
;	Parameters : R0 (Integer), R1 (Memory Address to Save in)
;	Output     : Stores ASCII chars in memory
VAL2DEC
	STMFD sp!, {r4-r6,lr}
	MOV R4,R0
	MOV R5,R1
	MOV R6,R1
forone
	CMP R4,#0
	BEQ endforone
	
	MOV R0,R4
	MOV R1,#0xA
	
	BL DIVIDE
	
	MOV R4,R0
	ADD R1,#'0'
	
	MOV R0,R1
	MOV R1,R5
	ADD R6,#1
	MOV R2,R6
	
	BL SHIFTANDSTORE
	
	B forone
endforone
	LDMFD sp!, {r4-r6,pc}
	
	
stop	B	stop


	AREA	TestString, DATA, READWRITE

decstr	SPACE	16

	END	