	AREA	BubbleSort, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R4, =array	; address of array
	LDR	R5, =arrayN	; address of array size
	LDR	R5, [R5]	; load array size

	MOV R0,R4
	MOV R1,R5
	
	BL SORT


	
stop	B	stop

;
; Swap
; Swaps values of 2 memory addresses
; Parameters : Array address (R0), Index I (R1), Index J (R2)

SWAP
	LDMFD sp!,{r4-r5,lr}
	LDR R4,[R0,R1,LSL #2]		; val_a = load(a)
	LDR R5,[R0,R2,LSL #2]		; val_b = load(b)
	STR R5,[R0,R1,LSL #2]		; store(b,val_a)
	STR R4,[R0,R2,LSL #2]		; store(a,val_b)
	STMFD sp!,{r4-r5,pc}

;
; Sort
; Sorts an array of values
; Parameters : R0 (Start address), R1 (size of array)

SORT
	LDMFD sp!,{r4-r5,r11,lr}
	MOV R11,R1
forone
	MOV R4,#0				;swapped = false
	MOV R5,#1				;for index = 1,N do
fortwo
	CMP R11,R5				
	BEQ endfortwo
	SUB R2,R5,#1
	LDR R2,[R0,R2,LSL #2]
	LDR R1,[R0,R5,LSL #2]	
	CMP R2,R1				;	if array[i-1] > array[i] then
	BHS skip				;		swap()
	SUB R2,R5,#1
	MOV R1,R5
	BL SWAP
skip
	ADD R5,#1				;index ++
	B fortwo
endfortwo
	CMP R4,#0				;if !swapped then break
	BEQ endforone
	B forone
endforone
	STMFD sp!,{r4-r5,r11,pc}

	AREA	TestArray, DATA, READWRITE

; Array Size
arrayN	DCD	15

; Array Elements
array	DCD	33,17,18,92,49,28,78,75,22,13,19,13,8,44,35

	END	