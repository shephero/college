	AREA	Expressions, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R1, =5	; x = 5
	LDR	R2, =6	; y = 6
	
	LDR R3, =3
	LDR R4, =5
	MUL R5, R1, R1 ; x^2
	MUL R0, R5, R3 ; 3x^2
	MUL R3, R1, R4 ; 5x
	ADD R0, R3, R0 ; x^2 + 5x
	
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	LDR	R1, =5	; x = 5
	LDR	R2, =6	; y = 6
	;reset x and y for the second one
	
	LDR R3, =2
	LDR R4, =6
	LDR R5, =3
	MUL R6, R1, R1 ; x^2
	MUL R7, R1, R2 ; xy
	MUL R8, R2, R2 ; y^2
	MUL R0, R6, R3 ; 2x^2
	MUL R3, R4, R7 ; 6xy
	MUL R4, R5, R8 ; 3y^2
	ADD R0, R0, R3 ; 2x^2 + 6xy
	ADD R0, R0, R4 ; 2X^2 + 6xy + 3y^2
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	LDR	R1, =5	; x = 5
	LDR	R2, =6	; y = 6
	;reset x and y for the third one
	
	LDR R3, =4
	LDR R4, =3
	LDR R5, =8
	MUL R6, R1, R1 ; x^2
	MUL R0, R6, R1 ; x^3
	MUL R7, R6, R3 ; 4x^2
	SUB R0, R0, R7 ; x^3 - 4x^2
	MUL R6, R4, R1 ; 3x
	ADD R0, R0, R6 ; x^3 - 4x^2 + 3x
	ADD R0, R0, R5 ; x^3 - 4x^2 + 3x + 8
	
	
stop	B	stop

	END	