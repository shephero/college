	AREA	AsciiAdd, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R1, ='2'	; R1 = 0x32 (ASCII symbol '2')
	LDR	R2, ='4'	; R2 = 0x34 (ASCII symbol '4')	
	
	SUB R1, R1, #0x30 ; convert to number
	SUB R2, R2, #0x30 ; convert to number
	
	ADD R0, R1, R2 ; add numbers
	ADD R0, R0, #0x30 ; convert back to ASCII
	
stop	B	stop

	END	