	AREA	AsciiValue, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R4, ='2'	; Load '2','0','3','4' into R4...R1
	LDR	R3, ='0'	;
	LDR	R2, ='3'	;
	LDR	R1, ='4'	;
	
	SUB R4, R4, #0x30 ; convert all to numbers
	SUB R3, R3, #0x30 ;
	SUB R2, R2, #0x30 ;
	SUB R1, R1, #0x30 ;
	
	LDR R5, =10
	MUL R6, R5, R2 ; multiply digit in 10s position by 10
	
	LDR R5, =100
	MUL R7, R5, R3 ; multiply digit in 100s position by 100
	
	LDR R5, =1000
	MUL R8, R5, R4 ; multiply digit in 1000s position by 1000
	
	ADD R0, R8, R7
	ADD R0, R0, R6
	ADD R0, R0, R1 ; add all converted numbers
		
stop	B	stop

	END	