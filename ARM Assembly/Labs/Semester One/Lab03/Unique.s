	AREA	Unique, CODE, READONLY
	IMPORT	main
	EXPORT	start

start

	LDR	R1, =VALUES
	LDR R2, =COUNT
	LDR R2, [R2]
	LDR R0, =0
	
forEachNumber
	CMP R2, #0		; stop if count == 0
	BEQ isUnique
	LDR R3, [R1]	; load value for first loop
	SUB R2, R2, #1	; counter--
	ADD R1, #4		; address += 4
	
	MOV R4, R1		; Pointer to mess with for second loop
	MOV R5, R2		; Count to mess with for second loop
forEveryOtherNumber
	CMP R5, #0
	BEQ forEachNumber
	LDR R6, [R4]
	CMP R6, R3
	BEQ stop
	ADD R4, #4
	SUB R5, #1
	B forEveryOtherNumber
	
	
isUnique
	MOV R0, #1
	
stop	B	stop


	AREA	TestData, DATA, READWRITE
	
COUNT	DCD	10
VALUES	DCD	5, 2, 7, 4, 13, 20, 18, 8, 9, 18


	END	