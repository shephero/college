	AREA	GCD, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	
	LDR r2, =24  ;a
	LDR r3, =32  ;b
	LDR r0, =0
	LDR r1, =0

	
WHILE
	CMP r2, r3
	BEQ ENDWH  ;If a, b equal, end while loop
	
	CMP r2, r3
	BGE AISBIGGER  ;if a >= b, skip to AISBIGGER
	SUB r3, r3, r2  ;b = b - a
	B TOTOP
AISBIGGER
	SUB r2, r2, r3  ;a = a - b
TOTOP
	B WHILE  ;Branch to top
ENDWH

	MOV r0, r2

	
stop	B	stop

	END	