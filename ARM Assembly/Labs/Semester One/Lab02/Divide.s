	AREA	Divide, CODE, READONLY
	IMPORT	main
	EXPORT	start

start

	LDR r2, =120  ;Load dividend a in r2
	LDR r3, =33  ;Load divisor b in r3
	LDR r0, =0
	LDR r1, =0  ;Clear r0 and r1
	
	CMP r3, #0
	BEQ ENDWH  ;If divisor is zero, end program
	
	MOV r1, r2  ;Remainder in r1 = dividend
WHILE
	CMP r1, r3  ;if (remainder < divisor)
	BLO ENDWH  ;end program
	ADD r0, r0, #1  ;Add 1 to the quotient
	SUB r1, r1, r3  ;Subtract the divisor from the dividend
	B WHILE
ENDWH
	
stop	B	stop

	END	