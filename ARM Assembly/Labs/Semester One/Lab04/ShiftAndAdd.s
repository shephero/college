	AREA	ShiftAndAdd, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R1, =9
	LDR	R2, =10
	MOV R0, #0

	MOV R3, #0
	B COUNTONES
	
COUNTONESWASZERO
	ADD R3, #1			;position++
COUNTONES				;counts the bits with '1' and shifts depending on their position
	CMP R3, #32			;R3 is the current position checker. Stops when it reaches the end of the register
	BEQ stop
	MOV R5, R2			;copies R2 as it is mostly cleared and needs to be kept for each loop
	LSR R5, R3
	LSL R5, #31			;clears all but last bit
	LSR R5, #31
	CMP R5, #1
	BNE COUNTONESWASZERO
	MOV R4, R1, LSL R3		;shifts depending on position of bit
	ADD R0, R4
	ADD R3, #1				;postition++
	B COUNTONES
	
stop	B	stop


	END