	AREA	Shift64, CODE, READONLY
	IMPORT	main
	EXPORT	start

start
	LDR	R0, =0x13131313
	LDR	R1, =0x13131313
	LDR	R2, =2
	
	CMP R2, #0
	BLT whileNeg			;if negative, uses other loop to shift the other way
	
whilePos					;for positive values
	CMP R2, #0
	BEQ endwhile
	SUB R2, #1
	MOVS R1, R1, LSR #1		;if carried
	BCS carriedOne
	MOV R0, R0, LSR #1
	B whilePos
carriedOne
	MOV R0, R0, LSR #1
	ORR  R0, R0, #0x80000000	;puts 1 in most significant bit
	B whilePos
	
whileNeg					;for negative values;
	CMP R2, #0;
	BEQ endwhile
	ADD R2, #1
	MOVS R0, R0, LSL #1		;if carried
	BCS carriedOne2
	MOV R1, R1, LSL #1
	B whileNeg
carriedOne2
	MOV R1, R1, LSL #1
	ORR  R1, R1, #0x00000001		;puts 1 in least significant bit
	B whileNeg

	
endwhile
	
stop	B	stop


	END
		