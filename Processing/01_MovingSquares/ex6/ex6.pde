void setup() {
  size(1024, 600);
  frameRate(60);
  surface.setResizable(!record_video);
}

//Settings
int square_size = 50;
boolean tails = true;
boolean background_flash = false;
boolean background_sine = true;
boolean record_video = false;


int rect_one_horiz_pos = 0;
int rect_two_horiz_pos = width;
float position_seed = 0;
int vertical_position = 0;

float rect_col_seed = 0;
int bg_col;
int bg_col_one = 0;
int bg_col_two = 240;

Tail tail_one = new Tail();
Tail tail_two = new Tail();

class Tail {
  ArrayList<Integer> x_points = new ArrayList<Integer>();
  ArrayList<Integer> y_points = new ArrayList<Integer>();
  
  void add_point(int x_value, int y_value) {
    if (x_points.size() > 30) {
      x_points.remove(x_points.size() - 1);
      y_points.remove(y_points.size() - 1);
    }
    x_points.add(0, x_value);
    y_points.add(0, y_value);
  }
  
  void clear() {
    x_points.clear();
    y_points.clear();
  }
  
  void draw() {
    for(int i = 0; i < x_points.size(); i++) {
      ellipse(x_points.get(i), y_points.get(i), 4, 4);
    }
  }
}

void rect_one() {
  if (rect_col_seed > 2*PI) { rect_col_seed = 0; }
  float rcol = 127.5 + 127.5 * cos(rect_col_seed * 51);
  float gcol = 127.5 + 127.5 * cos(rect_col_seed * 63);
  float bcol = 127.5 + 127.5 * cos(rect_col_seed * 75);
  if (tails) {
    if (rect_one_horiz_pos % 5 == 0) {
      tail_one.add_point(rect_one_horiz_pos + square_size/2, height/2 - (square_size / 2) + vertical_position + square_size/2);
    }
  }
  
  fill(rcol, gcol, bcol);
  rect(rect_one_horiz_pos, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  rect(rect_one_horiz_pos - width, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  if (rect_one_horiz_pos > width) {
    rect_one_horiz_pos -= width;
  }
  rect_one_horiz_pos += 3;
}

void rect_two() {
  float rcol = 127.5 + 127.5 * cos(rect_col_seed * 70);
  float gcol = 127.5 + 127.5 * cos(rect_col_seed * 41);
  float bcol = 127.5 + 127.5 * cos(rect_col_seed * 62);
  if (tails) {
    if ((int) rect_two_horiz_pos % 5 == 0) {
      tail_two.add_point(rect_two_horiz_pos + square_size/2, height/2 - (square_size / 2) + vertical_position + square_size/2);
    }
  }

  fill(rcol, gcol, bcol);
  rect(rect_two_horiz_pos, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  rect(rect_two_horiz_pos + width, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  if (rect_two_horiz_pos < 0 - square_size) {
    rect_two_horiz_pos += width;
  }
  rect_two_horiz_pos -= 1;
}

void draw() {
  if (background_flash) {
    if(sin(rect_col_seed * 50) > 0) {bg_col = bg_col_one;}
    else {bg_col = bg_col_two;}
  }
  if (background_sine) {bg_col = (int) (120 + 120 * sin(rect_col_seed * 10));}
  if (!background_sine && !background_flash) {bg_col = bg_col_two;}
  background(bg_col);
  if (position_seed > 2*PI) {
    position_seed = 0;
  }
  vertical_position = (int) (square_size * sin(position_seed));
  position_seed += 0.05;
  if (background_flash) {stroke((bg_col == bg_col_two)?bg_col_one:bg_col_two);}
  if (background_sine) {stroke(120 - 120 * sin(rect_col_seed * 10));}
  if (!background_sine && !background_flash) {stroke(bg_col_one);}
  fill(bg_col);
  tail_one.draw();
  tail_two.draw();
  rect_one();
  rect_two();
  rect_col_seed += 0.001;
  if (record_video) {saveFrame("line-######.png");}
}