void setup() {
  size(1024, 600);
}

int square_size = 50;


int rect_one_horiz_pos = 0;
int rect_two_horiz_pos = width;
float position_seed = 0;
float vertical_position = 0;

float rect_col_seed = 0;
int bg_col = 0;

void rect_1() {
  if (rect_col_seed > 2*PI) { rect_col_seed = 0; }
  float rcol = 127.5 + 127.5 * cos(rect_col_seed * 51);
  float gcol = 127.5 + 127.5 * cos(rect_col_seed * 63);
  float bcol = 127.5 + 127.5 * cos(rect_col_seed * 75);
  fill(rcol, gcol, bcol);
  rect(rect_one_horiz_pos, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  rect(rect_one_horiz_pos - width, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  if (rect_one_horiz_pos > width) {
    rect_one_horiz_pos -= width;
  }
  rect_one_horiz_pos += 1;
}

void rect_2() {
  float rcol = 127.5 + 127.5 * cos(rect_col_seed * 71);
  float gcol = 127.5 + 127.5 * cos(rect_col_seed * 40);
  float bcol = 127.5 + 127.5 * cos(rect_col_seed * 52);
  fill(rcol, gcol, bcol);
  rect(rect_two_horiz_pos, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  rect(rect_two_horiz_pos + width, height/2 - (square_size / 2) + vertical_position, square_size, square_size);
  if (rect_two_horiz_pos < 0 - square_size) {
    rect_two_horiz_pos += width;
  }
  rect_two_horiz_pos -= 1;
}

void draw() {
  background(bg_col);
/*if(sin(rect_col_seed * 100) > 0) {
      bg_col = 0;
  }
  else {
    bg_col = 255;
  }*/
  
  if (position_seed > 2*PI) {
    position_seed = 0;
  }
  vertical_position = (int) (square_size * sin(position_seed));
  position_seed += 0.05;
  rect_1();
  rect_2();
  rect_col_seed += 0.001;
}