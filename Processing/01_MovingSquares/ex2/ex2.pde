void setup() {
  size(500, 500);
}

int i = 0;

void draw() {
  background(255, 100, 0);
  rect(i, 225, 50, 50);
  if (i > 450) {
    i = 0;
  }
  i += 2;
}