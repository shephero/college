void setup() {
  size(500, 500);
}

int rect_one_pos = 0;
int rect_two_pos = 450;

void rect_1() {
  rect(rect_one_pos, 275, 50, 50);
  rect(rect_one_pos - 500, 275, 50, 50);
  if (rect_one_pos > 500) {
    rect_one_pos -= 500;
  }
  rect_one_pos += 2;
}

void rect_2() {
  rect(rect_two_pos, 175, 50, 50);
  rect(rect_two_pos + 500, 175, 50, 50);
  if (rect_two_pos < -50) {
    rect_two_pos += 500;
  }
  rect_two_pos -= 2;
}

void draw() {
  background(255, 100, 0);
  rect_1();
  rect_2();
}