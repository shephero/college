void settings() {
  size(CANVAS_SIZE[0], CANVAS_SIZE[1], P2D);
}

void setup() {
  fullScreen();
  frameRate(60);
  font = loadFont("Ubuntu-Bold-48.vlw");
  float closestToBlockWidth = 0;
  for (int i = 1; i < CANVAS_SIZE[1]; i += 2) {
    if (Math.abs((CANVAS_SIZE[1] / i) - DOTTED_LINE_WIDTH) < Math.abs(closestToBlockWidth - DOTTED_LINE_WIDTH)) {
      closestToBlockWidth = (float) CANVAS_SIZE[1] / DOTS_IN_LINE;
      DOTS_IN_LINE = i;
    }
  }
  DOTTED_LINE_WIDTH = (float) CANVAS_SIZE[1] / DOTS_IN_LINE;
}

PFont font;
//  Constants
final int[] CANVAS_SIZE = {1500, 800};
final int SHADE_ONE = 25;
final int SHADE_TWO = 75;
final int SHADE_THREE = 200;
final int PADDLE_SPEED = 6;
final boolean IS_AI_GAME = false;
final boolean MOUSE_MODE = false;

//  Derived constants
final int STARTING_VELOCITY = CANVAS_SIZE[0]/200;
final int MAX_VELOCITY = CANVAS_SIZE[0]/70;
final int[] PADDLE_DIMENSIONS = {CANVAS_SIZE[0]/60, CANVAS_SIZE[1]/5};
final int PADDLE_MARGIN = CANVAS_SIZE[0]/20;
final int BALL_RADIUS = min(CANVAS_SIZE[1]/60, CANVAS_SIZE[0]/60);
final float[] CANVAS_CENTRE = {(float)CANVAS_SIZE[0]/2, CANVAS_SIZE[1]/2};

//  Almost Constants
float DOTTED_LINE_WIDTH = ((float)CANVAS_SIZE[0])/80;
int DOTS_IN_LINE;

Paddle paddleOne = new Paddle(true, PADDLE_MARGIN, false);
Paddle paddleTwo = new Paddle(false, PADDLE_MARGIN, IS_AI_GAME);
Ball ball = new Ball(BALL_RADIUS);
Score scoreOne = new Score(true);
Score scoreTwo = new Score(false);
FrameCounter frameCounter = new FrameCounter();
boolean leftWonLast = false;

private enum GameState {
  Playing,
  EndGame,
  NewRound,
}

GameState currentState = GameState.NewRound;


void draw() {
  switch (currentState) {
    case Playing:
      playing();
      break;
    case EndGame:
      endGame(frameCounter);
      break;
    case NewRound:
      newRound();
      break;
  }
}

class FrameCounter {
  int counter;
  
  FrameCounter() {
    counter = 0;
  }
  
  void iterate() {
    counter++;
  }
  
  void reset() {
    counter = 0;
  }
  
  int framesPassed() {
    return counter;
  }
};

void endGame(FrameCounter frames) {
  frames.iterate();
  if (frames.framesPassed() >= 180) {
    currentState = GameState.NewRound;
    frames.reset();
  }
  background(SHADE_ONE);
  displayMessage("Player " + (leftWonLast?"One":"Two") + " Wins!");
}

void newRound() {
  noStroke();
  background(SHADE_ONE);
  draw_stripe(DOTTED_LINE_WIDTH, DOTS_IN_LINE);
  paddleOne.draw();
  paddleTwo.draw();
  collision = detectAllCollisions(ball, paddleOne, paddleTwo);
  paddleOne.move(true);
  paddleTwo.move(false);
  dealWithCollisions();
  scoreOne.draw();
  scoreTwo.draw();
  if (!MOUSE_MODE) {
    scoreOne.countDown();
    scoreTwo.countDown();
  }
  else {
    displayMessage("Click To Start");
  }
}

void displayMessage(String message) {
    textFont(font); //<>//
    text(message, CANVAS_SIZE[0]/2, CANVAS_SIZE[1]/2);
}

void playing() {
  noStroke();
  background(SHADE_ONE);
  draw_stripe(DOTTED_LINE_WIDTH, DOTS_IN_LINE);
  ball.draw();
  ball.update();
  paddleOne.draw();
  paddleTwo.draw();
  collision = detectAllCollisions(ball, paddleOne, paddleTwo);
  paddleOne.move(true);
  paddleTwo.move(false);
  dealWithCollisions();
  scoreOne.draw();
  scoreTwo.draw();
}

void reset() {
  ball.reset();
  paddleOne.reset();
  paddleTwo.reset();
}

void hardReset() {
  ball.reset();
  paddleOne.reset();
  paddleTwo.reset();
  scoreOne.reset();
  scoreTwo.reset();
}