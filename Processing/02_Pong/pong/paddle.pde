class Paddle {
  int[] position = new int[2];
  int[] dimensions = new int[2];
  boolean onLeft;
  int margin;
  int velocity;
  boolean isAI;
  int lastFramePosition;
  
  Paddle(boolean inputOnLeft, int inputMargin, boolean inputIsAI) {
    position[0] = inputOnLeft? inputMargin : (CANVAS_SIZE[0] - inputMargin - PADDLE_DIMENSIONS[0]);
    position[1] = CANVAS_SIZE[1]/2 - PADDLE_DIMENSIONS[1]/2;
    dimensions = PADDLE_DIMENSIONS.clone();
    margin = inputMargin;
    onLeft = inputOnLeft;
    isAI = inputIsAI;
    lastFramePosition = position[1];
  }
  
  void reset() {
    //System.out.println(dimensions[1] + "  " + PADDLE_DIMENSIONS[1]);
    dimensions[1] = PADDLE_DIMENSIONS[1];
    position[1] = CANVAS_SIZE[1] - dimensions[1]/2;
  }
  
  float extraReflectAngle(int ballPosition) {
    //return (((ballPosition - dimensions[1]/2) / (dimensions/2)) * 500);
    return PI/4;
  }
  
  void limitToScreen(float movement) {
    if (!(position[1] <= 0) && movement < 0) {
      position[1] += movement;
      velocity = (int) movement;
    }
    else if (!(position[1] + dimensions[1] >= CANVAS_SIZE[1]) && movement > 0) {
      position[1] += movement;
      velocity = (int) movement;
    }
  }
  
  void limitMouseToScreen() {
    position[1] = constrain(mouseY - dimensions[1]/2, 0, CANVAS_SIZE[1] - dimensions[1]);
  }
  
  void move(boolean isPlayerOne) {
    lastFramePosition = position[1];
    if (isAI) {
      float difference = ball.position[1] - (position[1] + dimensions[1]/2);
      if (difference > 0) {limitToScreen(difference);}
      else if (difference < 0) {limitToScreen(difference);}
    }
    else {
      if (isPlayerOne) {
        if (!MOUSE_MODE) {
          if(pOneUpKey) {
            limitToScreen(-PADDLE_SPEED);
          }
          if(pOneDownKey) {
            limitToScreen(PADDLE_SPEED);
          }
        }
        else {
          limitMouseToScreen();
        }
      }
      else {
        if(pTwoUpKey) {
          limitToScreen(-PADDLE_SPEED);
        }
        if(pTwoDownKey) {
          limitToScreen(PADDLE_SPEED);
        }
      }
    }
  }
  
  void draw() {
    fill(SHADE_THREE);
    rect(position[0], position[1], dimensions[0], dimensions[1], 4); 
  }
  
  int calculateVelocity() {
    return position[1] - lastFramePosition;
  }
};