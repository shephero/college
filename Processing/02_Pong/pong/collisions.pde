CollisionType collision;

private enum CollisionType {
  goUp,
  goDown,
  goLeft,
  goRight,
  LeftWall,
  RightWall,
  None,
}

CollisionType detectAllCollisions(Ball ball, Paddle left, Paddle right) {
  if (ball.position[0] - ball.radius <= 0) {return CollisionType.LeftWall;}
  if (ball.position[0] + ball.radius >= CANVAS_SIZE[0]) {return CollisionType.RightWall;}
  if (ball.position[1] - ball.radius <= 0) {return CollisionType.goDown;}
  if (ball.position[1] + ball.radius >= CANVAS_SIZE[1]) {return CollisionType.goUp;}

  if (closestPaddleIsLeft(ball, left, right)) {
    return dealWithPartOfBallWithinPaddle(ball, left);
  }
  else {
    return dealWithPartOfBallWithinPaddle(ball, right);
  }
}

CollisionType dealWithPartOfBallWithinPaddle(Ball ball, Paddle paddle) {
  float[] positions = {leftOfPaddle(ball, paddle), topOfPaddle(ball, paddle), rightOfPaddle(ball, paddle), bottomOfPaddle(ball, paddle)};
  boolean withinBoundaries = true;
  float smallestDifference = CANVAS_SIZE[0];
  int positionOfLargest = 0;    // position of closest border
  for (int i = 0; i < positions.length; i++) {
     if (positions[i] > 0) {
       withinBoundaries = false;
     }
     if (Math.abs(positions[i]) < Math.abs(smallestDifference)) {
       positionOfLargest = i;
       smallestDifference = positions[i];
     }
  }
  if (withinBoundaries) {
    if (positionOfLargest == 0) {
      ball.increaseVelocity();
      return CollisionType.goLeft;
    }
    if (positionOfLargest == 2) {
      ball.increaseVelocity();
      return CollisionType.goRight;
    }
    if (positionOfLargest == 1) {
      ball.velocity[1] += paddle.velocity/2;
      return CollisionType.goUp;
    }
    if (positionOfLargest == 3) {
      ball.velocity[1] += paddle.velocity/2;
      return CollisionType.goDown;
    }
  }
  return CollisionType.None;
}

float rightOfPaddle(Ball ball, Paddle paddle) {
  return ((ball.position[0] - ball.radius) - (paddle.position[0] + paddle.dimensions[0]));
}

float leftOfPaddle(Ball ball, Paddle paddle) {
  return (paddle.position[0] - (ball.position[0] + ball.radius));
}

float topOfPaddle(Ball ball, Paddle paddle) {
  return (paddle.position[1] - (ball.position[1] + ball.radius));
}

float bottomOfPaddle(Ball ball, Paddle paddle) {
  return ((ball.position[1] - ball.radius) - (paddle.position[1] + paddle.dimensions[1]));
}

boolean closestPaddleIsLeft(Ball ball, Paddle left, Paddle right) {
  return (Math.abs(left.position[0] - ball.position[0]) < Math.abs(right.position[0] - ball.position[0]));
}

void dealWithCollisions() {
    switch (collision) {
      case LeftWall:
        scoreTwo.incrementScore();
        scoreTwo.message = Integer.toString(scoreTwo.score);
        reset();
        currentState = GameState.NewRound;
        leftWonLast = false;
        if (scoreTwo.score >= 3) {
          currentState = GameState.EndGame;
          hardReset();
        }
        break;
      case goDown:
        if (ball.velocity[1] < 0) {ball.velocity[1] *= -1;}
        break;
      case goUp:
        if (ball.velocity[1] > 0) {ball.velocity[1] *= -1;}
        break;
      case goLeft:
        paddleTwo.dimensions[1] *= 0.98;
        ball.velocity[1] += (paddleTwo.calculateVelocity()/6);
        if (ball.velocity[0] > 0) {ball.velocity[0] *= -1;}
        break;
      case goRight:
        paddleOne.dimensions[1] *= 0.98;
        ball.velocity[1] += (paddleOne.calculateVelocity()/6);
        //System.out.println("Hello");
        if (ball.velocity[0] < 0) {ball.velocity[0] *= -1;}
        break;
      case RightWall:
        scoreOne.incrementScore();
        reset();
        scoreOne.message = Integer.toString(scoreOne.score);
        currentState = GameState.NewRound;
        leftWonLast = true;
        if (scoreOne.score >= 3) {
          currentState = GameState.EndGame;
          hardReset();
        }
        break;
      default:
        break;
  }
}