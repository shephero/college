class Ball {
  int radius;
  float[] position = new float[2];
  float[] velocity = new float[2];
  
  Ball(int inputRadius) {
    radius = inputRadius;
    position = CANVAS_CENTRE.clone();
    randomVelocity();
  }
  
  void randomVelocity() {
    velocity[0] = STARTING_VELOCITY * (float) Math.random();
    velocity[1] = (float) Math.sqrt(Math.pow(STARTING_VELOCITY, 2) - Math.pow(velocity[0], 2));
    if (Math.random() > 0.5) {velocity[0] *= -1;}
    if (Math.random() > 0.5) {velocity[1] *= -1;}
    if (Math.abs(velocity[0]) / Math.abs(velocity[1]) < 1) {randomVelocity();}
    //velocity[0] = 1; velocity[1] = 0;
  }
  
  void draw() {
    ellipseMode(RADIUS);
    fill(SHADE_THREE);
    ellipse(position[0], position[1], radius, radius);
  }
  
  void update() {
    position[0] += velocity[0];
    position[1] += velocity[1];
  }
  
  void reset() {
    position = CANVAS_CENTRE.clone();
    randomVelocity();
  }
  
  void increaseVelocity() {
    if (Math.abs(velocity[0]) < MAX_VELOCITY) {
      velocity[0] *= 1.05;
      velocity[1] *= 1.05;
    }
  }
};