boolean pTwoUpKey = false;
boolean pTwoDownKey = false;
boolean pOneUpKey = false;
boolean pOneDownKey = false;

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      pTwoUpKey = true;
      pTwoDownKey = false;
    }
    else if (keyCode == DOWN) {
      pTwoUpKey = false;
      pTwoDownKey = true;
    }
  }
  if (key == 'w') {
    pOneUpKey = true;
    pOneDownKey = false;
  }
  else if (key == 's') {
    pOneUpKey = false;
    pOneDownKey = true;
  }
}

void keyReleased() {
  if (key == CODED) {
    if (keyCode == UP) {
      pTwoUpKey = false;
    }
    else if (keyCode == DOWN) {
      pTwoDownKey = false;
    }
  }
  if (key == 'w') {
    pOneUpKey = false;
  }
  else if (key == 's') {
    pOneDownKey = false;
  }
}

void mousePressed() {
  if (currentState == GameState.NewRound) {currentState = GameState.Playing;}
}