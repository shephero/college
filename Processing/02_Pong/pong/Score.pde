class Score {
  boolean isLeft;
  boolean endGame;
  String message;
  int score;
  int countDown;
  
  Score(boolean inputIsLeft) {
    isLeft = inputIsLeft;
    endGame = false;
    score = 0;
    countDown = 0;
    message = Integer.toString(score);
  }
  
  void reset() {
    score = 0;
  }
  
  void countDown() {
    message = Integer.toString(3 - (countDown/60)); //<>//
    System.out.println(Integer.toString(3 - (countDown/60)));
    System.out.println(countDown);
    countDown++;
    draw();
    if (countDown >= 180) {
      countDown = 0;
      currentState = GameState.Playing;
    }
  }
  
  void incrementScore() {
    score += 1;
  }
  
  void draw() {
    textFont(font);
    if (currentState == GameState.Playing) {message = Integer.toString(score);}
    if (isLeft) {
        text(message, CANVAS_SIZE[0]/8, CANVAS_SIZE[1]/8);
    }
    else {
      textAlign(CENTER, CENTER);
       text(message, CANVAS_SIZE[0] - CANVAS_SIZE[0]/8, CANVAS_SIZE[1]/8);
    }
  }  
};