void draw_stripe(float dotSize, float dots) {
  for (int i = 0; i < dots; i++) {
    fill(SHADE_TWO);
    rect(CANVAS_SIZE[0]/2 - dotSize/2, dotSize*2*i, dotSize, dotSize, 4);
  }
}