interface IScreen {
    void draw();
    void populate();
    IWidget[] getWidgets();
}

class Screen implements IScreen {
    IWidget[] widgets;
    
    Screen() {}
    
    void draw() {
        background(colours[currentColourIndex]);
        for (IWidget w : widgets) {
            w.draw();
        }
    }
    
    void populate() {}
    
    IWidget[] getWidgets() {return widgets;}
};

class StartScreen extends Screen {
    
    StartScreen() {

    }
    
    void populate() {
        int widgetNum = 3;
        widgets = new IWidget[widgetNum];
        widgets[0] = new Button("Start", this);
        widgets[1] = new Button("Settings", new SettingsScreen());
        widgets[2] = new Button("Exit", new ExitScreen());
        place(widgets);
    }
    
    void draw() {
        super.draw();
    }
};

void place(IWidget[] widgets) {
    int totalHeight = (widgets.length - 1) * MARGIN;
    for (IWidget w : widgets) {totalHeight += w.getHeight();}
    widgets[0].setY(CANVAS_Y/2 - totalHeight/2 + widgets[0].getHeight()/2);
    for (int i = 1; i < widgets.length; i++) {
        widgets[i].setY(widgets[i-1].getY() + widgets[i-1].getHeight()/2 + widgets[i].getHeight()/2 + MARGIN);
    }
    for (IWidget w : widgets) {
        w.setX(CANVAS_X/2);
        w.place(); 
    }
}

class SettingsScreen extends Screen { 
    SettingsScreen() {}
    void populate() {
        int widgetNum = 3;
        widgets = new IWidget[widgetNum];
        widgets[0] = new MusicBox("Music enabled");
        widgets[1] = new Button("Theme", new ColourScreen()); //<>//
        widgets[2] = new Button("Back", new StartScreen());
        place(widgets);
    }
    
    void draw() {
        super.draw();
    }
};

class ColourScreen extends Screen {
    ColourScreen() {}
    
    void populate() {
        int widgetNum = 2;
        widgets = new IWidget[widgetNum];
        widgets[0] = new ColourChanger();
        widgets[1] = new Button("Back", new SettingsScreen());
        place(widgets);
    }
    
    void draw() {
        super.draw();
    }
}

class ExitScreen extends Screen {
    long startTime;
    ExitScreen() {startTime = millis();}
    void populate() {widgets = new IWidget[0];}
    void draw() {
        super.draw();
        text("KTHXBAI", CANVAS_X/2, CANVAS_Y/2);
        if (millis() - startTime > POPUP_SECONDS * 1000) {
            exit();
        }
    }
};