import ddf.minim.*;

PFont font;
final short CANVAS_X = 500;
final short CANVAS_Y = 500;
final byte ROUNDING = 4;
final byte BORDER = 2;
final byte MARGIN = 10;
final byte POPUP_SECONDS = 4;
byte fontSize = 0;

// Colours
final color grey = color(144, 144, 144);
final color red = color(174, 130, 130);
final color green = color(130, 174, 130);
final color blue = color(130, 130, 174);
final color[] colours = new color[] {grey, red, green, blue};
short currentColourIndex = 0;


// Greys
color colourOne = color(25, 25, 25);
color colourTwo = color(75, 75, 75);
color colourThree = color(125, 125, 125);
color colourFour = color(175, 175, 175);
color colourFive = color(225, 225, 225);

//Audio
Minim minim;

AudioPlayer player;