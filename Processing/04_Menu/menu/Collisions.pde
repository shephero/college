boolean pointIsInIRect(IRect rect, int x, int y) {
    if (x > rect.getX() - rect.getWidth()/2) {
        if (x < rect.getX() + rect.getWidth()/2) {
            if (y > rect.getY() - rect.getHeight()/2) {
                if (y < rect.getY() + rect.getHeight()/2) {
                    return true;
                }
            }
        }
    }
    return false;
}