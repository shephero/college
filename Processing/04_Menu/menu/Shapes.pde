interface IRect {
    int getX();
    int getY();
    int getWidth();
    int getHeight(); 
}