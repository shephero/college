interface IWidget extends IRect {
    void draw();
    //boolean beenPressed();
    IScreen getChild();
    void setX(int x);
    void setY(int y);
    void place();
}

class Widget implements IWidget{
    int x, y;
    int width, height;
    IScreen child;
    
    Widget() {}
    Widget(IScreen child) {
        this.child = child; //<>//
    }
    
    void setX(int x) {this.x = x;}
    void setY(int y) {this.y = y;}
    int getX() {return x;}
    int getY() {return y;}

    int getWidth() {return width;}
    int getHeight() {return height;}
    IScreen getChild() {return child;}
    
    void draw() {}
    void place() {}
}

class Button extends Widget {
    String label;
    boolean beingPressed;
    boolean pressed;
    
    // Full position specified
    Button(String label, IScreen child) {
        super(child);
        this.label = label;
        super.width = (int) (textWidth(label) + MARGIN * 2);
        super.height = fontSize + MARGIN * 2;
        beingPressed = false;
        pressed = false;
    }

    void draw() {
        stroke(colourOne);
        fill(255, colourThree);
        if (hovering()) {
            stroke(colourFive);
            if (mousePressed) {
                fill(colourFive);
                beingPressed = true;
            }
            else if (beingPressed) {
                child.populate();
                pressed = true;
                changeScreen(child);
                beingPressed = false;
            }
        }
        rect(x, y, width, height, ROUNDING);
        fill(colourOne);
        text(label, x, y);
    }
    
    boolean hovering() {
        return(pointIsInIRect(this, mouseX, mouseY));
    }
}

class RadioButton implements IRect {
    String label;
    short index;
    int x, y;
    int width, height;
    boolean active;
    boolean beingPressed;
    boolean beenPressed;
    
    RadioButton(String label) {
        this.label = label;
        active = false;
        beingPressed = false;
        beenPressed = false;
        this.height = fontSize;
    }
    
    void draw() {
        textColourAndStatus();
        text(label, x + fontSize/2 + MARGIN/2, y);
        fill(colourFive);
        noStroke();
        ellipse(x - width/2 + fontSize/2, y, fontSize, fontSize);
        buttonColour();
        ellipse(x - width/2 + fontSize/2, y, (fontSize * 3) / 5, (fontSize * 3) / 5);
    }
    
    void textColourAndStatus() {
        if (active) {
            fill(colourOne);
        }
        else if (hovering()) {
            if (mousePressed) {beingPressed = true;}
            else if (beingPressed) {beenPressed = true;}
            fill(colourFive);
        }
        else {
            fill(colourOne);
            beingPressed = false;
            beenPressed = false;
        }
    }
    
    void buttonColour() {
        if (active) {fill(colourOne);}
        //else if (hovering()) {fill(colourThree);}
        else {fill(colourFive);}
    }
    
    boolean hovering() {
        return pointIsInIRect(this, mouseX, mouseY);
    }
    
    void setX(int x) {this.x = x;}
    void setY(int y) {this.y = y;}
    int getX() {return x;}
    int getY() {return y;}

    int getWidth() {return this.width;}
    int getHeight() {return this.height;}
    void setWidth(int width) {this.width = width;}
    
    void setActive() {active = true;}
    void setInactive() {active = false;}
}

class RadioButtons extends Widget {
    RadioButton[] buttons;
    short active;
    
    RadioButtons(String[] labels) {
        this.buttons = new RadioButton[labels.length];
        int maxWidth = 0;
        for (String l : labels) {
            if (textWidth(l) > maxWidth) {
                maxWidth = (int) textWidth(l);
            }
        }
        maxWidth += fontSize + MARGIN;
        for (short i = 0; i < buttons.length; i++) {
            this.buttons[i] = new RadioButton(labels[i]);
            this.buttons[i].setX(CANVAS_X / 2);
            this.buttons[i].setWidth(maxWidth);
            this.buttons[i].index = i;
        }
        this.width = maxWidth + MARGIN * 2;
        this.height = fontSize * labels.length + MARGIN * (buttons.length + 1);
    }
    
    void draw() {
        for (short i = 0; i < buttons.length; i++) {
            buttons[i].draw();
            if (buttons[i].beenPressed && !buttons[i].active) {
                buttons[active].setInactive();
                buttons[i].setActive();
                active = i;
            }
        }
    }
    
    void place() {
        this.buttons[0].setY(this.y - this.height/2 + fontSize/2 + MARGIN);
        for (short i = 1; i < buttons.length; i++) {
            this.buttons[i].setY(buttons[i - 1].y + fontSize + MARGIN);
        }
    }
    
    void update() {}
}

class ColourChanger extends RadioButtons {
    ColourChanger() {
        super(new String[] {"Grey", "Red", "Green", "Blue"});
        buttons[currentColourIndex].setActive();
    }
    
    void draw() {
        for (short i = 0; i < buttons.length; i++) {
            buttons[i].draw();
            if (buttons[i].beenPressed && !buttons[i].active) {
                buttons[currentColourIndex].setInactive();
                buttons[i].setActive();
                currentColourIndex = i;
            }
        }
    }
}

class CheckBox extends Widget implements IRect {
    boolean on;
    String label;
    boolean active;
    boolean beingPressed;
    boolean beenPressed;
    
    CheckBox(String label) {
        this.label = label;
        beingPressed = false;
        beenPressed = false;
        this.height = fontSize + MARGIN * 2;
        this.width = (int) textWidth(label) + fontSize + MARGIN;
    }
    
    boolean hovering() {
        return pointIsInIRect(this, mouseX, mouseY);
    }
    
    
    void draw() {
        textColourAndStatus();
        text(label, x + MARGIN/2 + fontSize/2, y);
        noStroke();
        fill(colourFive);
        rect(x - width/2 + fontSize/2, y, fontSize, fontSize);
        buttonColour();
        rect(x - width/2 + fontSize/2, y, (fontSize * 3) / 5, (fontSize * 3) / 5);
    }
    
    void textColourAndStatus() {
        if (active) {
            fill(colourOne);
        }
        if (hovering()) {
            if (mousePressed) {beingPressed = true;}
            else if (beingPressed) {beenPressed = true;}
            fill(colourFive);
        }
        else {
            fill(colourOne);
            beingPressed = false;
            beenPressed = false;
        }
    }
    
    void buttonColour() {
        if (active) {fill(colourOne);}
        else if (hovering()) {fill(colourThree);}
        else {fill(colourFive);}
    }
}

class MusicBox extends CheckBox {
    
    MusicBox(String label) {
        super(label);
        active = player.isPlaying();
    }
    
    void draw() {
        super.draw();
        if (beenPressed) {
            if (active) {
                active = false;
                beenPressed = false;
                beingPressed = false;
                player.pause();
            }
            else {
                active = true;
                beenPressed = false;
                beingPressed = false;
                player.play();
            }
        }
    }
}