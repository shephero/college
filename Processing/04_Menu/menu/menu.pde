void settings() {
  size(CANVAS_X, CANVAS_Y, P2D);
}

IScreen screen;

void changeScreen (IScreen newScreen) {
    screen = newScreen;
}

void setup() {
    frameRate(60);      
    font = loadFont("UbuntuMono-Bold-20.vlw");
    fontSize = (byte) (textAscent() + textDescent());
    textFont(font);
    textAlign(CENTER, CENTER);
    rectMode(CENTER);
    strokeWeight(BORDER);
    screen = new StartScreen();
    screen.populate();
    minim = new Minim(this);
    player = minim.loadFile("moonlight.mp3");
    player.play();
    player.loop();
    //smooth(8);
}

void draw() {
    screen.draw();
}