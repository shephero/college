class Spawner {
  int invadersToSpawn;
  int invadersSpawned;
  float invaderSpeed;
  int timer;
  Random rand;
  
  Spawner(int invadersToSpawn) {
    this.invadersToSpawn = invadersToSpawn;
    this.invadersSpawned = 0;
    invaderSpeed = INVADER_SPEED;
    timer = 0;
    rand = new Random();
  }
  
  void spawn() {
    if (invadersSpawned < invadersToSpawn && (invadersSpawned == 0 || invaders.get(invaders.size() - 1).x > INVADER_SPACING)) {
      invaders.add(returnSpecial(-1 * invImg.width, 0, MovementState.toRightWall));
      invaders.get(invaders.size() - 1).speed = invaderSpeed;
      //System.out.println(invaderSpeed);
      invadersSpawned++;
    }
    timer++;
    if (invaders.size() > 0) {
      Invader randomInvader = getRandomInvader();
      if (randomInvader != null) {
        if (timer % (BASE_POWERUP_DROP_RATE - (game.round * 10)) == 0) {
          //System.out.println("True");
          timer = 0;
          powerups.add(new Powerup((rand.nextInt() % 2 == 0)?PowerUpMode.triple:PowerUpMode.laser, randomInvader));
        }
        if (timer % 60 == 0) {
          bombs.add(new Bomb(randomInvader)); //<>//
        }
      }
    }
    //System.out.println(invaders.get(invaders.size() - 1).x);
  }
  
  Invader getRandomInvader() {
    Invader randomInvader = invaders.get(Math.abs(rand.nextInt() % invaders.size()));
    if (randomInvader.state == State.normal) {
      return randomInvader;
    }
    return null;
  }
  
  void spawnTest() {
    invaders.add(new ChangeInv(CANVAS_X/2 - invImg.width/2, CANVAS_Y/2 - invImg.height/2, MovementState.toRightWall));
  }
  
  void reset() {
    spawner.invadersToSpawn = INVADER_NUM;
    spawner.invadersSpawned = 0;
    invaderSpeed = INVADER_SPEED;
  }
}