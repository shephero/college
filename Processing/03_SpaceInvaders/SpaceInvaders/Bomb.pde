class Bomb {
  float x;
  float y;
  float dy;
  PImage sprite;
  
  Bomb(Invader invader) {
    sprite = bombImg;
    dy = 1;
    x = (int) (invader.x + invader.sprite.width/2);
    y = (int) invader.y;
  }
  
  void move() {
    y += dy;
    dy += GRAVITY * 5;
  }
  
  void draw() {
    move();
    pushMatrix();
    translate(x, y);
    image(sprite, 0, 0); //<>//
    popMatrix();
  }
}