Invader returnSpecial(int x, int y, MovementState movementState) {
  Random randomGen = new Random();    
  int cases = 8;

  int whichCase = Math.abs(randomGen.nextInt()) % cases;
  switch (whichCase) {
    case 1:
      return new ChangeInv(x, y, movementState);
    case 2:
      return new UpsideInv(x, y, movementState);
    case 3:
      return new JitterInv(x, y, movementState);
    case 4:
      return new NoseInv(x, y, movementState);
    case 5:
      return new MouthInv(x, y, movementState);
    case 6:
      return new SinInv(x, y, movementState);
    default:
      //System.out.println(randomInt);
      return new Invader(x, y, movementState);
  }
}

class SinInv extends Invader {
  float sine;
  float sinExtent;
  SinInv(int x, int y, MovementState movementState) {
    //System.out.println("Created sine");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invImg;
    sine = (float) Math.random();
    sinExtent = 0;
    while (sinExtent <= 5) {
      sinExtent = (float) ( Math.random() * 10);
    }
  }
  
  void drawNormal() {
    move();
    sine += 0.1;
    image(sprite, x, y + sinExtent * sin(sine));
  }
}

class TintedInv extends Invader {
  int r;
  int g;
  int b;
  
  void drawNormal() {
    move();
    tint(r, g, b);
    image(sprite, x, y);
    tint(255, 255, 255);
  }
  
  void setExplode() {
    this.state = State.exploding;
    particles = createParticles(x, y, lastx, lasty, r, g, b);   
  }
  
  TintedInv(int x, int y, MovementState movementState) {
    //System.out.println("Created tinted");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    r = (int) (Math.random() * 255);
    g = (int) (Math.random() * 255);
    b = (int) (Math.random() * 255);
    sprite = invInv;
  }
}

class BludInv extends Invader {
  BludInv(int x, int y, MovementState movementState) {
    //System.out.println("Created blud");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invBlud;
  }
}

class MouthInv extends Invader {
  MouthInv(int x, int y, MovementState movementState) {
    //System.out.println("Created mouth");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invMouth;
  }
}

class ChangeInv extends Invader {
  float seed;
  int r;
  int g;
  int b;
  
  ChangeInv(int x, int y, MovementState movementState) {
    //System.out.println("Created change");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invInv;
    seed = (float) (Math.random() * -1 * 255);
  }
  
  void setExplode() {
    this.state = State.exploding;
    particles = createParticles(x, y, lastx, lasty, r, g, b);   
    //System.out.println("Particles: " + particles.size());
  }
  
  void drawNormal() {
    move();
    seed += 0.0005;
    r = (int)  (127.5 + 127.5 * cos(seed * 51));
    g = (int) (127.5 + 127.5 * cos(seed * 63));
    b = (int) (127.5 + 127.5 * cos(seed * 75));
    tint(r, g, b);
    image(sprite, x, y);
    tint(255, 255, 255);
  }
}

class JitterInv extends Invader {  
  int jitterFactor;
  
  JitterInv(int x, int y, MovementState movementState) {
    //System.out.println("Created Jitter");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invImg;
    jitterFactor = 0;
    while (jitterFactor < 2) {
      jitterFactor = (int) (Math.random() * 10);
    }
  }
  
  void drawNormal() {
    move();
    float randx = (float) (jitterFactor * (Math.random() - 0.5));
    float randy = (float) (jitterFactor * (Math.random() - 0.5));
    image(sprite, x + randx, y + randy);
  }
}

class UpsideInv extends Invader {
  UpsideInv(int x, int y, MovementState movementState) {
    //System.out.println("Created upside");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invUps;
  }
}

class NoseInv extends Invader {
  NoseInv(int x, int y, MovementState movementState) {
    //System.out.println("Created nose");
    this.x = x;
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invNos;
  }
}