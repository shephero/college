class Particle {
  protected float[] position;
  public float size;
  protected int[] fillColour;
  protected int[] strokeColour;

  //rates
  protected float dsize;
  protected float[] velocity;
  
  Particle() {
    
  }
  
  protected int[] chooseRandomColour() {
    int[] colour = new int[3];
    colour[0] = (int) random(255);
    colour[1] = (int) random(255);
    colour[2] = (int) random(255);
    return colour;
  }
  
  protected float ranDsize() {
    //return 0;
    return random(0.1) - 0.01;
  }
  
  protected float[] randomVelocity() {
    float[] velocity = new float[2];
    float speed = random(1) * PARTICLE_SPEED - PARTICLE_SPEED/2;
    float direction = random(2 * PI);
    velocity[0] = speed * cos(direction);
    velocity[1] = speed * sin(direction);
    //float yprop = Math.sqrt(velocity[0] * velocity[0]);
    //if (random(1) > 0.5) {velocity[1] *= -1;}
    return velocity;
  }
  
  protected int[] chooseColour() {
    int[] colour = new int[3];
    if (random(1) > 0.8) {
      colour[0] = 0;
      colour[1] = 0;
      colour[2] = 0;
    }
    else {
      colour[0] = 0;
      colour[1] = 0;
      colour[2] = 0;
    }
    return colour;
  }
  
  protected float randomSize() {
    return random(1) * MAX_PARTICLE_SIZE;
  }
  
  public boolean wontComeBack() {
    return (position[1] >= CANVAS_Y);
  }
  
  public void update() {
    velocity[1] += GRAVITY;
    position[0] += velocity[0];
    position[1] += velocity[1];
    size -= dsize;
  }
  
  public void draw() {
    
  }
}

class SquareParticle extends Particle {
  float spin;
  float rotation;
  SquareParticle(float x, float y, float dx, float dy) {
    dsize = ranDsize();
    position = new float[2];
    position[0] = x;
    position[1] = y;
    this.size = randomSize();
    velocity = randomVelocity();
    velocity[0] += dx * 0.8;
    velocity[1] += dy * 0.8;
    this.fillColour = chooseColour();
    this.strokeColour = chooseRandomColour();
    spin = randomSpin();
    rotation = 0;
  }
  
  private float randomSpin() {
    return random(MAX_SPIN) - MAX_SPIN/2;
  }
  
  SquareParticle(float x, float y, float dx, float dy, int r, int g, int b) {
    dsize = ranDsize();
    position = new float[2];
    position[0] = x;
    position[1] = y;
    this.size = randomSize();
    velocity = randomVelocity();
    velocity[0] += dx * 0.8;
    velocity[1] += dy * 0.8;
    int[] fill = new int[3];
    fill[0] = r;
    fill[1] = g;
    fill[2] = b;
    this.fillColour = fill;
    this.strokeColour = chooseRandomColour();
    spin = randomSpin();
  }
  
  public void draw() {
    pushMatrix();
    rotation += spin;
    update();
    fill(fillColour[0], fillColour[1], fillColour[2]);
    stroke(strokeColour[0], strokeColour[1], strokeColour[2]);
    //float c = cos(spin);
    translate(position[0], position[1]);     
    rotate(rotation);
    //translate(-position[0], -position[1]);
    rect(0, 0, size, size);
    popMatrix();
  }
}

class RoundParticle extends Particle {
  RoundParticle(float x, float y, float dx, float dy) {
    dsize = ranDsize();
    position = new float[2];
    position[0] = x;
    position[1] = y;
    this.size = randomSize();
    velocity = randomVelocity();
    velocity[0] += dx * 0.8;
    velocity[1] += dy * 0.8;
    this.fillColour = chooseColour();
    this.strokeColour = chooseRandomColour();
  }
  
  RoundParticle(float x, float y, float dx, float dy, int r, int g, int b) {
    dsize = ranDsize();
    position = new float[2];
    position[0] = x;
    position[1] = y;
    this.size = randomSize();
    velocity = randomVelocity();
    velocity[0] += dx * 0.8;
    velocity[1] += dy * 0.8;
    int[] fill = new int[3];
    fill[0] = r;
    fill[1] = g;
    fill[2] = b;
    this.fillColour = fill;
    this.strokeColour = chooseRandomColour();
  }
  
  public void draw() {
    pushMatrix();
    update();
    fill(fillColour[0], fillColour[1], fillColour[2]);
    stroke(strokeColour[0], strokeColour[1], strokeColour[2]);
    translate(position[0], position[1]);
    ellipse(0, 0, size, size);
    popMatrix();
  }
}

ArrayList<Particle> createParticles(float x, float y, float lastx, float lasty, int r, int g, int b) {
  int particleNum = (int) random(MAX_PARTICLES - MIN_PARTICLES) + MIN_PARTICLES;
  ArrayList<Particle> particles = new ArrayList(particleNum);
  for (int i = 0; i < particleNum; i++) {
    if (i % 2 == 0) {particles.add(new RoundParticle(x, y, x - lastx, y - lasty, r, g, b));}
    else {particles.add(new SquareParticle(x, y, x - lastx, y - lasty, r, g, b));}
  }
  return particles;
}

ArrayList<Particle> createParticles(float x, float y, float lastx, float lasty) {
  int particleNum = (int) random(MAX_PARTICLES - MIN_PARTICLES) + MIN_PARTICLES;
  ArrayList<Particle> particles = new ArrayList(particleNum);
  for (int i = 0; i < particleNum; i++) {
    if (i % 2 == 0) {particles.add(new RoundParticle(x, y, x - lastx, y - lasty));}
    else {particles.add(new SquareParticle(x, y, x - lastx, y - lasty));}
  }
  return particles;
}