public class Score {
  String message;
  int score;
  
  Score() {
    score = 0;
    message = Integer.toString(score);
  }
  
  void setMessage(String message) {
    this.message = message;
  }
  
  void setScore(int score) {
    this.score = score;
  }
  
  void updateScoreMessage() {
    message = Integer.toString(score);
  }
  
  void draw() {
    stroke(SCORE_COLOUR);
    textAlign(RIGHT);
    fill(SCORE_COLOUR);
    text(message, CANVAS_X - 48, CANVAS_Y - 48);
    updateScoreMessage();
  }
}