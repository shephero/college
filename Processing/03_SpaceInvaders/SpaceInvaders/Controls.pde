void keyPressed() {
  if (keyCode == LEFT) {
    leftKey = true;
    rightKey = false;
  }
  else if (keyCode == RIGHT) {
    leftKey = false;
    rightKey = true;
  }
  else if (key == ' ') {
    spaceKey = true;
    firer.fire();
  }
}

void keyReleased() {
  if (keyCode == LEFT) {
    leftKey = false;
  }
  else if (keyCode == RIGHT) {
    rightKey = false;
  }
  else if (key == ' ') {
    spaceKey = false;
  }
}