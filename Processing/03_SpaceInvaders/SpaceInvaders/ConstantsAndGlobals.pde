import java.util.Random;
//import processing.sound.*;

final int CANVAS_X = 800;
final int CANVAS_Y = 600;

final int INVADER_NUM = 30  ;
final int INVADER_SPEED = 2;
final float SPEED_INCREASE = 0.5;
final int INVADER_SPACING = 20;

final int SHIP_SPEED = 6;
final int SHIP_MARGIN = 30;

final float GRAVITY = 0.05;
final int MAX_PARTICLE_SIZE = 10;
final float PARTICLE_SPEED = 6; // pixels per second
final int MAX_PARTICLES = 30;
final int MIN_PARTICLES = 10;
final boolean EXPLOSION_TEST = false;
//final float JITTER_INCREASE = 0.1;
final float MAX_JITTER = 10;
final float MAX_SPIN = 0.7;

final int BULLET_COLOUR = 0;
final int BULLET_SIZE = 4;
final int BULLET_SPEED = 10;
final int DIAG_BULLET_DX = 3;
final int MAX_BULLETS = 3;

final int BASE_POWERUP_DROP_RATE = 500;
final int POWERUP_SPEED = 4;
final int MAX_TRIPLES = 10;

final int BACKGROUND = 200;
final int BACKGROUND_BLUR = 150;

final int SCORE_COLOUR = 50;
final int NEW_ROUND_FRAMES = 180;

final int BLOCK_Y_POSITION = 100;
final int BLOCK_SIZE = 10;
final int BLOCK_SPACING = 15;
final int BLOCK_HITS = 3;

//SinOsc sine = new SinOsc(this);


GameManager game;
Score score;
PFont font;

boolean leftKey = false;
boolean rightKey = false;
boolean spaceKey = false;

Spawner spawner;
ArrayList<Invader> invaders;
PImage invImg;
PImage invInv;
PImage invUps;
PImage invBlud;
PImage invNos;
PImage invMouth;
PImage laserUP;
PImage tripleUP;
PImage bombImg;

Ship ship;
PImage shipImg;

FiringManager firer; 
ArrayList<Powerup> powerups;
ArrayList<Bullet> bullets;

ArrayList<Bomb> bombs;
ArrayList<Block> blocks;