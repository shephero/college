enum PowerUpMode {
  none,
  triple,
  laser,
}

class FiringManager {
  PowerUpMode mode;
  int uses;
  
  FiringManager() {
    mode = PowerUpMode.none;
    uses = 0;
  }
  
  public void fire() {
    switch (mode) {
      case none:
        if (bullets.size() < MAX_BULLETS) {
          bullets.add(new Bullet(ship.x + ship.sprite.width/2, ship.y, -BULLET_SPEED));
        }
        break;
      case triple:
        uses++;
        if (uses > MAX_TRIPLES) {
          mode = PowerUpMode.none;
          uses = 0;
        }
        if (bullets.size() < MAX_BULLETS * 3) {
          bullets.add(new Bullet(ship.x + ship.sprite.width/2, ship.y, -BULLET_SPEED));
          bullets.add(new DiagBullet(ship.x + ship.sprite.width/2, ship.y, -BULLET_SPEED, DIAG_BULLET_DX));
          bullets.add(new DiagBullet(ship.x + ship.sprite.width/2, ship.y, -BULLET_SPEED, -DIAG_BULLET_DX));
        }
        break;
      case laser:
        uses++;
        if (uses > MAX_TRIPLES) {
          mode = PowerUpMode.none;
          uses = 0;
        }
    }
  }
};

class Powerup {
  int x;
  int y;
  PowerUpMode type;
  PImage sprite;
  Powerup(PowerUpMode type, Invader creator) {
    this.type = type;
    this.sprite = (type == PowerUpMode.laser)?laserUP:tripleUP;
    x = (int) (creator.x + creator.sprite.width/2);
    y = (int) (creator.y + creator.sprite.height);
  }
  void draw() {
    update();
    pushMatrix();
    translate(x, y);
    image(sprite, 0, 0);
    popMatrix();
  }
  void update() {
    y += POWERUP_SPEED;
  }
}