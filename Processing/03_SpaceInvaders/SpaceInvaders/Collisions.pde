public void detectCollisions() {
  for (Invader i : invaders) {
    if (invaderShipCollision(i, ship)) {
      game.gameOver();
      //System.out.println("True");
    }
    for (Bullet b : bullets) {
      if (bulletInvaderCollision(b, i) && i.state == State.normal ) {
        i.setExplode();
        //b.dead = true;
        score.score++;
        //explode.play();
      }
    }
  }
  for (int p = 0; p < powerups.size(); p++) {
    if (powerupShipCollision(powerups.get(p), ship)) {
      firer.mode = powerups.get(p).type;
      firer.uses = 0;
      powerups.remove(p);
    }
  }
  for (Bomb b : bombs) {
    if (b.y + b.sprite.height > ship.y) {
      if (b.x + b.sprite.height > ship.x) {
        if (b.x < ship.x + ship.sprite.width) {
          game.gameOver();
        }
      }
    }
  }
  for (Block b : blocks) {
    for (Bullet bu : bullets) {
      if (blockBulletCollision(b, bu)) {
        b.hit();
        //System.out.println("HI");
        bullets.remove(bullets.indexOf(bu));
        break;
      }
    }
    for (Bomb bo : bombs) {
      if (blockBombCollision(b, bo)) {
        b.hit();
        //System.out.println("");
        bombs.remove(bombs.indexOf(bo));
        break;
      }
    }
  }
}

public boolean blockBulletCollision(Block b, Bullet bu) {
  if (bu.x + BULLET_SIZE > b.x - BLOCK_SIZE/2) {
    if (bu.x - BULLET_SIZE < b.x + BLOCK_SIZE/2) {
      if (bu.y + BULLET_SIZE > b.y - BLOCK_SIZE/2) {
        if (bu.y - BULLET_SIZE < b.y + BLOCK_SIZE/2) {
          return true;
        }
      }
    }
  }
  return false;
}

public boolean blockBombCollision(Block b, Bomb bo) {
  if (bo.x + bo.sprite.width > b.x - BLOCK_SIZE/2) {
    if (bo.x < b.x + BLOCK_SIZE/2) {
      if (bo.y + bo.sprite.height > b.y - BLOCK_SIZE/2) {
        if (bo.y  < b.y + BLOCK_SIZE/2) {
          return true;
        }
      }
    }
  }
  return false;
}

public boolean powerupShipCollision(Powerup p, Ship s) {
  if (p.y + p.sprite.height > s.y) {
    if (p.x + p.sprite.width > s.x) {
      if (p.x < s.x + s.sprite.width) {
        //System.out.println("POWERUP!");
        return true;
      }
    }
  }
  return false;
}

public boolean invaderShipCollision(Invader i, Ship s) {
  if ((i.y + i.sprite.height > s.y && i.y + i.sprite.height < s.y + s.sprite.height) || (i.y < s.y + s.sprite.height && i.y > s.y)) {
    //System.out.println("True"); //<>//
    if ((i.x < s.x + s.sprite.width && i.x > s.x) || (i.x + i.sprite.width > ship.x && i.x + i.sprite.width < ship.x + ship.sprite.width)) {
      return true;
    }
  }
  return false;
}

public static boolean bulletInvaderCollision(Bullet b, Invader i) {
  return (b.x > i.x && b.x < i.x + i.sprite.width && b.y > i.y && b.y < i.y + i.sprite.width);
}