class Block {
  int x;
  int y;
  int hits;
  boolean dead;
  
  Block(int x, int y) {
    this.x = x;
    this.y = y;
    hits = 0;
    dead = false;
  }
  
  void hit() {
    hits++; //<>//
    if (hits > BLOCK_HITS) {dead = true;}
  }
  
  void draw() {
    fill((255 / BLOCK_HITS) * hits);
    rect(x, y, BLOCK_SIZE, BLOCK_SIZE);
  }
}

void initBlocks() {
  for (int i = 0; i < CANVAS_X / (BLOCK_SIZE + BLOCK_SPACING); i++) {
    Block block = new Block(BLOCK_SIZE/2 + i * (BLOCK_SPACING + BLOCK_SIZE), CANVAS_Y - BLOCK_Y_POSITION);
    blocks.add(block);
  }
}