enum MovementState {
  toRightWall,
  toLeftWall,
  downFromLeft,
  downFromRight,
}

enum State {
  normal,
  preExplode,
  exploding,
  dead,
}

class Invader {
  float x;
  float y;
  float lastx;
  float lasty;
  int layer;
  PImage sprite;
  float speed;
  float explosionIntensity;
  
  MovementState movementState;
  State state = State.normal;
  ArrayList<Particle> particles;
  
  Invader() {
    explosionIntensity = randomExplosionIntensity();
  };
  
  Invader(int x, int y, MovementState movementState) {
    this.x = x; //<>//
    this.y = y;
    this.movementState = movementState;
    layer = 1;
    sprite = invImg;
    explosionIntensity = randomExplosionIntensity();
  }
  
  float randomExplosionIntensity() {
    return 1.5 + (float) Math.random() * 5;
  }
  
  void move() {
    adjustMovement();
    lastx = x;
    lasty = y;
    switch (movementState) {
      case toRightWall:
        x += speed;
        break;
      case toLeftWall:
        x -= speed;
        break;
      case downFromLeft:
        y += speed;
        break;
      case downFromRight:
        y += speed;
        break;
    }
  }
  
  void setExplode() {
    this.state = State.exploding;
    particles = createParticles(x, y, lastx, lasty);    //<>//
    //System.out.println("Particles: " + particles.size());
  }
  
  void adjustMovement() {
    switch (movementState) {
      case toRightWall:
        if (x + sprite.width >= CANVAS_X) {
          movementState = MovementState.downFromRight;
        }
        break;
      case toLeftWall:
        if (x <= 0) {
          movementState = MovementState.downFromLeft;
        }
        break;
      case downFromLeft:
        if (y >= layer * sprite.width) {
          movementState = MovementState.toRightWall;
          layer++;
          speed *= 1.1;
        }
        break;
      case downFromRight:
        if (y >= layer * sprite.width) {
          movementState = MovementState.toLeftWall;
          layer++;
          speed *= 1.1;
        }
        break;
    }
  }
  
  void drawExplode() {
    for (Particle i : particles) {
      i.draw();
    }
    for (int i = 0; i < particles.size(); i++) {
      if (particles.get(i).wontComeBack() || particles.get(i).size <= 0) {
        particles.remove(i);
      }
    }
    if (particles.size() == 0) {state = State.dead;}
  }
  
  void drawNormal() {
    pushMatrix();
    move();
    translate(x, y);
    image(sprite, 0, 0);
    popMatrix();
  }
  
  boolean onScreen() {
    if (x + sprite.width < 0) {return false;}
    if (x > CANVAS_X) {return false;}
    if (y > CANVAS_Y) {return false;}
    if (y + sprite.width < 0) {return false;}
    return true;
  }
  
  void draw() {
    switch (state) { //<>//
      case exploding:
        drawExplode();
        break;
      case normal:
        drawNormal();
        break;
      default:
        break;
    }
  }
}