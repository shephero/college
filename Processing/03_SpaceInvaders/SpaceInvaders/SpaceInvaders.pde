void settings() {
  size(CANVAS_X, CANVAS_Y, P2D);
}

void setup() {
  frameRate(60);      
  rectMode(CENTER);
  font = loadFont("Ubuntu-Bold-48.vlw");

  invImg = loadImage("invader.gif");
  invInv = loadImage("invader.gif");
  invInv.filter(INVERT);
  invUps = loadImage("upsideInv.gif");
  invNos = loadImage("transNosInv.gif");
  invMouth = loadImage("mouthInv.gif");
  shipImg = loadImage("arch.png");
  laserUP = loadImage("laser.png");
  tripleUP = loadImage("triple.png");
  bombImg = loadImage("bomb.png");
  
  blocks = new ArrayList();
  invaders = new ArrayList(INVADER_NUM);
  bombs = new ArrayList();
  spawner = new Spawner(INVADER_NUM);
  score = new Score();
  ship = new Ship();
  bullets = new ArrayList(20);
  smooth(2);
  textFont(font);
  firer = new FiringManager();
  game = new GameManager();
  powerups = new ArrayList(1);
  initBlocks();
}

void draw() {
  switch (game.state) {
    case playing:
      drawPlaying();
      break;
    case roundDisplay:
      game.displayRound();
      break;
    case gameOver:
      game.displayGameOver();
      break;
  }
}

void drawPlaying() {
  stroke(BACKGROUND, BACKGROUND_BLUR);
  fill(BACKGROUND, BACKGROUND_BLUR);
  rect(CANVAS_X/2, CANVAS_Y/2, CANVAS_X, CANVAS_Y);
  
  spawner.spawn();
  for (Bullet b : bullets) {b.draw();}
  ship.draw();
  for (Powerup p : powerups) {p.draw();}
  cleanupBullets();
  cleanupInvaders();
  cleanupPowerups();
  detectCollisions();
  for (Invader i : invaders) {i.draw();}
  score.draw();
  cleanupBombs();
  cleanupBlocks();
  for (Bomb b : bombs) {b.draw();}
  for (Block b : blocks) {b.draw();}
  //saveFrame("image-######.png");
}

public void cleanupPowerups() {
  ArrayList<Integer> toDelete = new ArrayList(1);
  for (int i = 0; i < powerups.size(); i++) {
    if (powerups.get(i).y > CANVAS_Y) {
      toDelete.add(i);
    }
  }
  for (int i : toDelete) {
    try {
      powerups.remove(i);
    }
    catch (Exception e) {
      System.out.println("POWERUP ERROR");
      System.out.println(e);
    }
  }
}

public void cleanupBullets() {
  while (cleanBullet()) {}
}

boolean cleanBullet() {
  int index = -1;
  for (int i = 0; i < bullets.size(); i++) {
    if (bullets.get(i).y < 0) {
      index = i;
    }
  }
  if (index != -1) {
    bullets.remove(index);
    return true;
  }
  return false;
}

public void cleanupInvaders() {
  while (cleanInvader()) {}
  if (invaders.size() == 0) {
    game.newRound(); //<>//
  }
}

public void cleanupBombs() {
  while (cleanBomb()) {}
}

public boolean cleanBomb() {
  int index = -1;
  for (int i = 0; i < bombs.size(); i++) {
    if (bombs.get(i).y > CANVAS_Y) {
      index = i;
    }
  }
  if (index != -1) {
    bombs.remove(index);
    return true;
  }
  return false;
}

public void cleanupBlocks() {
  while (cleanBlock()) {}
}

public boolean cleanBlock() {
  int index = -1;
  for (int i = 0; i < blocks.size(); i++) {
    if (blocks.get(i).dead) {
      index = i; //<>//
    }
  }
  if (index != -1) {
    blocks.remove(index);
    return true;
  }
  return false;
}

boolean cleanInvader() {
  int index = -1;
  for (int i = 0; i < invaders.size(); i++) {
    if (invaders.get(i).state == State.dead) {
      index = i;
    }
  }
  if (index != -1) {
    invaders.remove(index);
    return true;
  }
  return false;
}