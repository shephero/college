class Bullet {
  int x;
  int y;
  int dy;  
  Bullet() {
    
  }
  
  Bullet(int x, int y, int dy) {
    this.x = x;
    this.y = y;
    this.dy = dy;
  }
  
  private void update() {
    y += dy;
  }
  
  public void draw() {
    update();
    fill(BULLET_COLOUR);
    stroke(BULLET_COLOUR);
    pushMatrix();
    translate(x, y);
    ellipse (0, 0, BULLET_SIZE, BULLET_SIZE);
    popMatrix();
  }
}

class DiagBullet extends Bullet {
  int dx;
  DiagBullet(int x, int y, int dy, int dx) {
    this.x = x;
    this.y = y;
    this.dy = dy;
    this.dx = dx;
  }
  private void update() {
    y += dy;
    x += dx;
  }
  public void draw() {
    update();
    fill(BULLET_COLOUR);
    stroke(BULLET_COLOUR);
    pushMatrix();
    translate(x, y);
    ellipse (0, 0, BULLET_SIZE, BULLET_SIZE);
    popMatrix();
  }
};