class Ship {
  int x;
  int y;
  PImage sprite;
  
  Ship() {
    sprite = shipImg;
    x = CANVAS_X/2 - sprite.width/2;
    y = CANVAS_Y - sprite.height;
  }
  
  void draw() {
    update();
    pushMatrix();
    translate(x, y);
    image(sprite, 0, 0);
    popMatrix();
  }
  
  void limitToLeft(int dx) {
    if (x + dx > SHIP_MARGIN) {x += dx;}
  }
  
  void limitToRight(int dx) {
    if (x + sprite.width < CANVAS_X - SHIP_MARGIN) {x += dx;}
  }
  
  void update() {
    if (leftKey) {
      limitToLeft(-SHIP_SPEED);
    }
    else if (rightKey) {
      limitToRight(SHIP_SPEED);
    }
  }
}