enum GameState {
  playing,
  roundDisplay,
  gameOver,
}

class GameManager {
  GameState state;
  int round;
  int timer;
  
  GameManager() {
    this.state = GameState.roundDisplay;
    timer = 0;
    round = 1;
  }
  
  void gameOver() {
    timer = 0;
    state = GameState.gameOver;
  }
  
  void displayMessage(String message) {
    stroke(BACKGROUND, BACKGROUND_BLUR);
    fill(BACKGROUND, BACKGROUND_BLUR);
    rect(CANVAS_X/2, CANVAS_Y/2, CANVAS_X, CANVAS_Y);
    for (Invader i : invaders) {i.draw();}
    stroke(SCORE_COLOUR);
    noStroke();
    fill(SCORE_COLOUR);
    textAlign(CENTER);
    
    text(message, CANVAS_X/2, CANVAS_Y/2);
  }
  
  void displayGameOver() {
    displayMessage("Game Over");
    timer++;
    if(timer > NEW_ROUND_FRAMES) {
      round = 0;
      score.score = 0;
      state = GameState.playing;
      for (int i = invaders.size() - 1; i >=0; i--) {invaders.remove(i);}
      for (int i = bombs.size() - 1; i >=0; i--) {bombs.remove(i);}
      for (int i = blocks.size() - 1; i >=0; i--) {blocks.remove(i);}
      initBlocks();
    }
  }
  
  void newRound() {
    this.state = GameState.roundDisplay;
    this.round++;
    spawner.invaderSpeed = round * SPEED_INCREASE + INVADER_SPEED;
    timer = 0; //<>//
  }
  
  void displayRound() {
    displayMessage("Round " + round);
    timer++;
    if(timer > NEW_ROUND_FRAMES) {
      spawner.invadersToSpawn = (int) (INVADER_NUM + (round - 1) * 0.2 * INVADER_NUM);
      spawner.invadersSpawned = 0;
      score.score = 0;
      state = GameState.playing;
      for (int i = 0; i < invaders.size(); i++) {invaders.remove(i); }
      for (int i = blocks.size() - 1; i >=0; i--) {blocks.remove(i);}
      for (int i = bombs.size() - 1; i >=0; i--) {bombs.remove(i);}
      initBlocks();
    }
  }
}