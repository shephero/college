### What is this repository for? ###

This repository hosts my college work, in case I accidentally wipe my computer whilst trying to install an unusual unix derivative.

### How do I test Java code? ###

To test a java program, assuming a unix-like system with java installed, compile with the commands:

```
#!bash
cd /path/to/file
javac nameOfFile.java
java -cp . nameOfFile   (Without Suffix)
```

### How do I test the ARM Assembly code? ###

Unfortunately, the assembly code uses functions specific to my course that I did not write, and hence I cannot upload. If you want a demo, email me, and I'll screencast my code running your tests.

### Disclaimer ###

Not all code here was part of an assignment, those files weren't submitted, and as they were only for my own testing, they have not been polished at all.